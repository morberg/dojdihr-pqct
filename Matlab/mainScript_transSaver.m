clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, regPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

setNames = {'1-1','1-2','1-3','2-1','2-3','3-1','3-3','4-1','5-1','5-2','5-3',...
    '6-1','6-2','6-3','7-1','7-2','7-3','8-1','8-2','8-3','9-1','9-2','11-1',...
    '10-1','10-2','10-3','11-2','11-3','12-1','12-3','13-1','13-2','13-3','14-1',...
    '14-1','15-1','15-2','16-1','16-2','16-3','17-1','17-2','17-3','18-1','18-2',...
    '18-3','19-1','19-2','20-1','21-1','22-1','23-1','23-2','23-3','24-1','24-2',...
    '24-3','25-1','25-2','25-3','26-1','27-1','27-2','27-3','28-1','28-2','28-3',...
    '29-1','29-2','30-1','30-2','30-3','31-1','31-2','31-3','32-1','32-2','32-3',...
    '33-1','33-3','34-1','34-2','34-3','35-1','35-2','35-3','36-1','36-2','36-3',...
    '37-1','37-2','37-3','38-1','38-2','38-3','39-1','39-2','39-3','40-1','40-2',...
    '40-3','41-1','41-2','41-3','42-1','42-2','42-3','43-1','43-3','44-1','45-1',...
    '45-3','53-1','53-2','53-3'};

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 1; 

% Select to display the timing of the invidual steps
data.displayTimes = 0;
% Select to have waitbars
data.wb = 0;

data.evalData = 0;

data.evalFull = 0;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;

data.evalAccuracy = 0;
data.evalAccuracyRatio = 0;
data.evalDice  = 0;
data.evalHausdorff = 0;

rad = 18;
alpha = 0.08;
num_it = 20;
epsilon = 1;
imgs = [110,111,220,221];

params.GradientMagnitudeTolerance = 0.0001;
params.MinimumStepLength = 0.00001;
params.MaximumStepLength = 0.0325;
params.MaximumIterations = 100;
params.RelaxationFactor = 0.5;


for j = 1:size(setNames,2)

data.setName = setNames{j};

data = preprocessing(data);

data = Thresholder(data);

data = MO(data);

for i = imgs
    data.imDataArrayBW(:,:,i) = local_AC_UM(im2double(data.imDataArrayAdj(:,:,i)),im2double(data.imDataArrayBW(:,:,i)),rad,alpha,num_it,epsilon);
end

data = cropJoints(data);

%% Transformation
[M,N,O] = size(data.imDataArrayBW);
cb_ref = imref2d(size(data.imDataArrayBW(:,:,1)));

[result.t1.tform1,~] = register(double(data.joint1BW(:,:,111)), double(data.joint1BW(:,:,110)),1,params);
[result.t1.tform2,~] = register(double(data.joint2BW(:,:,111)), double(data.joint2BW(:,:,110)),1,params);
[result.t1.tform3,~] = register(double(data.joint3BW(:,:,111)), double(data.joint3BW(:,:,110)),1,params);
[result.t1.tform4,~] = register(double(data.joint4BW(:,:,111)), double(data.joint4BW(:,:,110)),1,params);

data.joint1BW(:,:,220) = imwarp(data.joint1BW(:,:,220), result.t1.tform1,'OutputView', cb_ref);
data.joint2BW(:,:,220) = imwarp(data.joint2BW(:,:,220), result.t1.tform2,'OutputView', cb_ref);
data.joint3BW(:,:,220) = imwarp(data.joint3BW(:,:,220), result.t1.tform3,'OutputView', cb_ref);
data.joint4BW(:,:,220) = imwarp(data.joint4BW(:,:,220), result.t1.tform4,'OutputView', cb_ref);

[result.t2.tform1,~] = register(double(data.joint1BW(:,:,221)), double(data.joint1BW(:,:,220)),1,params);
[result.t2.tform2,~] = register(double(data.joint2BW(:,:,221)), double(data.joint2BW(:,:,220)),1,params);
[result.t2.tform3,~] = register(double(data.joint3BW(:,:,221)), double(data.joint3BW(:,:,220)),1,params);
[result.t2.tform4,~] = register(double(data.joint4BW(:,:,221)), double(data.joint4BW(:,:,220)),1,params);

if exist([pathdata.gitpath, 'radetect_src/Matlab/transResults.mat'], 'file') == 2
    
    load([pathdata.gitpath, 'radetect_src/Matlab/transResults.mat']);
    
    result.set = data.setName;
    
    results = [results, result];
else
    result.set = data.setName;
    results = result;
end
    
save([pathdata.gitpath, 'radetect_src/Matlab/transResults.mat'], 'results');

disp(['Dataset: ', data.setName])
end