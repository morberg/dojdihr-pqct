close all
clear


addpath('C:\Users\Andersen\Documents\github\radetect_src\Matlab\Preprocessing\')
data.drive = 'N';
data.setName = '28-2';
data.wb = 1;
data.step = 'Loading images ';
data.imgNr = 0;
data.imgsNr = 330;
data.displayTimes = 1;
data = loadDataset(data);
Amp1 = zeros(1,330);
Cen1 = zeros(1,330);
Wid1 = zeros(1,330);
Amp2 = zeros(1,330);
Cen2 = zeros(1,330);
Wid2 = zeros(1,330);

for i = 1:data.imgsNr

    image = im2uint16(data.imDataArray(:,:,i));
    [counts,binLocations] = imhist(image, 65536);
    f = fit(binLocations,counts,'gauss2');

    if f.b1 > f.b2
        Amp1(1,i) = f.a1;
        Cen1(1,i) = f.b1;
        Wid1(1,i) = f.c1;
        Amp2(1,i) = f.a2;
        Cen2(1,i) = f.b2;
        Wid2(1,i) = f.c2;
    else
        Amp1(1,i) = f.a2;
        Cen1(1,i) = f.b2;
        Wid1(1,i) = f.c2;
        Amp2(1,i) = f.a1;
        Cen2(1,i) = f.b1;
        Wid2(1,i) = f.c1;
    end
    disp(['Image nr: ', num2str(i)])
end

meanAmp1= mean(Amp1); 
varAmp1 = var(Amp1);
meanCen1 = mean(Cen1); 
varCen1 = var(Cen1);
meanWid1 = mean(Wid1); 
varWid1 = var(Wid1);
meanAmp2 = mean(Amp2); 
varAmp2 = var(Amp2);
meanCen2 = mean(Cen2); 
varCen2 = var(Cen2);
meanWid2 = mean(Wid2); 
varWid2 = var(Wid2);

%%
figure
plot(Amp1), title('Amplitude Gaussian 1');
figure
plot(Amp2), title('Amplitude Gaussian 2');
figure
plot(Cen1), title('Center Gaussian 1');
figure
plot(Cen2), title('Center Gaussian 2');
figure
plot(Wid1), title('Width Gaussian 1');
figure
plot(Wid2), title('Width Gaussian 2');
