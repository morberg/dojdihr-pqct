clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'None';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;

%%
data = preprocessing(data);

%%
data = histAnalysis2(data);

%%
data.step = 'Analysis';
data = loadDataset(data);


%%
hist = histcounts(data.imDataArrayAdj(:,:,1), 65536);
x = 1:65536;
x = x(hist > 0);
hist = hist(hist > 0);

figure
plot(x, hist)
title('Histogram of HR-pQCT image')
xlabel('Intensity value')
ylabel('Counts')


%%
imageNoise = data.imDataArrayAdj(:,:,1);
imageTissue = data.imDataArrayAdj(:,:,1);
imageNoise(imageNoise > 12360) = 0;
imageTissue(imageTissue < 12360) = 0;

figure 
imshow(imageNoise)

figure
imshow(imageTissue)

%%
hist = histcounts(data.imDataArray, 65536);
x = 1:65536;
x = x(hist > 0);
hist = hist(hist > 0);

figure
plot(x, hist)
title('Histogram of HR-pQCT dataset')
xlabel('Intensity value')
ylabel('Counts')
%%
f = fit(x1(:),hist(:),'gauss2');

plot(f)

center = [f.b1, f.b2]%, f.b3];
width  = [f.c1, f.c2]%, f.c3];
height = [f.a1, f.a2]%, f.a3];

g1 = height(1)*exp(-((x-center(1))/width(1)).^2);
g2 = height(2)*exp(-((x-center(2))/width(2)).^2);
%g3 = height(3)*exp(-((x-center(3))/width(3)).^2);
combined = g1+g2%+g3;

    figure
    plot(x, g1); hold on
    plot(x, g2); hold on 
    %plot(x, g3); hold on
    plot(x, combined)
    legend('Noise', 'Tissue', 'Bone', 'Combined')
    
%%
GT = imread('C:\Users\Andersen\Documents\github\radetect_src\Labeling\GT\1-1\Label_1.png');

[n, m] = size(data.imDataArrayAdj(:,:,1));

imSize = n*m;

gtSize = sum(sum(GT));

pro = gtSize/imSize;

%%

GTIm = data.imDataArrayAdj(:,:,1);
GTIm(~logical(GT)) = 0; 

figure
imshow(GTIm)

hist2 = histcounts(GTIm, 65536);
hist2(1) = 0;
x2 = 1:65536;
x2 = x2(hist2 > 0);
hist2 = hist2(x2);

hist1Sum = sum(hist)

hist2Sum = sum(hist2)

figure
plot(x2, hist2)

f2 = fit(x2(:),hist2(:),'gauss2');

center2 = [f2.b1, f2.b2];
width2  = [f2.c1, f2.c2];
height2 = [f2.a1, f2.a2];

g12 = height2(1)*exp(-((x-center2(1))/width2(1)).^2);
g22 = height2(2)*exp(-((x-center2(2))/width2(2)).^2);
combined2 = g12+g22;

    figure
    plot(x, g1); hold on
    plot(x, g2); hold on 
    plot(x, g3); hold on
    plot(x, combined); hold on
    plot(x, combined2)
    legend('Noise', 'Tissue', 'Bone', 'Combined', 'GT')
    
%%

[th, co, h] = mipentropy_th(im2double(data.imDataArrayAdj(:,:,1)), 65536);

im =  imbinarize(data.imDataArrayAdj(:,:,1), th);

figure
imshow(im)

%%

[th, cf, cbins] = mipminerror(im2double(data.imDataArrayAdj(:,:,1)), 65536);

im =  imbinarize(data.imDataArrayAdj(:,:,1), th);

figure
imshow(im)

%%
[th, cf, cbins] = mipbcv(im2double(data.imDataArrayAdj(:,:,1)), 65536);

im =  imbinarize(data.imDataArrayAdj(:,:,1), th);

figure
imshow(im)
%%
[th, cf, cbins] = mipkurita(im2double(data.imDataArrayAdj(:,:,1)), 65536);

im =  imbinarize(data.imDataArrayAdj(:,:,1), th);

im2 = data.imDataArrayAdj(:,:,1);
im2(~im) = 0;

figure
imshow(im2)

[th, cf, cbins] = mipkurita(im2, 65536);

im =  imbinarize(data.imDataArrayAdj(:,:,1), th);

figure
imshow(im)