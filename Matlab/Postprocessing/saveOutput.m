function data = saveOutput( data)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    % Initialize waitbar and timer
    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Saving dataset'];
        h = waitbar(0,wbMsg);
    end
    
    setSize = size(data.imDataArrayBW,3);
    
    resultDirName   = [data.resultPath, '/results'];
    setDir          = [resultDirName, '/', data.setName];
    maskDir         = [setDir, '/', 'BW_Mask'];
    segDir          = [setDir, '/', 'Segmented'];
    jointDir        = [setDir, '/', 'Joints'];
    jointSegDir     = [jointDir, '/', 'Segmented'];
    jointBWDir      = [jointDir, '/', 'BW_Mask'];
    joint1SegDir    = [jointSegDir, '/', '1'];
    joint2SegDir    = [jointSegDir, '/', '2'];
    joint3SegDir    = [jointSegDir, '/', '3'];
    joint4SegDir    = [jointSegDir, '/', '4'];
    joint1BWDir     = [jointBWDir, '/', '1'];
    joint2BWDir     = [jointBWDir, '/', '2'];
    joint3BWDir     = [jointBWDir, '/', '3'];
    joint4BWDir     = [jointBWDir, '/', '4'];
    
    if 7 ~= exist(resultDirName, 'dir')
        mkdir(resultDirName);
    end
    if 7 ~= exist(setDir, 'dir')
            mkdir(setDir);
            mkdir(maskDir);
            mkdir(segDir);
            mkdir(jointDir);
            mkdir(jointSegDir);
            mkdir(jointBWDir);
            mkdir(joint1SegDir);
            mkdir(joint2SegDir);
            mkdir(joint3SegDir);
            mkdir(joint4SegDir);
            mkdir(joint1BWDir);
            mkdir(joint2BWDir);
            mkdir(joint3BWDir);
            mkdir(joint4BWDir);
    end
    
    for k = 1:setSize
        
         if data.wb == 1
            waitbar(k/setSize)
         end
        
        if data.saveBW == 1
            % BW
            outputFile = strcat(maskDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.imDataArrayBW(:,:,k))), outputFile, data.imInfoArray(:,k)) 
        end
        
        if data.saveSeg == 1
            % Seg
            outputFile = strcat(segDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.imDataArraySeg(:,:,k))), outputFile, data.imInfoArray(:,k))
        end
        
        if data.saveJointsSeg == 1
            % joint
            outputFile = strcat(joint1SegDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint1(:,:,k))), outputFile, data.imInfoArray(:,k))

            outputFile = strcat(joint2SegDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint2(:,:,k))), outputFile, data.imInfoArray(:,k))

            outputFile = strcat(joint3SegDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint3(:,:,k))), outputFile, data.imInfoArray(:,k))

            outputFile = strcat(joint4SegDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint4(:,:,k))), outputFile, data.imInfoArray(:,k))
        end
        
        if data.saveJointsBW == 1
            outputFile = strcat(joint1BWDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint1BW(:,:,k))), outputFile, data.imInfoArray(:,k))

            outputFile = strcat(joint2BWDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint2BW(:,:,k))), outputFile, data.imInfoArray(:,k))

            outputFile = strcat(joint3BWDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint3BW(:,:,k))), outputFile, data.imInfoArray(:,k))

            outputFile = strcat(joint4BWDir,'/',data.filePrefix,'_',num2str(sprintf('%05d',k-1+data.imgNr)), '.DCM');
            dicomwrite(uint16(double(intmax('uint16'))*mat2gray(data.joint4BW(:,:,k))), outputFile, data.imInfoArray(:,k))
        end
    end
    
    if data.wb == 1
        close(h)
    end
    
    if data.displayTimes ==1
        fprintf("Save dataset complete. Time: %3.2f s\n", toc(time));
    end
end

