function data = postprocessing(data)
%POSTPROCESS Summary of this function goes here
%   Detailed explanation goes here
    time = tic;
    data.step = 'Postprocessing: ';

    % Crop images in the individual joints
    if data.saveJointsSeg == 1 || data.saveJointsBW == 1
        if 1 ~= isfield(data,'joint1BW')
            data = cropJoints(data);
        end
    end
    
    % Save images to dcm files in results folder
    data = saveOutput(data);
    
    if data.displayTimes ==1
        fprintf("Postprocessing complete. Time: %3.2f s\n", toc(time));
    end
    
end

