function data = cropJoints(data)
%POSTPROCESS Summary of this function goes here
%   Detailed explanation goes here

    % Initialize waitbar and timer
    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Cropping joints'];
        h = waitbar(0,wbMsg);
    end
    
    setSize = size(data.imDataArrayBW,3);

    data.joint1BW = logical(false(size(data.imDataArrayBW)));
    data.joint2BW = data.joint1BW;
    data.joint3BW = data.joint1BW;
    data.joint4BW = data.joint1BW;

    data.joint1 = uint16(zeros(size(data.imDataArrayBW)));
    data.joint2 = data.joint1;
    data.joint3 = data.joint1;
    data.joint4 = data.joint1;

    x = zeros(1,4);
    elems = 0;

    for i = 1:10
        [labelBW, numElems] = bwlabel(data.imDataArrayBW(:,:,i)); 
        if numElems == 4
            elems = elems + 1;
            rp = regionprops(labelBW, 'Centroid');
            for j = 1:4
                x(1,j) = x(1,j) + rp(j).Centroid(1);
            end
        end
    end

    x = x/elems;
    regions = [0, ((x(1)+x(2))/2), ((x(2)+x(3))/2), ((x(3)+x(4))/2), size(data.imDataArrayBW(:,:,i),2)];

    for k = 1:setSize
        
         if data.wb == 1
            waitbar(k/setSize)
         end
        
        [labelBW, numElems] = bwlabel(data.imDataArrayBW(:,:,k)); 

        rp = regionprops(labelBW, 'Centroid');
        cc = bwconncomp(data.imDataArrayBW(:,:,k), 8);

        for l = 1:numElems
            elemTemp = false(size(data.imDataArrayBW(:,:,k)));
            elemTemp(cc.PixelIdxList{l}) = true;
            if rp(l).Centroid(1) > regions(1) && rp(l).Centroid(1) < regions(2)
                data.joint1BW(:,:,k) = data.joint1BW(:,:,k) + double(elemTemp(:,:,1));
            elseif rp(l).Centroid(1) > regions(2) && rp(l).Centroid(1) < regions(3) 
                data.joint2BW(:,:,k) = data.joint2BW(:,:,k) + double(elemTemp(:,:,1));
            elseif rp(l).Centroid(1) > regions(3) && rp(l).Centroid(1) < regions(4) 
                data.joint3BW(:,:,k) = data.joint3BW(:,:,k) + double(elemTemp(:,:,1));
            elseif rp(l).Centroid(1) > regions(4) && rp(l).Centroid(1) < regions(5) 
                data.joint4BW(:,:,k) = data.joint4BW(:,:,k) + double(elemTemp(:,:,1));
            end
        end

        tempJoint1 = uint16(data.imDataArrayAdj(:,:,k));
        tempJoint1(~data.joint1BW(:,:,k)) = 0;
        data.joint1(:,:,k) = tempJoint1;

        tempJoint2 = uint16(data.imDataArrayAdj(:,:,k));
        tempJoint2(~data.joint2BW(:,:,k)) = 0;
        data.joint2(:,:,k) = tempJoint2;

        tempJoint3 = uint16(data.imDataArrayAdj(:,:,k));
        tempJoint3(~data.joint3BW(:,:,k)) = 0;
        data.joint3(:,:,k) = tempJoint3;

        tempJoint4 = uint16(data.imDataArrayAdj(:,:,k));
        tempJoint4(~data.joint4BW(:,:,k)) = 0;
        data.joint4(:,:,k) = tempJoint4;

    end
    
    if data.wb == 1
        close(h)
    end
    
    if data.displayTimes == 1
        fprintf("Cropping joints done. Time: %3.2f s\n", toc(time));
    end
end

