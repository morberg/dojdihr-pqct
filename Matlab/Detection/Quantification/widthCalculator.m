function [len,pixel1,pixel2,slice] = widthCalculator(erosion, imageSet)
%WIDTHCALCULATOR Summary of this function goes here
    %   Detailed explanation goes here
    
    full = imageSet + erosion;
    full = imclose(full, strel('sphere',3));
    inner = imerode(full, strel('sphere', 1));
    outer = imabsdiff(full,inner);

    side = erosion.*outer;
    side  = bwmorph3(side, 'clean');
    
    rp = regionprops3(side);
    [~, index] = max(rp.Volume);
    sliceMin = round(rp.BoundingBox(index,3));
    sliceMax = sliceMin + rp.BoundingBox(index,6);

    sliceSize = sliceMax-sliceMin;
    length = zeros(1,sliceSize);
    pixel1Pos = zeros(2,sliceSize);
    pixel2Pos = zeros(2,sliceSize);
    dist = zeros(1,8*8);
    pos1 = zeros(2,8*8);
    pos2 = zeros(2,8*8);
    for i = 1:sliceSize
        rp = regionprops(side(:,:,sliceMin-1+i),'Area', 'Extrema');
        rpArea = [rp.Area];
        [~, index] = max(rpArea);
        extrema = rp(index).Extrema;
        for j = 1:8
            for k = 1:8
                x1 = extrema(j,1);
                x2 = extrema(k,1);
                y1 = extrema(j,2); 
                y2 = extrema(k,2);
                pos1(1,j*8+k) = x1;
                pos1(2,j*8+k) = y1;
                pos2(1,j*8+k) = x2;
                pos2(2,j*8+k) = y2;
                dist(1,j*8+k) = sqrt((x1-x2)^2+(y1-y2)^2);     
            end
        end
        [length(1,i), indexLenght] = max(dist);
        pixel1Pos(:,i) = pos1(:,indexLenght);
        pixel2Pos(:,i) = pos2(:,indexLenght);
    end

    [len, maxIndex] = max(length);
    pixel1 = pixel1Pos(:,maxIndex);
    pixel2 = pixel2Pos(:,maxIndex);
    slice = sliceMin-1+maxIndex;
end

