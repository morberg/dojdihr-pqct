function [maxDepth,pixel] = depthCalculator(erosion, pixel1, pixel2, slice)
%DEPTHCALCULATOR Summary of this function goes here
%   Detailed explanation goes here
erosionBorder = bwmorph(erosion(:,:,slice), 'remove');

[M,N] = size(erosionBorder);
x1 = pixel1(1);
y1 = pixel1(2);
x2 = pixel2(1);
y2 = pixel2(2);

numPixels = sum(sum(erosionBorder));
depths = zeros(1,numPixels);
xVals = zeros(1,numPixels);
yVals = zeros(1,numPixels);
it = 1;

for i = 1:M
    for j = 1:N
        if erosionBorder(i,j) == 1
            depths(1,it) = abs( (y2-y1)*j-(x2-x1)*i+x2*y1-y2*x1)/sqrt((y2-y1)^2+(x2-x1)^2);
            xVals(1,it) = j;
            yVals(1,it) = i;
            it = it +1;       
        end      
    end
end

[maxDepth, maxIndex] = max(depths);
pixel = [xVals(1,maxIndex), yVals(1,maxIndex)];

end

