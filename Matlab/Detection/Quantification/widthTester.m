clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath ]));
addpath(genpath([data.gitPath, detPath ]));

data.setName = '31-3';
reg = 1;

data.datapath = ['P:\results\',data.setName,'\'];
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitterv2(imageSet));

imageSet = logical(elementRemoval(imageSet));

load([pathdata.gitpath,'radetect_src\Matlab\Detection\erosionResults\erosions',data.setName,'.mat'])

[imageSetReturn,erosionsReturn] = registrationRetrans(imageSet,erosions, data.setName, data.gitPath);
erosionsReturn = bwmorph3(erosionsReturn, 'majority');

%%
cc = bwconncomp(erosionsReturn);
erosion = false(size(erosionsReturn));
erosion(cc.PixelIdxList{2}) = 1;

volumeViewer(erosion)

%%

    rp = regionprops3(erosion);
    
    center = round(rp.Centroid(3));
    
    full = imageSet(:,:,center) + erosion(:,:,center);
    full = imclose(full, strel('disk',3));
    inner = imerode(full, strel('disk', 1));
    outer = imabsdiff(logical(full),logical(inner));
    
    side = erosion(:,:,center).*outer;

    dist = zeros(1,8*8);
    pos1 = zeros(2,8*8);
    pos2 = zeros(2,8*8);
    
    rp = regionprops(side,'Area', 'Extrema');
    rpArea = [rp.Area];
    [~, index] = max(rpArea);
    extrema = rp(index).Extrema;
    for j = 1:8
        for k = 1:8
            x1 = extrema(j,1);
            x2 = extrema(k,1);
            y1 = extrema(j,2); 
            y2 = extrema(k,2);
            pos1(1,j*8+k) = x1;
            pos1(2,j*8+k) = y1;
            pos2(1,j*8+k) = x2;
            pos2(2,j*8+k) = y2;
            dist(1,j*8+k) = sqrt((x1-x2)^2+(y1-y2)^2);     
        end
    end
    [len, maxIndex] = max(dist);
    pixel1 = pos1(:,maxIndex);
    pixel2 = pos2(:,maxIndex);
    slice = center;
    
    %%
    figure
    imshow(inner)
