function [volume,centroid,width,depth,slice] = quantification(erosion,imageSet)
%QUANTIFICATION Summary of this function goes here
%   Detailed explanation goes here

    rp = regionprops3(erosion);
    volume = rp.Volume;
    centroid = rp.Centroid;
    [width,pixel1,pixel2,slice] = widthCalculator2(erosion, imageSet);
    depth = depthCalculator(erosion, pixel1, pixel2, slice);

end

