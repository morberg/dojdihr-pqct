clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath ]));
addpath(genpath([data.gitPath, detPath ]));

list = dir([data.gitPath,'\radetect_src\Matlab\Detection\erosionQuanFinal']);

res.TP = zeros(1, size(list,1)-2);
res.FP = zeros(1, size(list,1)-2);
res.FN = zeros(1, size(list,1)-2);

res.MethodNr = 0;
res.TPAll = 0;
res.FPEro = 0;

TPInc = 1;
NrInc = 1;

for i = 3:size(list,1)

    load([data.gitPath,'\radetect_src\Matlab\Detection\erosionQuanFinal\', list(i).name]);
    
    res.TP(1,i-2) = results.TP;
    res.FP(1,i-2) = results.FP;
    res.FN(1,i-2) = results.FN;
    res.MethodNr = res.MethodNr + results.numEroMethod;
    res.TPAll = res.TPAll + results.TP;
    if results.TP > 1
        res.FPEro = res.FPEro + results.FP; 
    end
    
end

res.FPAll = sum(res.FP);
res.FNAll = sum(res.FN);

res.vMethod = zeros(1, res.TPAll);
res.vGold = zeros(1, res.TPAll);

res.wMethod = zeros(1, res.TPAll);
res.wGold = zeros(1, res.TPAll);

res.dMethod = zeros(1, res.TPAll);
res.dGold = zeros(1, res.TPAll);

res.xMethod = zeros(1, res.TPAll);
res.xGold = zeros(1, res.TPAll);

res.yMethod = zeros(1, res.TPAll);
res.yGold = zeros(1, res.TPAll);

res.zMethod = zeros(1, res.TPAll);
res.zGold = zeros(1, res.TPAll);

res.ero.vMethod = zeros(1, res.MethodNr);
res.ero.wMethod = zeros(1, res.MethodNr);
res.ero.dMethod = zeros(1, res.MethodNr);
res.ero.xMethod = zeros(1, res.MethodNr);
res.ero.yMethod = zeros(1, res.MethodNr);
res.ero.zMethod = zeros(1, res.MethodNr);

for i = 3:size(list,1)

    load([data.gitPath,'\radetect_src\Matlab\Detection\erosionQuanFinal\', list(i).name]);
    
    for j = 1:results.TP

        res.vMethod(1, TPInc) =results.detections(j).vMethod;
        res.vGold(1, TPInc) = results.detections(j).vGold;
                
        res.wMethod(1, TPInc) = results.detections(j).wMethod;
        res.wGold(1, TPInc) = results.detections(j).wGold;

        res.dMethod(1, TPInc) = results.detections(j).dMethod;
        res.dGold(1, TPInc) = results.detections(j).dGold;
                
        res.xMethod(1, TPInc) = results.detections(j).xMethod;
        res.xGold(1, TPInc) = results.detections(j).xGold;
                
        res.yMethod(1, TPInc) = results.detections(j).yMethod;
        res.yGold(1, TPInc) = results.detections(j).yGold;
                
        res.zMethod(1, TPInc) = results.detections(j).zMethod;
        res.zGold(1, TPInc) = results.detections(j).zGold;
        
        TPInc = TPInc + 1;
    end
    
    for j = 1:results.numEroMethod

        res.ero.vMethod(1, NrInc) = results.erosions(j).vMethod;
        res.ero.wMethod(1, NrInc) = results.erosions(j).wMethod;
        res.ero.dMethod(1, NrInc) = results.erosions(j).dMethod;
        res.ero.xMethod(1, NrInc) = results.erosions(j).xMethod;
        res.ero.yMethod(1, NrInc) = results.erosions(j).yMethod;
        res.ero.zMethod(1, NrInc) = results.erosions(j).zMethod;
        
        NrInc = NrInc + 1;
    end

end

res.TPR = sum(res.TP)/(sum(res.TP)+sum(res.FN));
res.PPV = sum(res.TP)/(sum(res.TP)+sum(res.FP));
res.PPVEro = sum(res.TP)/(sum(res.TP)+sum(res.FPEro));

res.dist = sqrt((res.xMethod - res.xGold).^2+(res.yMethod - res.yGold).^2+(res.zMethod - res.zGold).^2);
res.distReal = res.dist*0.082;
res.distAcc = mean(res.dist*0.082);
res.distPreReal = sqrt(mean((res.dist*0.082).^2));

res.wDiff = res.wGold - res.wMethod;
res.wDiffReal = res.wDiff * 0.082;
res.wDiffR = res.wDiffReal./(res.wGold*0.082)*100;
res.wAcc = mean(res.wGold* 0.082) - mean(res.wMethod* 0.082);
res.wAccR = mean(res.wDiffR);
res.wPreSD = sqrt(mean((res.wDiffReal).^2));
res.wPreSDR = sqrt(mean((res.wDiffR).^2));

res.dDiff = res.dGold - res.dMethod;
res.dDiffReal = res.dDiff * 0.082;
res.dDiffR = res.dDiffReal./(res.dGold*0.082)*100;
res.dAcc = mean(res.dGold* 0.082) - mean(res.dMethod* 0.082);
res.dAccR = mean(res.dDiffR);
res.dPreSD = sqrt(mean((res.dDiffReal).^2));
res.dPreSDR = sqrt(mean((res.dDiffR).^2));

res.vDiff = res.vGold - res.vMethod;
res.vDiffReal = res.vDiff * 0.082* 0.082* 0.082;
res.vDiffR = res.vDiffReal./(res.vGold*0.082* 0.082* 0.082)*100;
res.vAcc = mean(res.vGold* 0.082* 0.082* 0.082) - mean(res.vMethod* 0.082* 0.082* 0.082);
res.vAccR = mean(res.vDiffR);
res.vPreSD = sqrt(mean((res.vDiffReal).^2));
res.vPreSDR = sqrt(mean((res.vDiffR).^2));


%% Volume Error
name = 'volumeError';
maxVal = max([res.vMethod*0.082*0.082*0.082, res.vGold*0.082*0.082*0.082]);
line = linspace(1,maxVal+maxVal/20);
figure 
plot(res.vMethod*0.082*0.082*0.082,res.vGold*0.082*0.082*0.082,'*'); hold on;
plot(line, line); hold on 
xlim([0, maxVal+maxVal/20]);
ylim([0, maxVal+maxVal/20]);
xlabel('Proposed method volume (mm^3)')
ylabel('Golden reference volume (mm^3)')
saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])

%% Width Error
name = 'widthError';
maxVal = max([res.wMethod*0.082, res.wGold*0.082]);
line = linspace(0,maxVal+maxVal/20);
figure 
plot(res.wMethod*0.082,res.wGold*0.082,'*'); hold on;
plot(line, line); hold on 
xlim([0, maxVal+maxVal/20]);
ylim([0, maxVal+maxVal/20]);
xlabel('Proposed method width (mm)')
ylabel('Golden reference width (mm)')
saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])

%% Depth Error
name = 'depthError';
maxVal = max([res.dMethod*0.082, res.dGold*0.082]);
line = linspace(0,maxVal+maxVal/20);
figure 
plot(res.dMethod*0.082,res.dGold*0.082,'*'); hold on;
plot(line, line); hold on 
xlim([0, maxVal+maxVal/20]);
ylim([0, maxVal+maxVal/20]);
xlabel('Proposed method depth (mm)')
ylabel('Golden reference depth (mm)')
saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])

%% Distance Error
name = 'distanceError';

figure 
maxVal = max((res.dist*0.082));
x = 0:(size(res.dist,2)+1);
mean = ones(1,size(res.dist,2)+2)*res.distAcc*0.082;

plot(res.dist*0.082,'*'); hold on;

ylim([0, maxVal+maxVal/20]);
xlim([0,size(res.dist,2)+1]);

ylabel('Distance error (mm)')
xlabel('Erosion number')

saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])

%% Width Error Ratio
name = 'widthRatio';

figure 
xMax = max(res.wGold*0.082)+max(res.wGold*0.082)/10;
xMin = min(res.wGold*0.082)-max(res.wGold*0.082)/10;

plot(res.wGold*0.082,res.wDiffR,'*'); hold on;
plot([xMin,xMax],[0,0])

xlim([xMin,xMax]);

ylabel('Width error ratio (%)')
xlabel('Golden reference width (mm)')

saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])

%% Depth Error Raiio
name = 'depthRatio';

figure 
xMax = max(res.dGold*0.082)+max(res.dGold*0.082)/10;
xMin = min(res.dGold*0.082)-max(res.dGold*0.082)/10;

plot(res.dGold*0.082,res.dDiffR,'*'); hold on;
plot([xMin,xMax],[0,0])

xlim([xMin,xMax]);

ylabel('Depth error ratio (%)')
xlabel('Golden reference depth (mm)')

saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])

%% Volume Error
name = 'volumeRatio';

figure 
xMax = max(res.vGold*0.082*0.082*0.082)+max(res.vGold*0.082*0.082*0.082)/10;
xMin = min(res.vGold*0.082*0.082*0.082)-max(res.vGold*0.082*0.082*0.082)/10;

plot(res.vGold*0.082*0.082*0.082,res.vDiffR,'*'); hold on;
plot([xMin,xMax],[0,0])

xlim([xMin,xMax]);

ylabel('Volume error ratio (%)')
xlabel('Golden reference volume (mm^3)')

saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\',name,'.png'])