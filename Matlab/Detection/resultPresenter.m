clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath]));
addpath(genpath([data.gitPath, detPath ]));
fprintf("1 - done\n");
%%
data.setName = '16-2';
% The drive name of the harddrive
data.datapath = ['P:\results\',data.setName,'\'];
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitterv2(imageSet));

imageSet = logical(elementRemoval(imageSet));

%erosions = DetectionMethod(imageSet);
load(['erosionResults/erosions',data.setName,'.mat']);

[imageSetReturn,erosionsReturn] = registrationRetrans(imageSet,erosions, data.setName, data.gitPath);
%erosionsReturn = erosions;
%%volumeViewer(erosions)
fprintf("2 - done\n");
%% FINDING BORDERS
erosionsReturnBW = imbinarize(erosionsReturn);
[M,N,K] = size(erosionsReturn);
erosionBorder(:,:,:) = double(ones(M,N,330));
for i = 1:330
    erosionBorder(:,:,i) = imerode(erosionsReturnBW(:,:,i) ,strel('disk',1));
    erosionBorder(:,:,i) = erosionsReturnBW(:,:,i) - erosionBorder(:,:,i);
end
%%figure,imshow(erosionBorder(:,:,130));

%% LOADING OF ORIGINAL DATA
% The drive name of the harddrive
data.datapath = pathdata.datapath;

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data = preprocessing(data);


%% COMBINING BORDER WITH ORIGINAL
imdataBorder = data.imDataArrayAdj + im2uint16(erosionBorder);

%%figure, imshow(imdataBorder(:,:,1))

%%
volumeViewer(imdataBorder)

%% Example 2: TRUE pixels are red, FALSE pixels are black:
imgNr = 153;
name = [data.setName,'-',num2str(imgNr)];
RGB = double(erosionBorder(:,:,imgNr));
RGB(end, end, 3) = 0;

imdataBorder = data.imDataArrayAdj(:,:,imgNr) + im2uint16(RGB);
figure
imshow(imdataBorder)
saveas(gcf,[data.gitPath, '\radetect_latex\doc\figs\results\detection\visual\',name,'.png'])
