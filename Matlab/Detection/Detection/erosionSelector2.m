function erosions = erosionSelector(breaks,full)
%EROSIONSELECTOR Summary of this function goes here
%   Detailed explanation goes here
    rp = regionprops3(breaks);
    rpSize = size(rp,1);
    erosions = false(size(full));
    cc = bwconncomp(full);
    for i = 1:rpSize

        C = round(rp.Centroid(i,1));
        R = round(rp.Centroid(i,2));
        P = round(rp.Centroid(i,3));
        for j = 1:cc.NumObjects
            
            ero = false(size(breaks));
            ero(cc.PixelIdxList{j}) = 1;
            
            if ero(R,C,P) == 1
                erosions = erosions + ero;
            end
        end
    end
end

