function [BW3DDiff, BW3D] = objectFinder2(image)
%OBJECTFINDER Summary of this function goes here
%   Detailed explanation goes here

[M,N,O] = size(image);
strelSize = 20;

BW110 = false(size(image));
BW220 = false(size(image));
BW330 = false(size(image));

BW110(:,:,1:110) = image(:,:,1:110);
BW220(:,:,111:220) = image(:,:,111:220);
BW330(:,:,221:330) = image(:,:,221:330);


for i = 1:O

   BW110(:,:,i) = imclose(BW110(:,:,i), strel('disk', strelSize));

   BW220(:,:,i) = imclose(BW220(:,:,i), strel('disk', strelSize));

   BW330(:,:,i) = imclose(BW330(:,:,i), strel('disk', strelSize));

end
BWX = logical(BW110+BW220+BW330);

BW110 = false(size(image));
BW220 = false(size(image));
BW330 = false(size(image));

BW110(:,:,1:110) = image(:,:,1:110);
BW220(:,:,111:220) = image(:,:,111:220);
BW330(:,:,221:330) = image(:,:,221:330);

for i = 1:N
   
   BW110(:,i,:) = imclose(squeeze(BW110(:,i,:)), strel('disk', strelSize));

   BW220(:,i,:) = imclose(squeeze(BW220(:,i,:)), strel('disk', strelSize));

   BW330(:,i,:) = imclose(squeeze(BW330(:,i,:)), strel('disk', strelSize));

end
BWY = logical(BW110+BW220+BW330);

BW110 = false(size(image));
BW220 = false(size(image));
BW330 = false(size(image));

BW110(:,:,1:110) = image(:,:,1:110);
BW220(:,:,111:220) = image(:,:,111:220);
BW330(:,:,221:330) = image(:,:,221:330);

for i = 1:M
   
   BW110(i,:,:) = imclose(squeeze(BW110(i,:,:)), strel('disk', strelSize));

   BW220(i,:,:) = imclose(squeeze(BW220(i,:,:)), strel('disk', strelSize));

   BW330(i,:,:) = imclose(squeeze(BW330(i,:,:)), strel('disk', strelSize));

end
BWZ = logical(BW110+BW220+BW330);

BW3D = logical(BWX+BWY+BWZ);

BWXDiff = imabsdiff(BWX, image);
BWYDiff = imabsdiff(BWY, image);
BWZDiff = imabsdiff(BWZ, image);

BW3DDiff = BWXDiff.*BWYDiff + BWXDiff.*BWZDiff + BWYDiff.*BWZDiff;
BW3DDiff = bwmorph3(BW3DDiff,'clean');
BW3DDiff = bwmorph3(BW3DDiff,'majority');

end

