function finalErosions = removeOuterObjects(erosions,imageSet)
%REMOVEOUTEROBJECTS Summary of this function goes here
%   Detailed explanation goes here
    eor = bwareaopen(erosions, 100);
    cc = bwconncomp(eor);

    finalErosions = false(size(eor));

    for i = 1:cc.NumObjects
        ero = false(size(eor));
        ero(cc.PixelIdxList{i}) = 1;

        [~,pixel1,pixel2,slice] = widthCalculator2(ero, imageSet);
        [~,point] = depthCalculator(ero, pixel1, pixel2, slice);

        combined = logical(imageSet + ero);
        rpSlice = regionprops(combined(:,:,slice));

        for j =  1:size(rpSlice, 1)

            if point(1) > rpSlice(j).BoundingBox(1)...
                && point(1) < rpSlice(j).BoundingBox(1) + rpSlice(j).BoundingBox(3)...
                && point(2) > rpSlice(j).BoundingBox(2)...
                && point(2) < rpSlice(j).BoundingBox(2) + rpSlice(j).BoundingBox(4)

                lenght1 = sqrt((point(1)-rpSlice(j).Centroid(1))^2+(point(2)-rpSlice(j).Centroid(2))^2);   
                lenght2 = sqrt((pixel1(1)-rpSlice(j).Centroid(1))^2+(pixel1(2)-rpSlice(j).Centroid(2))^2); 
                lenght3 = sqrt((pixel2(1)-rpSlice(j).Centroid(1))^2+(pixel2(2)-rpSlice(j).Centroid(2))^2); 

                if max(lenght2,lenght3) > lenght1
                   finalErosions = finalErosions + ero;
                end
            end
        end
    end
end

