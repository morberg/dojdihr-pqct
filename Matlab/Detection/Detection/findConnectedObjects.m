function erosionsRefined = findConnectedObjects(erosions,imageSet)
%FINDCONNECTEDOBJECTS2 Summary of this function goes here
%   Detailed explanation goes here
    full = imclose(imageSet, strel('cube',50));

    erosionsFull = imabsdiff(full, imageSet);

    erosionsFull = bwmorph3(erosionsFull, 'majority');

    erosionsFull = imopen(erosionsFull, strel('cube', 10));

    erosionsFull = bwareaopen(erosionsFull, 200);

    fullRefined  = logical(imageSet + erosionsFull);

    for i = 1:330

        fullRefined(:,:,i) = imfill(fullRefined(:,:,i), 'holes');

    end

    erosionRefine = imabsdiff(fullRefined, imageSet);

    cc = bwconncomp(erosionRefine);
    rp = regionprops3(logical(erosions));

    finalObject = false(size(erosions));

    for i = 1:cc.NumObjects

        object = false(size(erosions));
        object(cc.PixelIdxList{i}) = 1;

        numErosions = 0;

        for j = 1:size(rp,1)

            if object(round(rp.Centroid(j,2)),round(rp.Centroid(j,1)),round(rp.Centroid(j,3))) == 1
                numErosions = numErosions +1;
            end
        end

        if numErosions > 1

            finalObject = finalObject + object;

        end

    end

    erosionsRefined = logical(erosions + finalObject);

end

