function [BW3DDiff, BW3D] = objectFinder(image)
%OBJECTFINDER Summary of this function goes here
%   Detailed explanation goes here

[M,N,O] = size(image);
strelSize = 20;

BWX = image;
BWY = image;
BWZ = image;

for i = 1:O
   BWX(:,:,i) = imclose(BWX(:,:,i), strel('disk', strelSize)); 
end

for i = 1:N
   BWY(:,i,:) = imclose(squeeze(BWY(:,i,:)), strel('disk', strelSize)); 
end

for i = 1:M
   BWZ(i,:,:) = imclose(squeeze(BWZ(i,:,:)), strel('disk', strelSize)); 
end

BW3D = logical(BWX+BWY+BWZ);

BWXDiff = imabsdiff(BWX, image);
BWYDiff = imabsdiff(BWY, image);
BWZDiff = imabsdiff(BWZ, image);

BW3DDiff = BWXDiff.*BWYDiff.*BWZDiff;
BW3DDiff = bwmorph3(BW3DDiff,'clean');
BW3DDiff = bwmorph3(BW3DDiff,'majority');

end

