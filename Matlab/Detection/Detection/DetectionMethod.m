function erosions = DetectionMethod(imageSet)
%DETECTIONMETHOD Summary of this function goes here
%   Detailed explanation goes here

% Size of dataset
cc = bwconncomp(imageSet);

M2 = false(size(imageSet));
M3 = false(size(imageSet));
M4 = false(size(imageSet));
M5 = false(size(imageSet));

P2 = false(size(imageSet));
P3 = false(size(imageSet));
P4 = false(size(imageSet));
P5 = false(size(imageSet));

P2(cc.PixelIdxList{4}) = 1;
rp = regionprops3(P2);
P2(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;

P3(cc.PixelIdxList{3}) = 1;
rp = regionprops3(P3);
P3(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;

P4(cc.PixelIdxList{2}) = 1;
rp = regionprops3(P4);
P4(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;

P5(cc.PixelIdxList{1}) = 1;
rp = regionprops3(P5);
P5(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;


M2(cc.PixelIdxList{5}) = 1;
rp = regionprops3(M2);
M2(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

M3(cc.PixelIdxList{6}) = 1;
rp = regionprops3(M3);
M3(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

M4(cc.PixelIdxList{7}) = 1;
rp = regionprops3(M4);
M4(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

M5(cc.PixelIdxList{8}) = 1;
rp = regionprops3(M5);
M5(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

% Erosion detector

M2Erosions = objectFinder2(M2);
disp('M2')
M3Erosions = objectFinder2(M3);
disp('M3')
M4Erosions = objectFinder2(M4);
disp('M4')
M5Erosions = objectFinder2(M5);
disp('M5')

P2Erosions = objectFinder2(P2);
disp('P2')
P3Erosions = objectFinder2(P3);
disp('P3')
P4Erosions = objectFinder2(P4);
disp('P4')
P5Erosions = objectFinder2(P5);
disp('P5')

Breaks = logical(M2Erosions + M3Erosions + M4Erosions + M5Erosions +P2Erosions +P3Erosions + P4Erosions + P5Erosions);

FullStructure  = logical(imageSet + Breaks);
FullStructure  = imclose(FullStructure, strel('sphere',1));

[CorticalBreaks,~]= CorticalBreakFinder(FullStructure, Breaks, 100);

erosions = imdilate(CorticalBreaks, strel('sphere',4));
erosions = imsubtract(erosions, imageSet);
erosions(erosions == -1) = 0;

erosions = bwareaopen(erosions, 100);

erosions = findConnectedObjects(erosions,logical(P2+P3+P4+P5));
erosions = findConnectedObjects(erosions,logical(M2+M3+M4+M5));

erosions = removeOuterObjects(erosions,imageSet);

end

