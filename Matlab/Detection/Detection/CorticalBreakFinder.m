function [CorticalBreaks,Cortical] = CorticalBreakFinder(full, breaks, size)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    % Cortical border
    Cortical = imerode(full, strel('sphere',4));
    Cortical = logical(imabsdiff(full,Cortical));

    % Cortical breaks

    CorticalBreaks = imsubtract(breaks,Cortical);
    CorticalBreaks(CorticalBreaks == -1) = 0;

    CorticalBreaks = bwmorph3(CorticalBreaks,'clean');
    CorticalBreaks = bwmorph3(CorticalBreaks,'majority');
    CorticalBreaks = bwareaopen(CorticalBreaks, size);
end

