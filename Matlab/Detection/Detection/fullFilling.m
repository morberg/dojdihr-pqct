function BW4 = fullFilling(image)
%FULLFILLING Summary of this function goes here
%   Detailed explanation goes here

    BW1 = image;
    BW1(:,:,1:110)   = imclose(image(:,:,1:110), strel('cube',50));
    BW1(:,:,111:220) = imclose(image(:,:,111:220), strel('cube',50));
    BW1(:,:,221:330) = imclose(image(:,:,221:330), strel('cube',50));

    BW2 = imabsdiff(BW1, image);
    BW3 = bwmorph3(BW2,'clean');
    BW3 = bwmorph3(BW3,'majority');
    BW3 = bwareaopen(BW3, 100);
    BW4 = imopen(BW3, strel('sphere', 2));

end

