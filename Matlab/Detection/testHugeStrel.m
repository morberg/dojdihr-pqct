clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath] ));
addpath(genpath([data.gitPath, detPath] ));

data.setName = '31-3';
reg = 1;

data.datapath = ['P:\results\',data.setName,'\'];

load([data.gitPath,'radetect_src\Results\ResultsManuelStruct.mat']);
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitterv2(imageSet));

imageSet = logical(elementRemoval(imageSet));

load([pathdata.gitpath,'radetect_src\Matlab\Detection\erosionResults\erosions',data.setName,'.mat'])
erosions = bwareaopen(erosions, 100);

if reg == 1
    [imageSetReturn,erosionsReturn] = registrationRetrans(imageSet,erosions, data.setName, data.gitPath);
    erosionsReturn = bwmorph3(erosionsReturn, 'majority');
else
    imageSetReturn = imageSet;
    erosionsReturn = erosions;
end

volumeViewer(double(imageSetReturn)*0.5+double(erosionsReturn)*1)

%%
image = 160;
figure 
imshow(double(imageSetReturn(:,:,image))*0.5+double(erosionsReturn(:,:,image))*1)
