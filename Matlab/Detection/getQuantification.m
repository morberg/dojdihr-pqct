clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath ]));
addpath(genpath([data.gitPath, detPath ]));

data.setName = '31-3';
reg = 1;

data.datapath = ['P:\results\',data.setName,'\'];
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitterv2(imageSet));

imageSet = logical(elementRemoval(imageSet));

load([pathdata.gitpath,'radetect_src\Matlab\Detection\erosionResults\erosions',data.setName,'.mat'])

    if 1 == exist('results')
        clear results;
    end     
    
    if reg == 1
        [imageSetReturn,erosionsReturn] = registrationRetrans(imageSet,erosions, data.setName, data.gitPath);
        erosionsReturn = bwmorph3(erosionsReturn, 'majority');
    else
        imageSetReturn = imageSet;
        erosionsReturn = erosions;
    end
    [xGold,yGold,zGold,wGold,dGold,vGold] = getManuelData(data.setName,data.gitPath);
    results.numEroGold = size(xGold,2);
    detected = 0;

    cc = bwconncomp(erosionsReturn, 26);
    results.numEroMethod = cc.NumObjects;
    for i = 1:results.numEroMethod

        erosion = false(size(erosionsReturn));
        erosion(cc.PixelIdxList{i}) = 1;

        [vMethod,centroid,wMethod,dMethod,~] = quantification(erosion,imageSetReturn);

        ero.vMethod = vMethod;
        ero.vMethodReal = vMethod*0.082*0.082*0.082;

        ero.wMethod = wMethod;
        ero.wMethodReal = wMethod*0.082;
  
        ero.dMethod = dMethod;
        ero.dMethodReal = dMethod*0.082;

        ero.xMethod = centroid(1);
        ero.xMethodReal = centroid(1)*0.082;

        ero.yMethod = centroid(2);
        ero.yMethodReal = centroid(2)*0.082;

        ero.zMethod = centroid(3);
        ero.zMethodReal = centroid(3)*0.082;

        if 1 == isfield(results, 'erosions')
           results.erosions = [results.erosions,ero]; 
        else
           results.erosions = ero;
        end     
        
        for j = 1:results.numEroGold

            if(erosion(yGold(1,j),xGold(1,j),zGold(1,j)) == 1)

                detected = detected + 1;

                [vMethod,centroid,wMethod,dMethod,~] = quantification(erosion,imageSetReturn);

                stat.vMethod = vMethod;
                stat.vGold = vGold(1,j);
                stat.vMethodReal = vMethod*0.082*0.082*0.082;
                stat.vGoldReal = vGold(1,j)*0.082*0.082*0.082;
                stat.vDiff = abs(vMethod-vGold(1,j));
                stat.vDiffReal = abs(stat.vMethodReal - stat.vGoldReal);

                stat.wMethod = wMethod;
                stat.wGold = wGold(1,j);
                stat.wMethodReal = wMethod*0.082;
                stat.wGoldReal = wGold(1,j)*0.082;
                stat.wDiff = abs(wMethod-wGold(1,j));
                stat.wDiffReal = abs(stat.wMethodReal - stat.wGoldReal);

                stat.dMethod = dMethod;
                stat.dGold = dGold(1,j);
                stat.dMethodReal = dMethod*0.082;
                stat.dGoldReal = dGold(1,j)*0.082;
                stat.dDiff = abs(dMethod-dGold(1,j));
                stat.dDiffReal = abs(stat.dMethodReal - stat.dGoldReal);

                stat.xMethod = centroid(1);
                stat.xGold = xGold(1,j);
                stat.xMethodReal = centroid(1)*0.082;
                stat.xGoldReal = xGold(1,j)*0.082;
                stat.xDiff = abs(centroid(1)-xGold(1,j));
                stat.xDiffReal = abs(stat.xMethodReal - stat.xGoldReal);

                stat.yMethod = centroid(2);
                stat.yGold = yGold(1,j);
                stat.yMethodReal = centroid(2)*0.082;
                stat.yGoldReal = yGold(1,j)*0.082;
                stat.yDiff = abs(centroid(2)-yGold(1,j));
                stat.yDiffReal = abs(stat.yMethodReal - stat.yGoldReal);

                stat.zMethod = centroid(3);
                stat.zGold = zGold(1,j);
                stat.zMethodReal = centroid(3)*0.082;
                stat.zGoldReal = zGold(1,j)*0.082;
                stat.zDiff = abs(centroid(3)-zGold(1,j));
                stat.zDiffReal = abs(stat.zMethodReal - stat.zGoldReal);

                if 1 == isfield(results, 'detections')
                   results.detections = [results.detections,stat]; 
                else
                   results.detections = stat;
                end     
            end
        end
    end

    results.TP = detected;
    results.FP = results.numEroMethod - detected;
    results.FN = results.numEroGold - detected;
    results.TPR = results.TP/(results.TP+results.FN);
    results.PPV = results.TP/(results.TP+results.FP);

    save([data.gitPath,'radetect_src\Matlab\Detection\erosionDetectAndQuanResults\',data.setName,'.mat'], 'results')
%%
load([data.gitPath,'radetect_src\Results\ResultsManuelStruct.mat']);
image = 169;
figure 
imshow(double(imageSetReturn(:,:,image))*0.5+double(erosionsReturn(:,:,image))*1)