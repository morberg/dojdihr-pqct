function [imageSetReturn,erosionsReturn] = registrationRetrans(imageSet,erosions, setName,gitPath)
%REGISTRATIONRETRANS Summary of this function goes here
%   Detailed explanation goes here
    load([gitPath,'radetect_src\Matlab\transResults.mat'])
    
    ccAll = bwconncomp(imageSet+erosions);
    cb_ref = imref2d(size(imageSet(:,:,1)));

    names = {results.set};
    index = find(strcmp(names, setName));

    trans1 = results(index).t1;
    trans2 = results(index).t2;

    MCP2 = false(size(imageSet));
    MCP3 = false(size(imageSet));
    MCP4 = false(size(imageSet));
    MCP5 = false(size(imageSet));

    MCP2(ccAll.PixelIdxList{4}) = 1;
    MCP2(ccAll.PixelIdxList{5}) = 1;
    MCP2Ero = imsubtract(MCP2,imageSet);
    MCP2Ero(MCP2Ero == -1) = 0;

    MCP3(ccAll.PixelIdxList{3}) = 1;
    MCP3(ccAll.PixelIdxList{6}) = 1;
    MCP3Ero = imsubtract(MCP3,imageSet);
    MCP3Ero(MCP3Ero == -1) = 0;

    MCP4(ccAll.PixelIdxList{2}) = 1;
    MCP4(ccAll.PixelIdxList{7}) = 1;
    MCP4Ero = imsubtract(MCP4,imageSet);
    MCP4Ero(MCP4Ero == -1) = 0;

    MCP5(ccAll.PixelIdxList{1}) = 1;
    MCP5(ccAll.PixelIdxList{8}) = 1;
    MCP5Ero = imsubtract(MCP5,imageSet);
    MCP5Ero(MCP5Ero == -1) = 0;

    for i = 111:220

        MCP2(:,:,i) = imwarp(MCP2(:,:,i),invert(trans1.tform4),'OutputView', cb_ref);
        MCP2Ero(:,:,i) = imwarp(MCP2Ero(:,:,i),invert(trans1.tform4),'OutputView', cb_ref);

        MCP3(:,:,i) = imwarp(MCP3(:,:,i),invert(trans1.tform3),'OutputView', cb_ref);
        MCP3Ero(:,:,i) = imwarp(MCP3Ero(:,:,i),invert(trans1.tform3),'OutputView', cb_ref);

        MCP4(:,:,i) = imwarp(MCP4(:,:,i),invert(trans1.tform2),'OutputView', cb_ref);
        MCP4Ero(:,:,i) = imwarp(MCP4Ero(:,:,i),invert(trans1.tform2),'OutputView', cb_ref);

        MCP5(:,:,i) = imwarp(MCP5(:,:,i),invert(trans1.tform1),'OutputView', cb_ref);
        MCP5Ero(:,:,i) = imwarp(MCP5Ero(:,:,i),invert(trans1.tform1),'OutputView', cb_ref);

    end

    for i = 221:330

        MCP2(:,:,i) = imwarp(MCP2(:,:,i),invert(trans2.tform4),'OutputView', cb_ref);
        MCP2Ero(:,:,i) = imwarp(MCP2Ero(:,:,i),invert(trans2.tform4),'OutputView', cb_ref);

        MCP3(:,:,i) = imwarp(MCP3(:,:,i),invert(trans2.tform3),'OutputView', cb_ref);
        MCP3Ero(:,:,i) = imwarp(MCP3Ero(:,:,i),invert(trans2.tform3),'OutputView', cb_ref);

        MCP4(:,:,i) = imwarp(MCP4(:,:,i),invert(trans2.tform2),'OutputView', cb_ref);
        MCP4Ero(:,:,i) = imwarp(MCP4Ero(:,:,i),invert(trans2.tform2),'OutputView', cb_ref);

        MCP5(:,:,i) = imwarp(MCP5(:,:,i),invert(trans2.tform1),'OutputView', cb_ref);
        MCP5Ero(:,:,i) = imwarp(MCP5Ero(:,:,i),invert(trans2.tform1),'OutputView', cb_ref);

    end
    
    imageSetReturn = MCP2+MCP3+MCP4+MCP5;
    erosionsReturn = MCP2Ero+MCP3Ero+MCP4Ero+MCP5Ero;
end
