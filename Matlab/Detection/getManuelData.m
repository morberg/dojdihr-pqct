function [x,y,z,w,d,v] = getManuelData(setName,gitpath)
%GETMANUELDATA Summary of this function goes here
%   Detailed explanation goes here
    load([gitpath,'radetect_src\Results\ResultsManuelStruct.mat']);
    setResultName = strrep(setName, '-','_');

    fields = result.(['set_',setResultName]);

    numAct = 0;
    numScl = 0;
    numPas = 0;

    if 1 == isfield(fields, 'active')
       numAct = size(fields.active,2) ;
    end

    if 1 == isfield(fields, 'sclerosis')
       numScl = size(fields.sclerosis,2);
    end

    if 1 == isfield(fields, 'passiv')
       numPas = size(fields.passiv,2);
    end

    sizeAll = numAct+numScl+numPas;

    x = zeros(1,sizeAll);
    y = zeros(1,sizeAll);
    z = zeros(1,sizeAll);
    w = zeros(1,sizeAll);
    d = zeros(1,sizeAll);
    v = zeros(1,sizeAll);

    for i = 1:numAct

       x(1,i) = fields.active(i).xPixel;
       y(1,i) = fields.active(i).yPixel;
       z(1,i) = fields.active(i).zPixel;
       w(1,i) = fields.active(i).widthPixel;
       d(1,i) = fields.active(i).heightPixel;
       v(1,i) = fields.active(i).volumePixel;

    end

    for i = 1:numScl

       x(1,i+numAct) = fields.sclerosis(i).xPixel;
       y(1,i+numAct) = fields.sclerosis(i).yPixel;
       z(1,i+numAct) = fields.sclerosis(i).zPixel;
       w(1,i+numAct) = fields.sclerosis(i).widthPixel;
       d(1,i+numAct) = fields.sclerosis(i).heightPixel;
       v(1,i+numAct) = fields.sclerosis(i).volumePixel;

    end

    for i = 1:numPas

       x(1,i+numAct+numScl) = fields.passiv(i).xPixel;
       y(1,i+numAct+numScl) = fields.passiv(i).yPixel;
       z(1,i+numAct+numScl) = fields.passiv(i).zPixel;
       w(1,i+numAct+numScl) = fields.passiv(i).widthPixel;
       d(1,i+numAct+numScl) = fields.passiv(i).heightPixel;
       v(1,i+numAct+numScl) = fields.passiv(i).volumePixel;

    end
end

