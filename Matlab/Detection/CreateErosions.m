clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, regPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));
addpath(genpath([gitPath, detPath ]));

data.setName = '1-3';
% The drive name of the harddrive
data.datapath = ['P:\results\',data.setName,'\'];
% The name of the set to be validated

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 1;
% Select to save the individual segmented joints
data.saveJointsSeg = 1;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 1; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.step = 'detecting';

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitter(imageSet));

imageSet = logical(elementRemoval(imageSet));

erosions = DetectionMethod(imageSet);

save([pathdata.gitpath,'radetect_src\Matlab\Detection\erosionResults\erosions',data.setName,'.mat'], 'erosions')

