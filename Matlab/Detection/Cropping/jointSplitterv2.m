function imageSet = jointSplitterv2(imageSet)
%JOINTSPLITTER Summary of this function goes here
%   Detailed explanation goes here

    cc = bwconncomp(imageSet);
    rp = regionprops3(imageSet);   
    for i = 1:size(rp,1)    
        if rp.BoundingBox(i,3) == 0.5 && rp.BoundingBox(i,6) == 330
            mcp = false(size(imageSet));
            mcp(cc.PixelIdxList{i}) = 1;

            x = zeros(1,330);
            y = zeros(1,330);
            area = zeros(1,330);

            for j = 1:330
                rp2 = regionprops(mcp(:,:,j));
                [~,elem] = max([rp2.Area]);
                x(1,j) = rp2(elem).Centroid(1);
                y(1,j) = rp2(elem).Centroid(2);
                area(1,j) = rp2(elem).Area;
            end

            xDiff = diff(x);
            yDiff = diff(y);
            areaDiff = diff(area);

            [~,slicex1] = max(xDiff);
            [~,slicex2] = min(xDiff);

            [~,slicey1] = min(yDiff);
            [~,slicey2] = max(yDiff);

            [~,sliceArea1] = min(areaDiff);
            [~,sliceArea2] = max(areaDiff);

            mem = ismember([slicex1,slicex2],[slicey1,slicey2]);
            if sum(mem) == 1
                if mem(1,1) == 1
                    slice = slicex1;
                else
                    slice = slicex2;
                end
            else
                mem = ismember([sliceArea1,sliceArea2],[slicey1,slicey2,slicex2,slicex1]);
                if mem(1,1) == 1
                    slice = sliceArea1;
                elseif mem(1,2) == 1
                    slice = sliceArea2;
                else
                    array = sort([sliceArea1,sliceArea2,slicey1,slicey2,slicex2,slicex1]);
                    arrayInc = zeros(1,5);
                    for k = 2:6
                        arrayInc(1,k-1) = array(1,k)-array(1,k-1);
                    end
                    [~,pos] = min(arrayInc);
                    slice = array(pos);
                end
            end

            mcp(:,:,slice-1:slice+1) = 0;

            set = imageSet;
            set(cc.PixelIdxList{i}) = 0;
            set = set + mcp;
            imageSet = set; 

        end
    end
end


