function imageSet = elementRemoval(imageSet)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
    cc = bwconncomp(imageSet);
    rp = regionprops3(imageSet);

    if size(rp,1) > 8
        volume = zeros(1,size(rp,1));
        for i = 1:size(rp,1)
            volume(1,i) = rp.Volume(i);
        end
        [~, max_ids] = sort(volume, 'descend'); 

        set = false(size(imageSet));

        for i = 1:8
            element = false(size(imageSet));
            element(cc.PixelIdxList{max_ids(i)}) = 1;
            set = set + element;
        end
        
        imageSet = set;
    end
end

