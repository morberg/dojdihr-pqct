clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath ]));
addpath(genpath([data.gitPath, detPath ]));

data.setName = '3-3';
% The drive name of the harddrive
data.datapath = ['P:\results\',data.setName,'\'];
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitter(imageSet));

imageSet = logical(elementRemoval(imageSet));

cc = bwconncomp(imageSet);

M2 = false(size(imageSet));
M3 = false(size(imageSet));
M4 = false(size(imageSet));
M5 = false(size(imageSet));

P2 = false(size(imageSet));
P3 = false(size(imageSet));
P4 = false(size(imageSet));
P5 = false(size(imageSet));

P2(cc.PixelIdxList{4}) = 1;
P3(cc.PixelIdxList{3}) = 1;
P4(cc.PixelIdxList{2}) = 1;
P5(cc.PixelIdxList{1}) = 1;

M2(cc.PixelIdxList{5}) = 1;
M3(cc.PixelIdxList{6}) = 1;
M4(cc.PixelIdxList{7}) = 1;
M5(cc.PixelIdxList{8}) = 1;

%%
volumeViewer(P3)

%%
x = zeros(1,330);
y = zeros(1,330);

for i = 1:330
    rp = regionprops(P3(:,:,i));
    if size(rp,1) > 0
        vol = [rp.Area];
        [~,index] = max(vol);
        x(1,i) = rp(index).Centroid(1);
        y(1,i) = rp(index).Centroid(2);
    else
            x(1,i) = 0;
            y(1,i) = 0;
    end

end

xDiff = diff(x);
yDiff = diff(y);

[minDiff, index] = min(yDiff + xDiff);

figure
plot(x+y); 

figure
plot(xDiff);

figure
plot(yDiff);