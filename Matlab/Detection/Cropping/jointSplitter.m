function imageSet = jointSplitter(imageSet)
%JOINTSPLITTER Summary of this function goes here
%   Detailed explanation goes here

    cc = bwconncomp(imageSet);
    rp = regionprops3(imageSet);   
    
    for i = 1:size(rp,1)

        if rp.BoundingBox(i,3) == 0.5 && rp.BoundingBox(i,6) == 330

            element = false(size(imageSet));
            element(cc.PixelIdxList{i}) = 1;

            sums = sum(sum(element,1),2);
            [~,maxSlice] = min(sums);

            sumJoint = sums(max(maxSlice-20,1):min(maxSlice+20,330));

            elem1 = 1;
            elem2 = size(sumJoint,3)-1;
            
            while(sumJoint(elem1+1)<sumJoint(elem1) && elem1 < 40)
                elem1 = elem1 +1;
            end
            
            while(sumJoint(elem2+1) > sumJoint(elem2) && elem2 > 1)
                elem2 = elem2 -1;
            end
            
            minSlice = max(maxSlice-20+elem1,1);
            maxSlice = min(maxSlice-20+elem2,330);
  

            element(:,:,minSlice:maxSlice) = 0;

            set = imageSet;
            set(cc.PixelIdxList{i}) = 0;
            set = set + element;
            imageSet = set; 
        end
    end
end

