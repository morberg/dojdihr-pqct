function imageSet = ringRemoval(imageSet)
%RINGREMOVAL Summary of this function goes here
%   Detailed explanation goes here

    [M,N] = size(imageSet(:,:,1));

    for j = 1:330
        sumVec = sum(imageSet(:,:,j));

        sequence = 0;
        seq = zeros(size(sumVec));

        for i = 1:N

            if sumVec(i) < 6 && sumVec(i) > 0

                sequence = sequence + 1;
                seq(i) = sequence;
            else 
                sequence = 0;
            end

        end

        [maxSeq, index] = max(seq);

        if maxSeq > 20

            s1 = index-maxSeq+2;
            s2 = index-2;

            im = imageSet(:,:,j);
            im(1:M, s1:s2) = 0;
            imageSet(:,:,j) = im;

            disp(['Image: ' num2str(j)]);

        end
    end
end

