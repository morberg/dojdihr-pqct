clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath] ));
addpath(genpath([data.gitPath, detPath] ));

data.setName = '9-2';
% The drive name of the harddrive
data.datapath = ['P:\results\',data.setName,'\'];
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);
%%
cc = bwconncomp(imageSet);
rp = regionprops3(imageSet);   
for i = 1:size(rp,1)    
    if rp.BoundingBox(i,3) == 0.5 && rp.BoundingBox(i,6) == 330
        mcp = false(size(imageSet));
        mcp(cc.PixelIdxList{i}) = 1;
        
        x = zeros(1,330);
        y = zeros(1,330);
        area = zeros(1,330);

        for j = 1:330
            rp2 = regionprops(mcp(:,:,j));
            [~,elem] = max([rp2.Area]);
            x(1,j) = rp2(elem).Centroid(1);
            y(1,j) = rp2(elem).Centroid(2);
            area(1,j) = rp2(elem).Area;
        end

        xDiff = diff(x);
        yDiff = diff(y);
        areaDiff = diff(area);

        [~,slicex1] = max(xDiff);
        [~,slicex2] = min(xDiff);

        [~,slicey1] = min(yDiff);
        [~,slicey2] = max(yDiff);
        
        [~,sliceArea1] = min(areaDiff);
        [~,sliceArea2] = max(areaDiff);

        mem = ismember([slicex1,slicex2],[slicey1,slicey2]);
        if sum(mem) == 1
            if mem(1,1) == 1
                slice = slicex1;
            else
                slice = slicex2;
            end
        else
            mem = ismember([sliceArea1,sliceArea2],[slicey1,slicey2,slicex2,slicex1]);
            if mem(1,1) == 1
                slice = sliceArea1;
            elseif mem(1,2) == 1
                slice = sliceArea2;
            else
                array = sort([sliceArea1,sliceArea2,slicey1,slicey2,slicex2,slicex1]);
                arrayInc = zeros(1,5);
                for k = 2:6
                    arrayInc(1,k-1) = array(1,k)-array(1,k-1);
                end
                [minInc,pos] = min(arrayInc);
                slice = array(pos);
            end
        end
        
        mcp(:,:,slice-1:slice+1) = 0;

        set = imageSet;
        set(cc.PixelIdxList{i}) = 0;
        set = set + mcp;
        imageSet = set; 
        
    end
end

%%
volumeViewer(imageSet)


