clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

data.gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([data.gitPath, segPath ]));
addpath(genpath([data.gitPath, regPath ]));
addpath(genpath([data.gitPath, prePath ]));
addpath(genpath([data.gitPath, postPath]));
addpath(genpath([data.gitPath, valPath ]));
addpath(genpath([data.gitPath, detPath ]));

data.setName = '4-1';
% The drive name of the harddrive
data.datapath = ['G:\results\',data.setName,'\'];
% The name of the set to be validated

data = loadSegmented(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitterv2(imageSet));

imageSet = logical(elementRemoval(imageSet));
fprintf("1 - done\n");
%%
cc = bwconncomp(imageSet);

M2 = false(size(imageSet));
M3 = false(size(imageSet));
M4 = false(size(imageSet));
M5 = false(size(imageSet));

P2 = false(size(imageSet));
P3 = false(size(imageSet));
P4 = false(size(imageSet));
P5 = false(size(imageSet));

P2(cc.PixelIdxList{4}) = 1;
rp = regionprops3(P2);
P2(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;

P3(cc.PixelIdxList{3}) = 1;
rp = regionprops3(P3);
P3(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;

P4(cc.PixelIdxList{2}) = 1;
rp = regionprops3(P4);
P4(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;

P5(cc.PixelIdxList{1}) = 1;
rp = regionprops3(P5);
P5(:,:,max((rp.BoundingBox(1,6)-20),1):rp.BoundingBox(1,6)) = 0;


M2(cc.PixelIdxList{5}) = 1;
rp = regionprops3(M2);
M2(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

M3(cc.PixelIdxList{6}) = 1;
rp = regionprops3(M3);
M3(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

M4(cc.PixelIdxList{7}) = 1;
rp = regionprops3(M4);
M4(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;

M5(cc.PixelIdxList{8}) = 1;
rp = regionprops3(M5);
M5(:,:,rp.BoundingBox(1,3):min((rp.BoundingBox(1,3)+20),330)) = 0;
fprintf("2 - done\n");
%% 0
M = makehgtform('scale',2);

volumeViewer(M2,M)
%%
% Erosion detector

M2Erosions = objectFinder2(M2);
disp('M2')


Breaks = logical(M2Erosions);

%% 1

M = makehgtform('scale',2);

volumeViewer(M2Erosions,M)
%%
FullStructure  = logical(M2 + Breaks);
FullStructure  = imclose(FullStructure, strel('sphere',1));

[CorticalBreaks,border]= CorticalBreakFinder(FullStructure, Breaks, 100); % border

%% 2
M = makehgtform('scale',2);

volumeViewer(border,M)

%%

erosions = imdilate(CorticalBreaks, strel('sphere',4));
erosions = imsubtract(erosions, imageSet);
erosions(erosions == -1) = 0;

erosions = bwareaopen(erosions, 100);
%% 3
M = makehgtform('scale',2);

volumeViewer(erosions,M)

%%
%erosions = findConnectedObjects(erosions,logical(P2+P3+P4+P5));
erosions = findConnectedObjects(erosions,logical(M2));

erosions = removeOuterObjects(erosions,imageSet);

%% 4
M = makehgtform('scale',2);

volumeViewer(erosions,M)
