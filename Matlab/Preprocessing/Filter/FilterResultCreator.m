clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'None';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;


data = preprocessing(data);
imAdj = data.imDataArrayAdj;

data = histAnalysis(data);


%%

if strcmp(data.seg, 'Median3D333')
    steps = 3;
    thresh = 0.85;
elseif strcmp(data.seg, 'Median2D33')
    steps = 3;
    thresh = 0.85;
elseif strcmp(data.seg, 'Wiener33')
    steps = 3;
    thresh = 0.851;
elseif strcmp(data.seg, 'Wiener55')
    steps = 5;
    thresh = 0.87;
else
    steps = 1;
    thresh = 0.87;
end

for i = 1:size(steps,2)
    
    tic
    
    if strcmp(data.seg, 'Median3D333')
        data = median3DFilter(data, steps(i));
    elseif strcmp(data.seg, 'Median2D33')
        data = median2DFilter(data, steps(i));
    elseif strcmp(data.seg, 'Wiener33')
        data = wienerFilter(data, steps(i));      
    elseif strcmp(data.seg, 'Wiener55')
        data = wienerFilter(data, steps(i));     
    else
        steps = 1;
    end 
    
    time = toc;
    
    data = globalThresholder(data, (data.bone.center+(thresh*data.bone.width))/65536);
    
    data = Validation(data);

    if 1 == exist('results')
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        results = [results, result];
    else
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        results = result;
    end
    data.imDataArrayAdj = imAdj;
    disp(['Step: ', num2str(steps(i)), ' time: ', num2str(time)]);
end

save([data.seg,'ThresholdResults.mat'],'results');
