clear
clf
close all

feature('accel', 'on')

sets = {};
s = size(sets, 2);

sizeResults = 5;

steps = zeros(1,sizeResults,s);
time = zeros(1,sizeResults, s);
TPR = zeros(1,sizeResults, s);
TNR = zeros(1,sizeResults,s);
FPR = zeros(1,sizeResults,s);
FNR = zeros(1,sizeResults,s);
Dice = zeros(1,sizeResults,s);

for j = 1:s
    load(sets{j});
    for i = 1:sizeResults

        steps(1,i,j) = results(i).step;
        time(1,i,j) = results(i).time;
        TPR(1,i,j) = results(i).data.full.TPRMean;
        TNR(1,i,j) = results(i).data.full.TNRMean;
        FPR(1,i,j) = results(i).data.full.FPRMean;
        FNR(1,i,j) = results(i).data.full.FNRMean;
        Dice(1,i,j) = results(i).data.full.diceValMean;

    end
end

% singles

setss = {'NoneThresholdResults.mat', 'Median3D333ThresholdResults.mat',...
    'Median2D33ThresholdResults.mat', 'Wiener33ThresholdResults.mat',...
    'Wiener55ThresholdResults.mat'};
ss = size(setss, 2);

stepss = zeros(1,ss);
times = zeros(1, ss);
TPRs = zeros(1, ss);
TNRs = zeros(1,ss);
FPRs = zeros(1,ss);
FNRs = zeros(1,ss);
Dices = zeros(1,ss);

for j = 1:ss
    load(setss{j});

    stepss(1,j) = results.step;
    times(1,j) = results.time;
    TPRs(1,j) = results.data.full.TPRMean;
    TNRs(1,j) = results.data.full.TNRMean;
    FPRs(1,j) = results.data.full.FPRMean;
    FNRs(1,j) = results.data.full.FNRMean;
    Dices(1,j) = results.data.full.diceValMean;

end

%% ROC 1
% for i = 1:s
%      plot(FPR(:,:,i),TPR(:,:,i)); hold on
% end
for i = 1:ss
    plot(FPRs(:,i), TPRs(:,i), '*'); hold on
end
title('TPR as function of FPR');
xlabel('FPR')
ylabel('TPR')
legend([setss],'Location','southeast')
% xlim([0.0018, 0.0022])
% ylim([0.38, 0.42])

%% ROC 2
for i = 1:s
    plot(FNR(:,:,i),TNR(:,:,i)); hold on
end
for i = 1:ss
    plot(FNRs(:,i), TNRs(:,i), '*'); hold on
end
title('TNR as function of FNR');
xlabel('FNR')
ylabel('TNR')
legend([sets,setss],'Location','southeast')
xlim([0.45, 1])

%%

figure
plot(steps(:,:,3),time(:,:,3))
title('Time complexity as function of Threshold');
xlabel('Threshold')
ylabel('Time (s)')

%% DICE
figure
plot(steps(:,:,1),Dice(:,:,1))
title('DICE as function of Threshold');
xlabel('Threshold')
ylabel('DICE')

%% DICE as function of FPR
for i = 1:s
    plot(FPR(:,:,i),Dice(:,:,i)); hold on
end
for i = 1:ss
    plot(FPRs(:,i), Dices(:,i), '*'); hold on
end
title('Dice as function of FPR');
xlabel('FPR')
ylabel('Dice')
legend([sets,setss],'Location','southeast')
%xlim([0, 0.01])

%% TPR
figure
plot(steps,TPR)
title('TPR as function of Threshold');
xlabel('Threshold')
ylabel('TPR')

%% TNR
figure
plot(steps,TNR)
title('TNR as function of Threshold');
xlabel('Threshold')
ylabel('TNR')

%% FPR
figure
plot(steps,FPR)
title('FPR as function of Threshold');
xlabel('Threshold')
ylabel('FPR')

%% FNR
figure
plot(steps,FNR)
title('FNR as function of strel size');
xlabel('Threshold')
ylabel('FNR')


