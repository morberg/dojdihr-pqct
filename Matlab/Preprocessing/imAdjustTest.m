clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, regPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 1;
% Select to save the individual segmented joints
data.saveJointsSeg = 1;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 1; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 0;

data.evalFull = 0;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;

data.evalAccuracy = 0;
data.evalAccuracyRatio = 0;
data.evalDice  = 0;
data.evalHausdorff = 0;

data.step = 'Test: ';

data = loadDataset(data);

%%

data.imDataArrayAdj = uint16(zeros(size(data.imDataArray)));
minSet = zeros(1,size(data.imDataArray, 3));
maxSet = zeros(1,size(data.imDataArray, 3));
setSize = size(data.imDataArray,3);

for i = 1:setSize
    data.imDataArrayAdj(:,:,i) = im2uint16(data.imDataArray(:,:,i));  
end
%%

minSet = zeros(1,setSize);
maxSet = zeros(1,setSize);

for i = 1:setSize

    minSet(1,i) = min(min(data.imDataArrayAdj(:,:,i)));
    maxSet(1,i) = max(max(data.imDataArrayAdj(:,:,i)));
    
end

low_in = double(median(minSet))/65536;
high_in = double(median(maxSet))/65536;


for k=1:setSize
    data.imDataArrayAdj(:,:,k) = imadjust(data.imDataArrayAdj(:,:,k), [low_in, high_in]);
end

%%

figure
imshow(data.imDataArrayAdj(:,:,9))
