function data = preprocessing(data)
%PREPROCESSING Summary of this function goes here
%   Detailed explanation goes here
    time = tic;
    data.step = 'Preprocessing: ';

    % Loades in the dataset
    data = loadDataset(data);

    % Performs image adjustment for the image colors
    data = imAdjust(data);
    
    if data.displayTimes ==1
        fprintf("Preprocessing complete. Time: %3.2f s\n", toc(time));
    end

end

