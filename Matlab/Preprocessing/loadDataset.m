function data = loadDataset(data)
%LOAD Summary of this function goes here
%   Detailed explanation goes here

    % Initialize waitbar and timer
    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Loading dataset'];
        h = waitbar(0,wbMsg);
    end
    
    % Set the datapath and the dataset list
    dataPath        = [data.datapath, data.setName, '\'];
    datasetList     = dir(fullfile(dataPath,'*.DCM'));
    setSize         = numel(datasetList);
    if (data.imgNr+data.imgsNr) > setSize
        fprintf("ERROR: image numbers out of range\n");
        return
    end

    % Get the fileprefix for later save
    data.filePrefix      = strsplit((getfield(datasetList,'name')) , '_');
    data.filePrefix      = data.filePrefix{1};

    % Get the data size and info 
    imData = dicomread(fullfile(dataPath,datasetList(data.imgNr+1).name));
    data.imInfoArray = dicominfo(fullfile(dataPath,datasetList(data.imgNr+1).name),'UseDictionaryVR', true);
    sizeIm = size(imData);
    data.imDataArray = int16(ones(sizeIm(1),sizeIm(2),(data.imgsNr)));
    data.imDataArray(:,:,1) = imData;

    for k=2:(data.imgsNr)
         if data.wb == 1
            waitbar(k/data.imgsNr)
         end
         imData = dicomread(fullfile(dataPath,datasetList(k+data.imgNr).name));
         data.imDataArray(:,:,k) = imData;
         imInfo = dicominfo(fullfile(dataPath,datasetList(k+data.imgNr).name),'UseDictionaryVR', true);
         data.imInfoArray = [data.imInfoArray, imInfo];
    end

    if data.wb == 1
        close(h)
    end
    
    if data.displayTimes ==1
        fprintf("Load dataset complete. Time: %3.2f s\n", toc(time));
    end
end

