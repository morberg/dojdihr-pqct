function data = imAdjust( data )
%PREPROCESS Summary of this function goes here
%   Detailed explanation goes here

    % Initialize waitbar and timer
    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Image adjustment'];
        h = waitbar(0,wbMsg);
    end
    
    data.imDataArrayAdj = uint16(zeros(size(data.imDataArray)));
    minSet = zeros(1,size(data.imDataArray, 3));
    maxSet = zeros(1,size(data.imDataArray, 3));
    setSize = size(data.imDataArray,3);

    for i = 1:setSize
        data.imDataArrayAdj(:,:,i) = im2uint16(data.imDataArray(:,:,i));  
    end

    minSet = zeros(1,setSize);
    maxSet = zeros(1,setSize);

    for i = 1:setSize
        minSet(1,i) = min(min(data.imDataArrayAdj(:,:,i)));
        maxSet(1,i) = max(max(data.imDataArrayAdj(:,:,i)));
    end

    low_in = double(median(minSet))/65536;
    high_in = double(median(maxSet))/65536;

    for k=1:setSize
        if data.wb == 1
            waitbar(k/setSize)
        end
        data.imDataArrayAdj(:,:,k) = imadjust(data.imDataArrayAdj(:,:,k), [low_in, high_in]);
    end
    
    if data.wb == 1
        close(h)
    end
    
    if data.displayTimes ==1
        fprintf("Image adjustment complete. Time: %3.2f s\n", toc(time));
    end
end

