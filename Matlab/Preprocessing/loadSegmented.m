function data = loadSegmented(data)
%LOAD Summary of this function goes here
%   Detailed explanation goes here

    BWPath              = [data.datapath,'BW_Mask'];
    
    datasetList         = dir(fullfile(BWPath,'*.DCM'));
    
    imData = dicomread(fullfile(BWPath,datasetList(1).name));
    [M,N] = size(imData);
    data.imDataArrayBW = zeros(M,N,330);

    for k=1:330
            % BW mask
            imData = dicomread(fullfile(BWPath,datasetList(k).name));
            data.imDataArrayBW(:,:,k) = logical(imData);
    end

end

