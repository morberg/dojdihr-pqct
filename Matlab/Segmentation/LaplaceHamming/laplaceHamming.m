% Author: Morten Morberg Madsen
% Created: 08-03-2018
% Based on: 


function [imageBW, imageSeg] = laplaceHamming(image)
    
    figure, imshow(image,[]), title('image');
    
    % Display the original gray scale image.
    
    F=fft2(image);
    S=fftshift(log(1+abs(F)));
    figure, imshow(S,[]), title('Spectrum Image');
    
    lapFilt = fspecial('laplacian',0.1);
 
    F_lap = imfilter(double(F),lapFilt);
    
    
    hamFilt = hamming(size(F,1))*hamming(size(F,2))';
    hamFilt_shift = fftshift(hamFilt);
    %F_lap = imfilter(double(F_lap),hamFilt_shift);
    F_lap = double(F_lap).*hamFilt_shift;
    
    img_filtered = ifft2(F_lap);
    figure, imshow(img_filtered,[]), title('img filtered');
    
    
    
    imageBW = image;
    imageSeg = image;
    imageSeg(~image) = 0;
end
