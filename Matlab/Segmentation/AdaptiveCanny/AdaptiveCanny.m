% Author: Morten Morberg Madsen
% Created: 08-03-2018
% Based on: 


function [imageBW, imageSeg] = AdaptiveCanny(image)
    
    %figure, imshow(image,[]), title('image');
    
    % --------------------------------------------------------------------
    % Step 0: Histogram matching
    % --------------------------------------------------------------------    
%     
%     im_ref = 'C0003834_00000';
%     refpath = 'G:\DANACT_Anonymiseret\28-2\';
%     imref = dicomread([refpath, im_ref]);
%     
%     
%     imref = im2uint16(imref);
%     [counts, binlocation] = imhist(imref, 65536);
%     binlocation_real = binlocation(counts > 0);
%  
%     imref = imadjust(imref, [binlocation_real(1,1)/65536,...
%                       binlocation_real(size(binlocation_real,1),1)/65536], []);  
    %figure, imshow(imref), title('image');
    
    %image = imhistmatch(image,imref);
    % --------------------------------------------------------------------
    % Step 1: Global Thresholding (imageBW)
    % --------------------------------------------------------------------          
    BW = imbinarize(image, 0.4);

    BW1 = imclearborder(BW);
    %figure, imshowpair(BW, BW1), title('imcloseborder')

    BW2 = bwareaopen(BW1, 10);
    %figure, imshowpair(BW1, BW2), title('areaopen')

    BW3 = imdilate(BW2, strel('disk', 5)); 
    %figure, imshowpair(BW2, BW3), title('dilate')

    BW4 = imclose(BW3, strel('disk', 10));
    %figure, imshowpair(BW3, BW4), title('close')

    BW5 = bwareaopen(BW4, 300);
    %figure, imshowpair(BW4, BW5), title('areaopen')

    BW6 = imfill(BW5, 'holes');
    %figure, imshowpair(BW5, BW6), title('fill')

    BW7 = imdilate(BW6, strel('disk', 25)); 
    %figure, imshowpair(BW6, BW7), title('dilate')

    imageBW = imfill(BW7, 'holes');
    %figure, imshowpair(BW7, imageBW), title('fill')

    imageSeg = image;
    imageSeg(~imageBW) = 0;
    
    BWe = imerode(imageBW, strel('disk', 20)); 
    %figure, imshowpair(imageBW, BWe), title('dilate')
    
    % --------------------------------------------------------------------
    % Step 2: Edge Enhancement %Not in use as undesired elements are often
    % ehhanced aswell.
    % --------------------------------------------------------------------

    %imageEdge = imsharpen(image,'Radius',2,'Amount',3); % Adding filtered image
    %figure, imshowpair(image, imageEdge), title('Edge enhancement - Laplacian filtering');
    %figure, imshow(image-imageEdge,[]), title('image');   
    

    % --------------------------------------------------------------------
    % Step 3: SEGMENTATION
    % --------------------------------------------------------------------
    % Step 3.1: Adaptive Thresholding
    % --------------------------------------------------------------------   
    
    %imtool(image_adj);
    T = adaptthresh(image, 0.40);% TODO adjust
    IsegAdpThr = imbinarize(image, T);
    IsegAdpThr = IsegAdpThr .* BWe;
    %figure, imshowpair(image,IsegAdpThr), title('Image Adaptive Thresholded (IsegAdpThr)');

    % --------------------------------------------------------------------
    % Step 3.2: Canny Edge Detection
    % --------------------------------------------------------------------   
    [~, threshold] = edge(imageSeg,'canny');
    %imhist(image)
    %imtool(image)

    IsegCanny = edge(image,'canny',[0.10 0.38]); %0.15 0.38
    IsegCanny = IsegCanny .* BWe;
    %figure, imshowpair(image,IsegCanny), title('Iobrcbr,IsegCanny');
      
         
    ISeg = imbinarize(IsegAdpThr +  IsegCanny);
    %figure, imshowpair(image,ISeg), title('image,ISeg');
    %figure, imshow(ISeg,[]), title('ISeg');
    
    ISegOpen = bwareaopen(ISeg,10);
    %figure, imshowpair(ISeg,ISegOpen), title('image,ISeg');
    %figure, imshow(ISeg,[]), title('ISeg');

    IsegD = imdilate(ISegOpen,strel('disk',5));
    %figure, imshowpair(ISegOpen,IsegD), title('image,IsegAC');    
    
    ISegClosed = imclose(IsegD,strel('disk',5));
    %figure, imshowpair(ISegOpen,ISegClosed), title('IsegAC,ISegClosed');
    
    %IsegD = imdilate(ISegClosed,strel('disk',5));
    %figure, imshowpair(ISegClosed,IsegD), title('image,IsegAC');
    
    ISegFill = imfill(ISegClosed,'holes');
    %figure, imshowpair(IsegD,ISegFill), title('ISegFill,ISegClosed');
    
    IsegE = imerode(ISegFill,strel('disk',4));
    %figure, imshowpair(image,IsegE), title('image,IsegAC');
    IsegOpen = bwareaopen(IsegE,1000);
    %figure, imshowpair(image,IsegOpen), title('image,IsegAC');

    
    % --------------------------------------------------------------------
    % Step 3.3: Combine Canny with Adaptive Threshold
    % --------------------------------------------------------------------  
    
    ISegCannySkel = bwmorph(IsegCanny,'skel',Inf);
    %figure, imshowpair(IsegCanny,ISegCannySkel), title('image,IsegAC');
    %figure, imshow(ISegCannySkel,[]), title('image');
    
    ISegCannyMorphSpur = bwmorph(IsegCanny,'spur',10);
    %figure, imshowpair(ISegCannySkel,ISegCannyMorph), title('image,IsegAC');
    %figure, imshow(ISegCannyMorph,[]), title('image');
    
    ISegCannyMorph=imdilate(IsegCanny,strel('disk',2));
    %figure, imshow(ISegCannyMorph,[]), title('image');
    IsubCan = imbinarize(IsegOpen-ISegCannyMorph);

    
    CC = bwconncomp(IsubCan);
    cellfun(@numel,CC.PixelIdxList);

    ISegOpen = bwareaopen(IsubCan,700);
    %figure, imshowpair(IsubCan,ISegOpen), title('image,IsegAC');
    %figure, imshow(ISegFinal,[]), title('image');
    
    ISegRecon = ISegOpen + ISegCannyMorphSpur;
    %figure, imshowpair(image,ISegRecon), title('image,IsegAC');
    %figure, imshow(ISegRecon,[]), title('image');
    %imtool(image);
    
    % --------------------------------------------------------------------
    % Step 3.4: Adaptive Threshold lower threshold for closing canny
    % --------------------------------------------------------------------  
    
    T = adaptthresh(image, 0.35);
    IsegAdpThr = imbinarize(image, T);
    IsegAdpThr = IsegAdpThr .* BWe;
    %figure, imshowpair(image,IsegAdpThr), title('Image Adaptive Thresholded (IsegAdpThr)');
    
    IsegAdpThrAO = bwareaopen(IsegAdpThr,5);
    %figure, imshowpair(IsegAdpThr,IsegAdpThrAO), title('image,IsegAC');
    
    ISegReconAdp = imbinarize(ISegRecon + IsegAdpThrAO);
    %figure, imshowpair(image,ISegReconAdp), title('image,IsegAC');
    %figure, imshow(ISegReconAdp,[]), title('image');
    
    IsegD = imdilate(ISegReconAdp,strel('disk',5));
    %figure, imshowpair(ISegReconAdp,IsegD), title('image,IsegAC');
    
    %ISegClose = imclose(ISegReconAdp,strel('disk',5));
    %figure, imshowpair(ISegReconAdp,ISegClose), title('image,IsegAC');
   
    ISegFill = imfill(IsegD,'holes');
    %figure, imshowpair(image,ISegFill), title('image,IsegAC');
    
    IsegE = imerode(ISegFill,strel('disk',5));
    %figure, imshowpair(image,IsegE), title('image,IsegAC');
    %figure, imshowpair(ISegReconAdp,IsegE), title('image,IsegAC');
    
    IsegAdpThrAO = bwareaopen(IsegE,500);
    %figure, imshowpair(image,IsegAdpThrAO), title('image,IsegAC');
    
    imageBW = IsegAdpThrAO;
    imageSeg = image;
    imageSeg(~IsegAdpThrAO) = 0;
end
