clear
clf
close all

feature('accel', 'on')

gitPath  = 'C:\Github\';
data.resultPath  = 'C:\Github\radetect_src\Matlab\Segmentation\AdaptiveCanny';

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));

% The drive name of the harddrive
data.drive = 'G';
% The name of the set to be validated
data.setName = '28-2';

% Number of images to process (max 330)
data.imgsNr = 1;

% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'AdaptiveCanny';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 1;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

%%
data = preprocessing(data);

%%
data = segmenting(data);

%%
data = postprocessing(data);