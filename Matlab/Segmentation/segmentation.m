function data = segmentation(data)
%SEGMENTATION Summary of this function goes here
%   Detailed explanation goes here
    time = tic;
    data.step = 'Segmenting: ';

    data = Thresholder(data);

    data = MO(data);
    
    data = AC(data);
    
    data.imDataArraySeg = uint16(data.imDataArrayAdj);
    data.imDataArraySeg(~data.imDataArrayBW) = 0;
  
    if data.displayTimes ==1
        fprintf("Segmenting done. Time: %3.2f s\n", toc(time));
    end
end

