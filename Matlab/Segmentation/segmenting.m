function data = segmenting(data)
%SEGMENTING Summary of this function goes here
%   Detailed explanation goes here

    data.step = 'Segmenting';

    tic
    if data.wb == 1
        wbMsg = data.step;
    	h = waitbar(0,wbMsg);
    end
    
    setSize = size(data.imDataArray,3);
    
    data.imDataArrayBW  = logical(false(size(data.imDataArray)));
    data.imDataArraySeg = uint16(zeros(size(data.imDataArray)));
    
    for k = 1:setSize
         
        if data.wb == 1
           waitbar(k/setSize)
        end
        
        if strcmp(data.seg, 'Otsu')
            [imageBW, imageSeg] = otsuSegmentaion(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'Perio')
            [imageBW, imageSeg] = perioSeg(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'SegMask')
            [imageBW, imageSeg] = SegmentMasker(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'AdaptThresh')
            [imageBW, imageSeg] = AdaptThresh(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'Watershed')
            [imageBW, imageSeg] = Watershed(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'Niblack')
            [imageBW, imageSeg] = niblack_func(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'TestScript')
            [imageBW, imageSeg] = TestScript(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'AdaptiveCanny')
            [imageBW, imageSeg] = AdaptiveCanny(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'Semantic')
            [imageBW, imageSeg] = Semantic(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'LevelSet')
            [imageBW, imageSeg] = LevelSet(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'laplaceHamming')
            [imageBW, imageSeg] = laplaceHamming(data.imDataArrayAdj(:,:,k));
        elseif strcmp(data.seg, 'AdaptiveCanny3D')
            [imageBW, imageSeg] = AdaptiveCanny3D(data.imDataArrayAdj(:,:,k));    
        
        end
        
        %else if blabla 
                
        data.imDataArrayBW(:,:,k)  = imageBW;
        data.imDataArraySeg(:,:,k) = imageSeg;
        
    end 
    
    if data.wb == 1
        close(h)
    end
    
    if data.displayTimes ==1
        fprintf("Segmenting done. Time: %3.2f s\n", toc);
    end
    
end

