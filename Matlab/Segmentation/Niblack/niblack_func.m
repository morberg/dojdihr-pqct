function [imageBW, imageSeg] = niblack_func(image)

[imageBW, ~] = SegmentMasker(image);

ia=niblack(image,[40 40], 0.5, 0);

BW = ia .* imageBW;

BW1 = bwareaopen(BW, 100);

BW2 = imclose(BW1, strel('disk', 3));

BW3 = imfill(BW2, 'holes');

imageBW = bwareaopen(BW3, 400);

imageSeg = image;
imageSeg(~imageBW) = 0;

