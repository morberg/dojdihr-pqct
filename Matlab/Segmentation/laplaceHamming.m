clear
close all

f = imread('cameraman.jpg');

im = dicomread('N:\DANACT_Anonymiseret\1-1\C0002055_00005.dcm');

imDataUint = im2uint16(im);

minIm = min(min(imDataUint));
maxIm = max(max(imDataUint));

low_in = double(minIm)/65536;
high_in = double(maxIm)/65536;
    
imAdj = imadjust(imDataUint, [low_in, high_in]);


% h = fspecial('laplacian',0.01); % Laplacian filter 
% f2 = imfilter(imAdj,h);
% figure, imshow(f2, []);


im = wiener2(imAdj, [3,3], 0.5);
%figure, imshow(im, [])
%figure, imshowpair(im, imAdj, 'diff');

im = im2double(imAdj);

immean = imgaussfilt(im,7/6,'FilterSize',7,'Padding','replicate');
figure, imshow(immean,[])

imstd = sqrt(abs(imgaussfilt(im.*im,7/6,'FilterSize',7,'Padding','replicate') - immean.*immean));
figure, imshow(imstd,[])

bw = imbinarize(imstd+(im-immean),0.05);
figure, imshow(bw, [])


imnorm = (im-immean)./(imstd+1);
figure, imshow((im-immean), [])

contour(im)

% imcanny = edge(imnorm, 'log');
% figure, imshow(imcanny, [])

%h2 = hamming(256)*hamming(256)'; % Hamming filter 
%filtered = ifft2(fft2(double(f2)).*fftshift(h2)); % apply hamming filter to laplacian 
%figure, imshow(abs(filtered));