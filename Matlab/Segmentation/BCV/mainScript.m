clear
clf
close all

feature('accel', 'on')

gitPath  = 'C:\Users\Andersen\Documents\github\';
data.resultPath  = 'C:\Users\Andersen\Documents\github\radetect_src\Matlab\Segmentation\Kang';
data.GTPath   = 'C:\Users\Andersen\Documents\Github\radetect_src\Labeling\1-1_label\1-1\';

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.drive = 'P';
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'Niblack';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

%%
data = preprocessing(data);
%%
bins = 255;
[t1,t2] = mipbcviterative(data.imDataArrayAdj(:,:,5),65536);

im  = imbinarize(data.imDataArrayAdj(:,:,5), t2/65536);

figure
imshow(im)
