% Author: Morten Morberg Madsen
% Created: 08-03-2018

clear
clf
close all

feature('accel', 'on')

% Waitbars
% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

%% -----------------------------------------------------------------------
% Path setup
%-------------------------------------------------------------------------
% Github path:
gitPath  = 'C:\Github\';

% Path for DICOM-PNG converted data:
pngData  = 'radetect_src\Matlab\Segmentation\Semantic\pngData';

% Path for Preprocessing
prePath  = 'radetect_src\Matlab\Preprocessing';

% The drive name of the harddrive
data.drive = 'G';

addpath(genpath([gitPath, pngData ]));
%% -----------------------------------------------------------------------
% Preprocessing
%-------------------------------------------------------------------------

%% -----------------------------------------------------------------------
% DICOM to PNG conversion
%-------------------------------------------------------------------------
% Insert name of set to be converted:
data.setName = '28-2';

% Number of images to convert (max 330)
data.imgsNr = 1;

% Image to start from (Range is 0 to 329)
data.imgNr = 0;

data = dicom2png(data);

%% -----------------------------------------------------------------------
% Creation of ImageDatastore
%-------------------------------------------------------------------------



%% -----------------------------------------------------------------------
% Creation of Semantic Segmentation Network
%-------------------------------------------------------------------------


