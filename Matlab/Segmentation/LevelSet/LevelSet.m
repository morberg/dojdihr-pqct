% Author: Morten Morberg Madsen
% Created: 08-03-2018
% Based on: 


function [imageBW, imageSeg] = LevelSet(image)
    
    figure(1), imshow(image,[]), title('Original imput image(image)')
    Img=uint16(image(:,:,1));

    %% Global thresh
    
    BW = imbinarize(Img, 0.4);

    BW1 = imclearborder(BW);
    %figure, imshowpair(BW, BW1), title('imcloseborder')

    BW2 = bwareaopen(BW1, 10);
    %figure, imshowpair(BW1, BW2), title('areaopen')

    BW3 = imdilate(BW2, strel('disk', 5)); 
    %figure, imshowpair(BW2, BW3), title('dilate')

    BW4 = imclose(BW3, strel('disk', 10));
    %figure, imshowpair(BW3, BW4), title('close')

    BW5 = bwareaopen(BW4, 300);
    %figure, imshowpair(BW4, BW5), title('areaopen')

    BW6 = imfill(BW5, 'holes');
    %figure, imshowpair(BW5, BW6), title('fill')

    BW7 = imdilate(BW6, strel('disk', 20)); 
    figure(2), imshowpair(BW6, BW7), title('dilate')

  
    %% parameter setting
    timestep=10;  % time step
    mu=0.2/timestep;  % coefficient of the distance regularization term R(phi)
    iter_inner=5;
    iter_outer=40;
    lambda=5; % coefficient of the weighted length term L(phi)
    alfa=10000;  % coefficient of the weighted area term A(phi)
    epsilon=1.5; % papramater that specifies the width of the DiracDelta function
    
    sigma=1.5;     % scale parameter in Gaussian kernel
    G=fspecial('gaussian',15,sigma);
    Img_smooth=conv2(Img,G,'same');  % smooth image by Gaussiin convolution
    [Ix,Iy]=gradient(Img_smooth);
    f=Ix.^2+Iy.^2;
    g=1./(1+f);  % edge indicator function.
    
    % initialize LSF as binary step function
    c0=2;
    initialLSF=c0*ones(size(Img));
    % generate the initial region R0 as a rectangle
    initialLSF(BW7)=-c0;
    %initialLSF(10:165, 8:222)=-c0;  
    %initialLSF(10:55, 10:75)=-c0;  
    phi=initialLSF;

    figure(3);
    mesh(-phi);   % for a better view, the LSF is displayed upside down
    hold on;  contour(phi, [0,0], 'r','LineWidth',2);
    title('Initial level set function');
    view([-80 35]);
    
    figure(4);
    imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
    title('Initial zero level contour');
    %pause(0.5);
    
    potential=2;  
    if potential ==1
        potentialFunction = 'single-well';  % use single well potential p1(s)=0.5*(s-1)^2, which is good for region-based model 
    elseif potential == 2
        potentialFunction = 'double-well';  % use double-well potential in Eq. (16), which is good for both edge and region based models
    else
        potentialFunction = 'double-well';  % default choice of potential function
    end
    
    % start level set evolution
    for n=1:iter_outer
        phi = drlse_edge(phi, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);
        if mod(n,2)==0
            figure(4);
            imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
        end
    end

    % refine the zero level contour by further level set evolution with alfa=0
    alfa=0;
    iter_refine = 10;
    phi = drlse_edge(phi, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);

    finalLSF=phi;
    figure(4);
    imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
    hold on;  contour(phi, [0,0], 'r');
    str=['Final zero level contour, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
    title(str);

    %pause(0.1);
    figure;
    mesh(-finalLSF); % for a better view, the LSF is displayed upside down
    hold on;  contour(phi, [0,0], 'r','LineWidth',2);
    str=['Final level set function, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
    title(str);
    axis on;
    
    
    imageBW = image;
    imageSeg = image;
    imageSeg(~image) = 0;
end
