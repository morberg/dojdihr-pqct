% Author: Morten Morberg Madsen
% Created: 22-02-2018
% Based on: https://se.mathworks.com/help/images/examples/marker-controlled-watershed-segmentation.html


function [imageBW, imageSeg] = TestScript(image)
    figure, imshow(image,[]), title('image');
    image = imcrop(image,[649.5 0.5 307 487]);
    Threshold image - manual threshold
    BW = image > 30000;
    
    se = strel('disk',10);
    BW = imclose(BW,se);
    BW = imfill(BW,'holes');
    BW = imdilate(BW,se);
    figure, imshow(BW), title('image')
    
    %seg = region_seg(image, BW, 200, 40, true); %-- Run segmentation
    seg = activecontour(image, BW, 5, 'Chan-Vese');

    figure, imshow(seg), title('image');
    figure, imshowpair(seg,BW), title('image');
    
    % Create masked image.
    maskedImage = image;
    maskedImage(~BW) = 0;

    
    imageBW = image;
    imageSeg = image;
end
