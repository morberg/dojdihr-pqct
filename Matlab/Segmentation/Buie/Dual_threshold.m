% Author: Frederik Andersen
% Created: 14-02-2018
% Based on: Automatic segmentation of cortical and trabecular compartments
% based on a dual threshold technique for in vivo micro-CT bone analysis
% Description: Method for segmenting trabecular and cortical compartments using two
% thresholds

%% Test Data
clf
clear
close all

datapath = 'H:\DANACT_Anonymiseret\15-2\';
imagename = 'C0003357_00122.dcm'; 
image = dicomread(fullfile(datapath, imagename));

debug = 1;
showresult = 1;

if debug ==1
    figure
    imshow(image)
end

image_u = im2uint16(image);

max_val = max(max(image_u));
min_val = min(min(image_u));
low_in = double(min_val)/65536;
high_in = double(max_val)/65536;

image_u_adj = imadjust(image_u, [low_in, high_in]);

figure
imshow(image_u_adj)
%% Threshold values
t1 = 0.42;

% Periosteal surface of the cortical shell extraction

% Threshold

BW1 = imbinarize(image_u_adj, t1);

if debug == 1
    figure
    imshow(BW1);
end

% Median

BW2 = bwareaopen(BW1, 10);

if debug == 1
    figure
    imshow(BW2);
end

% Dilate

se = strel('disk', 5);
BW3 = imdilate(BW2, se);

if debug == 1
    figure
    imshow(BW3);
end

% Connectivity

BW4 = imfill(BW3, 'holes');

if debug == 1
    figure
    imshow(BW4);
end

% Erode

Perio_mask = imerode(BW4, se);

if debug == 1
    figure
    imshow(Perio_mask);
end


mask = cast(Perio_mask, class(image_u_adj));  % ensure the types are compatible
img_masked = image_u_adj .* repmat(mask, [1 1 3]);  % apply the mask
figure
imshow(img_masked)

%% Endosteal surface of the cortical shell extraction

% Threshold

thres = graythresh(image_u_adj); 
threshes = double(multithresh(image_u_adj,3))/65536;

t2 = 0.4236;

Endo_BW1 = imbinarize(image_u_adj, t2);

if debug == 1
    figure
    imshow(Endo_BW1);
    figure
    imshow(imcomplement(Perio_mask));
end

% Mask
Endo_BW2 = bitor(Endo_BW1, imcomplement(Perio_mask));

if debug == 1
    figure
    imshow(Endo_BW2);
end

% close

se = strel('disk', 15);
Endo_BW3 = imclose(imcomplement(Endo_BW2), se);

if debug == 1
    figure
    imshow(Endo_BW3);
end

% %Dialate 
% se = strel('disk', 3); 
% Endo_BW3 = imdilate(Endo_BW2, se);
% 
% if debug == 1
%     figure
%     imshow(Endo_BW3);
% end
% 
% % Connectivity
% Endo_BW4 = imfill(Endo_BW3, 'holes');
% 
% if debug == 1
%     figure
%     imshow(Endo_BW4);
% end
% 
% % Erode
% Endo_BW5 = imcomplement(imerode(imcomplement(Endo_BW4), se));
% 
% if debug == 1
%     figure
%     imshow(Endo_BW5);
% end
% 
% % Gaussian Smooth
% sigma = 3;
% Endo_BW6 = imgaussfilt(Endo_BW5, 1);
% 
% if debug == 1
%     figure
%     imshow(Endo_BW6);
% end
% 
% % Threshold
% t3 = 0.9;
% Endo_mask = imbinarize(Endo_BW6, t3);
% 
% if debug == 1
%     figure
%     imshow(Endo_mask);
% end
% 
% % Final
% 
% final = double(Perio_mask)*255 - double(Endo_mask)*100;
% 
% if showresult == 1
%     figure
%     imshow(uint8(final));
%     figure
%     imshow(image);
% end
% % Mask


