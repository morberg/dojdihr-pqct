function data = histAnalysis(data)
%HISTANALYSIS Summary of this function goes here
%   Detailed explanation goes here

plotHist = 0;

hist = histcounts(data.imDataArrayAdj, 65536);
x = 1:65536;
x = x(hist > 0);
hist = hist(hist > 0);

f = fit(x(:),hist(:),'gauss3');

center = [f.b1, f.b2, f.b3];
width  = [f.c1, f.c2, f.c3];
height = [f.a1, f.a2, f.a3];

% center = [f.b1, f.b2];%, f.b3];
% width  = [f.c1, f.c2];%, f.c3];
% height = [f.a1, f.a2];%, f.a3];

% [~, index1] = min(center);
% [~, index2] = max(center);

[~, index1] = min(center);
[~, index2] = min(height);
index3 = 6-index1-index2;

data.noise.center   = center(index1);
data.noise.width    = width(index1);
data.noise.height   = height(index1);
 data.tissue.center  = center(index3);
 data.tissue.width   = width(index3);
 data.tissue.height  = height(index3);
data.bone.center    = center(index2);
data.bone.width     = width(index2);
data.bone.height    = height(index2);

% % noise_tissue = @(x) data.noise.height*exp(-((x-data.noise.center)/data.noise.width).^2)...
% %     - data.tissue.height*exp(-((x-data.tissue.center)/data.tissue.width).^2);
% % noise_bone   = @(x) data.noise.height*exp(-((x-data.noise.center)/data.noise.width).^2)...
% %     - data.bone.height*exp(-((x-data.bone.center)/data.bone.width).^2);
% tissue_bone  = @(x) data.tissue.height*exp(-((x-data.tissue.center)/data.tissue.width).^2)...
%     - data.bone.height*exp(-((x-data.bone.center)/data.bone.width).^2);
% 
% data.tissue_bone_x2 = fzero(tissue_bone, data.tissue.center + data.tissue.width/2);
% data.tissue_bone_x1 = fzero(tissue_bone, data.tissue.center - data.tissue.width/2); 

if plotHist == 1
    noise = data.noise.height*exp(-((x-data.noise.center)/data.noise.width).^2);
     tissue = data.tissue.height*exp(-((x-data.tissue.center)/data.tissue.width).^2);
    bone = data.bone.height*exp(-((x-data.bone.center)/data.bone.width).^2);
    combined = noise+tissue+bone;

    figure
    plot(x, hist); hold on
    plot(x, noise); hold on
    plot(x, tissue); hold on 
    plot(x, bone); hold on
    plot(x, combined)
    legend('Orig', 'Noise', 'Bone', 'Combined')
end

end

