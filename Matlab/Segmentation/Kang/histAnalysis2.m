function data = histAnalysis2(data)
%HISTANALYSIS Summary of this function goes here
%   Detailed explanation goes here

plotHist = 0;

hist = histcounts(data.imDataArrayAdj, 65536);
x = 1:65536;
x = x(hist > 0);
hist = hist(hist > 0);

f = fit(x(:),hist(:),'gauss2');

center = [f.b1, f.b2];
width  = [f.c1, f.c2];
height = [f.a1, f.a2];

[~, index1] = min(center);
[~, index2] = max(center);

data.noise.center   = center(index1);
data.noise.width    = width(index1);
data.noise.height   = height(index1);
data.tissue.center  = center(index2);
data.tissue.width   = width(index2);
data.tissue.height  = height(index2);

if plotHist == 1
    noise = data.noise.height*exp(-((x-data.noise.center)/data.noise.width).^2);
    tissue = data.tissue.height*exp(-((x-data.tissue.center)/data.tissue.width).^2);
    combined = noise+tissue;

    figure
    plot(x, hist); hold on
    plot(x, noise); hold on
    plot(x, tissue); hold on 
    plot(x, combined)
    legend('Orig', 'Noise', 'Tissue', 'Combined')
end

end

