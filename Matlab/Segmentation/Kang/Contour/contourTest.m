clear
clf
close all

feature('accel', 'on')

gitPath  = 'C:\Users\Andersen\Documents\Github\';
data.resultPath  = 'C:\Users\Andersen\Documents\Github\radetect_src\Matlab\Segmentation\Kang';
data.GTPath   = 'C:\Users\Andersen\Documents\Github\radetect_src\Labeling\1-1_label\1-1\';

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.drive = 'N';
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'Niblack';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;


%%
data = preprocessing(data);

%%
data = histAnalysis(data);

%%


data = globalThresholder(data, (data.bone.center+data.bone.width)/65536);
data = morph(data, 4);

%%
image = 100;
rad = 20;
alpha = 0.1;
it = 50;
epsilon = 1;
data.imDataArrayBWContour = local_AC_UM(im2double(data.imDataArrayAdj(:,:,image)),double(data.imDataArrayBW(:,:,image)),rad,alpha,it,epsilon);
%%
GT = imread(['I:\Users\Andersen\Documents\Github\radetect_src\Labeling\GT\1-1\Label_', num2str(image),'.png']);

figure
imshow(data.imDataArrayBWContour)

diceMorph = dice(data.imDataArrayBW(:,:,image), GT);
FPMorph = sum(sum(uint8(data.imDataArrayBW(:,:,image) - GT)));
FNMorph = sum(sum(uint8(GT - data.imDataArrayBW(:,:,image))));

diceContour = dice(data.imDataArrayBWContour, GT);
FPContour = sum(sum(uint8(data.imDataArrayBWContour - GT)));
FNContour = sum(sum(uint8(GT - data.imDataArrayBWContour)));

%%
figure
imshowpair(data.imDataArrayBWContour, data.imDataArrayBW(:,:,image), 'diff')
figure
imshowpair(data.imDataArrayBWContour, GT, 'diff')

%%
figure
imshow(data.imDataArrayBWContour)

figure
imshow(data.imDataArrayAdj(:,:,image)); 

figure
imshow(data.imDataArrayAdj(:,:,image)); hold on

[B,L] = bwboundaries(data.imDataArrayBWContour,'noholes');

for k = 1:length(B)
   boundary = B{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 1)
end
%%
data.imDataArrayBWContour = activecontour(data.imDataArrayAdj,data.imDataArrayBW,20,'Chan-Vese', 'ContractionBias', 0.4, 'SmoothFactor', 0.1);
%%
image = 100;
GT = imread(['I:\Users\Andersen\Documents\Github\radetect_src\Labeling\GT\1-1\Label_', num2str(image),'.png']);

figure
imshowpair(data.imDataArrayBWContour(:,:,image), data.imDataArrayBW(:,:,image), 'diff')
figure
imshowpair(data.imDataArrayBWContour(:,:,image), GT, 'diff')
figure
imshow(data.imDataArrayBWContour(:,:,image))
%%
image = 5;
iter = 15;
smooth = 0.1;
contract = 0.4;

for i = 1:10
end

data.imDataArrayBWContour = activecontour(data.imDataArrayAdj(:,:,image),data.imDataArrayBW(:,:,image),iter,'Chan-Vese', 'ContractionBias', contract, 'SmoothFactor', smooth);


GT = imread(['I:\Users\Andersen\Documents\Github\radetect_src\Labeling\GT\1-1\Label_', num2str(image),'.png']);

figure
imshow(data.imDataArrayBWContour)

diceMorph = dice(data.imDataArrayBW(:,:,image), GT);
FPMorph = sum(sum(uint8(data.imDataArrayBW(:,:,image) - GT)));
FNMorph = sum(sum(uint8(GT - data.imDataArrayBW(:,:,image))));

diceContour = dice(data.imDataArrayBWContour, GT);
FPContour = sum(sum(uint8(data.imDataArrayBWContour - GT)));
FNContour = sum(sum(uint8(GT - data.imDataArrayBWContour)));

figure
imshowpair(data.imDataArrayBWContour, data.imDataArrayBW(:,:,image), 'diff')

%%

image = 5;
iter = 15;
smooth = 0.1;
contract = 0.4;

GT = imread(['C:\Users\Andersen\Documents\Github\radetect_src\Labeling\GT\1-1\Label_', num2str(image),'.png']);

iters = 5:5:50;
contracts = 0.1:0.1:1;

dices = zeros(10,10);
FPs = zeros(10,10);
FNs = zeros(10,10);

for i = 1:10
    for j = 1:10
    
        data.imDataArrayBWContour = activecontour(data.imDataArrayAdj(:,:,image),data.imDataArrayBW(:,:,image),iters(i),'Chan-Vese', 'ContractionBias', contracts(j), 'SmoothFactor', smooth);
        dices(i,j) = dice(data.imDataArrayBWContour, GT);
        FPs(i,j) = sum(sum(uint8(data.imDataArrayBWContour - GT)));
        FNs(i,j) = sum(sum(uint8(GT - data.imDataArrayBWContour)));
        disp(num2str(i*10+j));
    end
end

%%
dices(2,4)

%%

figure
for i = 1:10

plot(iters, dices(:,i)); hold on

end

plot(iters, dices(:,10))
xlabel('Iterations');
ylabel('Dice');
legend('0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','1')
title('Dice as function of iterations')

figure
for i = 1:10

plot(contracts, dices(i,:)); hold on

end

plot(contracts, dices(10,:))
xlabel('Contraction');
ylabel('Dice');
legend('5','10','15','20','25','30','35','40','45','50')
title('Dice as funtion of contraction bias')

%%
figure
for i = 1:10

plot(iters, FNs(:,i)); hold on

end

plot(iters, FNs(:,10))
xlabel('Iterations');
ylabel('Dice');
legend('5','10','15','20','25','30','35','40','45','50')
title('Fall negative as function of iterations')

figure
for i = 1:10

plot(contracts, FNs(i,:)); hold on

end

plot(contracts, FNs(10,:))
xlabel('Contraction');
ylabel('Dice');
legend('0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','1')
title('Fall negative as funtion of contraction bias')

%%
figure
for i = 1:10

plot(iters, FPs(:,i)); hold on

end

plot(iters, FPs(:,10))
xlabel('Iterations');
ylabel('Fall positive');
legend('0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','1')
title('Fall positive as function of iterations')

figure
for i = 1:10

plot(contracts, FPs(i,:)); hold on

end

plot(contracts, FPs(10,:))
xlabel('Contraction');
ylabel('Fall positive');
legend('5','10','15','20','25','30','35','40','45','50')
title('Fall positive as funtion of contraction bias')

%%

F = FNs+FPs;
min(min(F))
figure
for i = 1:10

plot(iters, F(:,i)); hold on

end

plot(iters, F(:,10))
xlabel('Iterations');
ylabel('False');
legend('0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','1')
title('False as function of iterations')

figure
for i = 1:10

plot(contracts, F(i,:)); hold on

end

plot(contracts, F(10,:))
xlabel('Contraction');
ylabel('False');
legend('5','10','15','20','25','30','35','40','45','50')
title('False as funtion of contraction bias')
%%
% [m,n,k]= size(data.imDataArrayBW);
% 
% testDatBW = zeros(m,n,k+30);
% testDat = zeros(m,n,k+30);
% for i  = 1:15
%     testDat(:,:,i) = data.imDataArrayAdj(:,:,1);
%     testDatBW(:,:,i) = data.imDataArrayBW(:,:,1);
%     testDat(:,:,361-i)= data.imDataArrayAdj(:,:,330);
%     testDatBW(:,:,361-i)= data.imDataArrayBW(:,:,330);
% end
% testDat(:,:,16:345) = data.imDataArrayAdj;
% testDatBW(:,:,16:345) = data.imDataArrayBW;