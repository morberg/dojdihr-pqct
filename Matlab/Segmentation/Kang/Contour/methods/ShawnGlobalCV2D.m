function data = ShawnGlobalCV2D(data, steps)

    %display = 'false';
    display = 0;

    alpha = 2000; % 400 as a result of test 2
    num_it = 4; 

    switch data.parameter
        case 'iter'
            num_it = steps;
        case 'alpha'
            alpha = steps;
        otherwise
            disp('Wrong parameter step selection, using default values')
    end
    
    for i = 1:data.imgsNr
        data.imDataArrayBW(:,:,i) = region_seg(data.imDataArrayAdj(:,:,i),...
                                                data.imDataArrayBW(:,:,i),...
                                                num_it,alpha,display);
        temp = data.imDataArrayAdj(:,:,i);
        temp(~data.imDataArrayBW(:,:,i)) = 0;
        data.imDataArraySeg(:,:,i) = temp;                                                                   
        disp(['Step complete for img #', num2str(i),' of ', num2str(data.imgsNr), ' on step ', num2str(steps)])
    end
    
end
