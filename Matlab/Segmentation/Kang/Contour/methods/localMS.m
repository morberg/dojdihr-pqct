function data = localMS(data, steps)

    rad = 19; % 3. rad: the side length of the square window (5-9) -- Selected to 9 on behalf of \Rad\1_1_20(10)_alpha0.07
    alpha = 0.07; % 4. alpha: the coeficicent to balance the image fidality term and the (0.001-0.3) -- Selected 0.07 on behalf of results\localMS\Alpha
    num_it = 20; % 5. num_it: maximum number of iterations
    epsilon = 1; % 6. epsilon: epsilon used for delta and heaviside function (1)

    switch data.parameter
        case 'iter'
            num_it = steps;
        case 'rad'
            rad = steps;
        case 'alpha'
            alpha = steps;
        otherwise
            disp('Wrong parameter step selection, using default values')
    end
            
    for i = 1:data.imgsNr
        data.imDataArrayBW(:,:,i) = local_AC_MS(im2double(data.imDataArrayAdj(:,:,i)),im2double(data.imDataArrayBW(:,:,i)),rad,alpha,num_it,epsilon);
        temp = data.imDataArrayAdj(:,:,i);
        temp(~data.imDataArrayBW(:,:,i)) = 0;
        data.imDataArraySeg(:,:,i) = temp;
        disp(['Step complete for img # ', num2str(i),' of ', num2str(data.imgsNr), ' on step ', num2str(steps)])
    end
    
end



