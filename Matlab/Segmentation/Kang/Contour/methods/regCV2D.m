function data = regCV2D(data, steps)
    
    for i = 1:data.imgsNr
        data.imDataArrayBW(:,:,i) = activecontour(data.imDataArrayAdj(:,:,i),data.imDataArrayBW(:,:,i),steps,'Chan-Vese');
    end

end
