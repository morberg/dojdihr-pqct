function data = manInit(data,method)
%MANINIT Summary of this function goes here
%   Detailed explanation goes here
    if method == 1 
        imshow(data.imDataArrayAdj);
        e = imellipse;
        tempMask_joint1 = createMask(e);

        imshow(data.imDataArrayAdj);
        e = imellipse;
        tempMask_joint2 = createMask(e);

        imshow(data.imDataArrayAdj);
        e = imellipse;
        tempMask_joint3 = createMask(e);

        imshow(data.imDataArrayAdj);
        e = imellipse;
        tempMask_joint4 = createMask(e);

        tempMask = tempMask_joint1 + tempMask_joint2 + tempMask_joint3 + tempMask_joint4;

        imshow(tempMask);
        imshow(imfuse(data.imDataArrayAdj,tempMask));
        
        data.imDataArrayBW = logical(tempMask);    
    else if method == 0
        mask = imread('mask\mask.png');
        imshow(mask);
        data.tempMask = mask;
        imshow(imfuse(data.imDataArrayAdj,data.tempMask));
        data.imDataArrayBW = logical(mask);    
    end
        
end
