clear
clf
close all

file = 'C0002055_00094.DCM';
%C:\Github\radetect_latex\doc\figs\results\activeContour\contribution
imORG = dicomread(['G:\DANACT_Anonymiseret\1-1\',file]);

imMO = dicomread(['G:\PreACresults\1-1\Segmented\',file]);
imAC = dicomread(['G:\results\1-1\Segmented\',file]);

imMObw = dicomread(['G:\PreACresults\1-1\BW_Mask\',file]);
imACbw = dicomread(['G:\results\1-1\BW_Mask\',file]);

imDIFF = imfuse(imAC,imMO);
%imDIFFbw = imfuse(imACbw,imMObw);
[h,rec2] = imcrop(imDIFF)
figure, imshow(h)

[h_org] = imcrop(imORG,rec2)
figure, imshow(h_org,[])

montage([h,h_org]);
%figure, imshowpair(h,h_org,'montage')
