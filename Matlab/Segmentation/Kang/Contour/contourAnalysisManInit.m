clear
clf
close all

feature('accel', 'on')

setss = {'LocalCVcontourResults_manTemp.mat', 'LocalCVcontourResults.mat'};
sets = {'LocalCV_manInit', 'LocalCV_threshMO'}; 

ss = size(setss, 2);

sizeResults_manInit = 17;
sizeResults_threshMO = 22;
imgsNr = 1;


steps = zeros(1,sizeResults_manInit);
time = zeros(1,sizeResults_manInit);
Dice = zeros(1,sizeResults_manInit);
%Haus = zeros(1,sizeResults_manInit);

load(setss{1});

for i = 1:sizeResults_manInit
    steps(1,i) = results(i).step;
    time(1,i) = results(i).time;
    Dice(1,i) = results(i).data.full.diceValMean;
    %Haus(1,i) = results(i).data.full.hausdorffValMean;
end


steps2 = zeros(1,sizeResults_threshMO);
time2 = zeros(1,sizeResults_threshMO);
Dice2 = zeros(1,sizeResults_threshMO);
%Haus2 = zeros(1,sizeResults_threshMO);

load(setss{2});

for i = 1:sizeResults_threshMO
    steps2(1,i) = results(i).step;
    time2(1,i) = results(i).time;
    Dice2(1,i) = results(i).vals(1).full.diceVal;
    %Haus2(1,i) = results(i).data.full.hausdorffValMean;
end

% load('LocalCVcontourResults.mat');
% Dice3 = zeros(1,2);
% for i = 1:2
%     Dice3(1,i) = results(i).vals(1).full.diceVal;
% end

%% Time complexity
%figure
stepTemp = [steps2, max(steps)];
timeTemp = [time2, max(time)];
DiceTemp = [Dice2, Dice2(end)];


plot(timeTemp,DiceTemp); hold on
plot(time,Dice); 




title('test');
%xlabel([results(i).parameter])
%ylabel('Time (s)')
