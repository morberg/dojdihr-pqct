function data = AC(data)
%AC Summary of this function goes here
%   Detailed explanation goes here
    
    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Active Contour'];
        h = waitbar(0,wbMsg);
    end

    rad = 18;
    alpha = 0.08;
    num_it = 20;
    epsilon = 1;

    for i = 1:data.imgsNr
        data.imDataArrayBW(:,:,i) = local_AC_UM(im2double(data.imDataArrayAdj(:,:,i)),im2double(data.imDataArrayBW(:,:,i)),rad,alpha,num_it,epsilon);
        temp = data.imDataArrayAdj(:,:,i);
        temp(~data.imDataArrayBW(:,:,i)) = 0;
        data.imDataArraySeg(:,:,i) = temp;
        
        if data.wb == 1
            waitbar(i/data.imgsNr)
        end
    end
    if data.wb == 1
       close(h)
    end
    if data.displayTimes ==1
        fprintf("Active contour complete. Time: %3.2f s\n", toc(time));
    end

end

