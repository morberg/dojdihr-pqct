clear
clf
%close all

feature('accel', 'on')

%sets = {'LocalCVcontourResults.mat', 'LocalMScontourResults.mat','RegCV2DcontourResults.mat', 'RegCV3DcontourResults.mat'};
   
%sets = {'LocalCVcontourResults.mat'}; 

%sets = {'results/opti/LocalMScontourResults.mat'};

%sets = {'ShawnGlobalCV2DcontourResults.mat'};
    
%sets = {'RegCV3DcontourResults.mat'};

%sets = {'param/localCV/alpha7/LocalCVcontourResults.mat'};
sets = {'param/localMS/alpha5/LocalMScontourResults.mat'};

%sets = {'param/localCV/rad1/LocalCVcontourResults.mat'};
%sets = {'param/localMS/rad1/LocalMScontourResults.mat'};      


s = size(sets, 2);

sizeResults = 12;


steps = zeros(1,sizeResults,s);
time = zeros(1,sizeResults,s);
TPR = zeros(1,sizeResults,s);
TNR = zeros(1,sizeResults,s);
FPR = zeros(1,sizeResults,s);
FNR = zeros(1,sizeResults,s);
Dice = zeros(1,sizeResults,s);
DiceStd = zeros(1,sizeResults,s);





for j = 1:s
    load(sets{j});
    for i = 1:sizeResults

        steps(1,i,j) = results(i).step;
        time(1,i,j) = results(i).time;
        TPR(1,i,j) = results(i).data.full.TPRMean;
        TNR(1,i,j) = results(i).data.full.TNRMean;
        FPR(1,i,j) = results(i).data.full.FPRMean;
        FNR(1,i,j) = results(i).data.full.FNRMean;
        Dice(1,i,j) = results(i).data.full.diceValMean;
        DiceStd(1,i,j)  = sqrt(results(i).data.full.diceValVar);
    end
end



%% Time complexity
figure
for i = 1:s 
    subplot(2,2,i);
    plot(steps(:,:,i),time(:,:,i))
    title(['Time complexity as function of ' results(i).parameter ' - ' sets(i)]);
    xlabel([results(i).parameter])
    ylabel('Time (s)')
end

%% DICE alpha mean
figure
for i = 1:s
    %subplot(2,2,i);
    plot(steps(:,:,i),Dice(:,:,i))
    hold on
    title('DICE mean as function of smoothing term');
    xlabel('Smoothing term (alpha)')
    ylabel('DICE mean')
    
    indexmin = find(min(Dice) == Dice); 
    xmin = steps(indexmin); 
    ymin = Dice(indexmin);
    
    indexmax = 4;%find(max(Dice) == Dice);
    xmax = steps(indexmax);
    ymax = Dice(indexmax);
    max(Dice)
   
    leg{1} = ['Min: ', num2str(ymin(1)), ' DICE @ ', num2str(xmin(1)), ' ', 'alpha'];
    leg{2} = ['Max: ', num2str(ymax(1)), ' DICE @ ', num2str(xmax(1)), ' ', 'alpha'];

    p(1) = plot(xmin,ymin,'b*');
    p(2) = plot(xmax,ymax,'r*');
    
    legend(p,leg,'Location','southeast')
    xlim([0, 4000])
    
    si = stepinfo(Dice,steps)   
end
%% DICE alpha std
figure
for i = 1:s
    %subplot(2,2,i);
    plot(steps(:,:,i),DiceStd(:,:,i))
    hold on
    title('DICE mean as function of smoothing term');
    xlabel('Smoothing term (alpha)')
    ylabel('DICE mean')
    
    indexmin = find(min(DiceStd) == DiceStd); 
    xmin = steps(indexmin); 
    ymin = DiceStd(indexmin);
    
    indexmax = 4;%find(max(Dice) == Dice);
    xmax = steps(indexmax);
    ymax = DiceStd(indexmax);
    max(DiceStd)
   
    leg{1} = ['Min: ', num2str(ymin(1)), ' DICE @ ', num2str(xmin(1)), ' ', 'alpha'];
    leg{2} = ['Max: ', num2str(ymax(1)), ' DICE @ ', num2str(xmax(1)), ' ', 'alpha'];

    p(1) = plot(xmin,ymin,'b*');
    p(2) = plot(xmax,ymax,'r*');
    
    legend(p,leg,'Location','southeast')
    xlim([0, 4000])
    
    si = stepinfo(DiceStd,steps)   
end
%% DICE side length
figure
for i = 1:s
    %subplot(2,2,i);
    plot(steps(:,:,i),Dice(:,:,i))
    hold on
    title('DICE mean as function of side length');
    xlabel('Side length (pixels)')
    ylabel('DICE mean')
    
    indexmin = find(min(Dice) == Dice); 
    xmin = steps(indexmin); 
    ymin = Dice(indexmin);
    
    indexmax = find(max(Dice) == Dice);
    xmax = steps(indexmax);
    ymax = Dice(indexmax);
    max(Dice)
   
    leg{1} = ['Min: ', num2str(ymin(1)), ' DICE @ ', num2str(xmin(1)), ' ', 'pixels'];
    leg{2} = ['Max: ', num2str(ymax(1)), ' DICE @ ', num2str(xmax(1)), ' ', 'pixels'];

    p(1) = plot(xmin,ymin,'b*');
    p(2) = plot(xmax,ymax,'r*');
    
    legend(p,leg,'Location','southeast')
    ylim([0.973, 0.984])

    
    si = stepinfo(Dice,steps)   
end

%% TPR
figure
for i = 1:s
    subplot(2,2,i);
    plot(steps(:,:,i),TPR(:,:,i))
    title(['TPR as function of ' results(i).parameter ' - ' sets(i)]);
    xlabel([results(i).parameter])
    ylabel('TPR')
end

%% TNR
figure
for i = 1:s
    subplot(2,2,i);
    plot(steps(:,:,i),TNR(:,:,i))
    title(['TNR as function of ' results(i).parameter ' - ' sets(i)]);
    xlabel([results(i).parameter])
    ylabel('TNR')
end


%% FPR
figure
for i = 1:s
    subplot(2,2,i);
    plot(steps(:,:,i),FPR(:,:,i))
    title(['FPR as function of ' results(i).parameter ' - ' sets(i)]);
    xlabel([results(i).parameter])
    ylabel('FPR')
end

%% FNR
figure
for i = 1:s
    subplot(2,2,i);
    plot(steps(:,:,i),FNR(:,:,i))
    title(['FNR as function of ' results(i).parameter ' - ' sets(i)]);
    xlabel([results(i).parameter])
    ylabel('FNR')
end

%% ROC 1
figure
for i = 1:s
    plot(FPR(:,:,i),TPR(:,:,i)); hold on
end
title('TPR as function of FPR');
xlabel('FPR')
ylabel('TPR')
legend(sets)
axis([0 1 0 1])

%% ROC 2
figure
for i = 1:s
    plot(FNR(:,:,i),TNR(:,:,i)); hold on
end
title('TNR as function of FNR');
xlabel('FNR')
ylabel('TNR')
legend(sets)
axis([0 1 0 1])

