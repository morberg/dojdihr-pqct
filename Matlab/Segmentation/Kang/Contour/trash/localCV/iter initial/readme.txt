steps = (1:1:10);

rad = 8; % 3. rad: the side length of the square window (5-9)
alpha = 0.001; % 4. alpha: the coeficicent to balance the image fidality term and the (0.001-0.3)
num_it = steps; % 5. num_it: maximum number of iterations
epsilon = 1; % 6. epsilon: epsilon used for delta and heaviside function (1)