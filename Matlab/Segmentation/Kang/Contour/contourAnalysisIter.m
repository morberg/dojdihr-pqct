clear
clf
close all

feature('accel', 'on')

setss = {'results/results_110MO_iter/LocalCVcontourResults.mat', ...
         'results/results_110MO_iter/LocalMScontourResults.mat'};%, ...
         %'results/results_110MO/iter_0_10_40/ShawnGlobalCV2DcontourResults.mat'
sets = {'LocalCV', 'LocalMS'};
    %'GlobalCV'};

%setss = {'ShawnGlobalCV2DcontourResults.mat'};
%sets = {'ShawnGlobalCV2DcontourResults'};

ss = size(setss, 2);

sizeResults = 6;

setss2 = {'results/results_110MO_iter/ShawnGlobalCV2DcontourResults.mat'};
sets2 = {'GlobalCV'};
ss2 = size(setss2, 2);

sizeResults2 = 12;

steps   = zeros(1,sizeResults,ss);
time    = zeros(1,sizeResults,ss);

TPR     = zeros(1,sizeResults,ss);
TPRStd  = zeros(1,sizeResults,ss);
TNR     = zeros(1,sizeResults,ss);
TNRStd  = zeros(1,sizeResults,ss);
FPR     = zeros(1,sizeResults,ss);
FPRStd  = zeros(1,sizeResults,ss);
FNR     = zeros(1,sizeResults,ss);
FNRStd  = zeros(1,sizeResults,ss);

Dice    = zeros(1,sizeResults,ss);
DiceStd = zeros(1,sizeResults,ss);
Haus    = zeros(1,sizeResults,ss);
HausStd = zeros(1,sizeResults,ss);



steps2      = zeros(1,sizeResults2,ss2);
time2       = zeros(1,sizeResults2,ss2);

TPR2        = zeros(1,sizeResults2,ss2);
TPRStd2     = zeros(1,sizeResults2,ss2);
TNR2        = zeros(1,sizeResults2,ss2);
TNRStd2     = zeros(1,sizeResults2,ss2);
FPR2        = zeros(1,sizeResults2,ss2);
FPRStd2     = zeros(1,sizeResults2,ss2);
FNR2        = zeros(1,sizeResults2,ss2);
FNRStd2     = zeros(1,sizeResults2,ss2);

Dice2       = zeros(1,sizeResults2,ss2);
DiceStd2    = zeros(1,sizeResults2,ss2);
Haus2       = zeros(1,sizeResults2,ss2);
HausStd2    = zeros(1,sizeResults2,ss2);

for j = 1:ss
    load(setss{j});
    for i = 1:sizeResults
        steps(1,i,j)    = results(i).step;
        time(1,i,j)     = results(i).time;
        
        TPR(1,i,j) = results(i).data.full.TPRMean;
        TNR(1,i,j) = results(i).data.full.TNRMean;
        FPR(1,i,j) = results(i).data.full.FPRMean;
        FNR(1,i,j) = results(i).data.full.FNRMean;
        
        TPRStd(1,i,j) = sqrt(results(i).data.full.TPRVar);
        TNRStd(1,i,j) = sqrt(results(i).data.full.TNRVar);
        FPRStd(1,i,j) = sqrt(results(i).data.full.FPRVar);
        FNRStd(1,i,j) = sqrt(results(i).data.full.FNRVar);
        
        Dice(1,i,j)     = results(i).data.full.diceValMean;
        DiceStd(1,i,j)  = sqrt(results(i).data.full.diceValVar);
        Haus(1,i,j)     = results(i).data.full.hausdorffValMean;
        HausStd(1,i,j)  = sqrt(results(i).data.full.hausdorffValVar);
    end
end
for j = 1:ss2
    load(setss2{j});
    
    
    for i = 1:sizeResults2
        steps2(1,i,j)   = results(i).step;
        time2(1,i,j)    = results(i).time;
        
        TPR2(1,i,j) = results(i).data.full.TPRMean;
        TNR2(1,i,j) = results(i).data.full.TNRMean;
        FPR2(1,i,j) = results(i).data.full.FPRMean;
        FNR2(1,i,j) = results(i).data.full.FNRMean;
        
        TPRStd2(1,i,j) = sqrt(results(i).data.full.TPRVar);
        TNRStd2(1,i,j) = sqrt(results(i).data.full.TNRVar);
        FPRStd2(1,i,j) = sqrt(results(i).data.full.FPRVar);
        FNRStd2(1,i,j) = sqrt(results(i).data.full.FNRVar);
        
        Dice2(1,i,j)    = results(i).data.full.diceValMean;
        DiceStd2(1,i,j) = sqrt(results(i).data.full.diceValVar);
        Haus2(1,i,j)    = results(i).data.full.hausdorffValMean;
        HausStd2(1,i,j) = sqrt(results(i).data.full.hausdorffValVar);
    end
end
%% Write values:
clc
for i = [1 4]
    disp('Means:')
    disp(['Steps ' num2str(steps(:,i,1)) ' - TPR mean: ' num2str(TPR(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - TNR mean: ' num2str(TNR(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FPR mean: ' num2str(FPR(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FNR mean: ' num2str(FNR(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - DICE mean: ' num2str(Dice(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - HAUS mean: ' num2str(Haus(:,i,1))])
    
    disp('STD:')
    disp(['Steps ' num2str(steps(:,i,1)) ' - TPR std: ' num2str(TPRStd(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - TNR std: ' num2str(TNRStd(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FPR std: ' num2str(FPRStd(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FNR std: ' num2str(FNRStd(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - DICE std: ' num2str(DiceStd(:,i,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - HAUS std: ' num2str(HausStd(:,i,1))])
    
    disp('mean Diff:')
    disp(['Steps ' num2str(steps(:,i,1)) ' - TPR mean diff: ' num2str(TPR(:,4,1)-TPR(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - TNR mean diff: ' num2str(TNR(:,4,1)-TNR(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FPR mean diff: ' num2str(FPR(:,4,1)-FPR(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FNR mean diff: ' num2str(FNR(:,4,1)-FNR(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - DICE mean diff: ' num2str(Dice(:,4,1)-Dice(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - HAUS mean diff: ' num2str(Haus(:,4,1)-Haus(:,1,1))])
      
    disp('std Diff:')
    disp(['Steps ' num2str(steps(:,i,1)) ' - TPR std diff: ' num2str(TPRStd(:,4,1)-TPRStd(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - TNR std diff: ' num2str(TNRStd(:,4,1)-TNRStd(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FPR std diff: ' num2str(FPRStd(:,4,1)-FPRStd(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - FNR std diff: ' num2str(FNRStd(:,4,1)-FNRStd(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - DICE std diff: ' num2str(DiceStd(:,4,1)-DiceStd(:,1,1))])
    disp(['Steps ' num2str(steps(:,i,1)) ' - HAUS std diff: ' num2str(HausStd(:,4,1)-HausStd(:,1,1))])
end

%% Time complexity

figure 
for i = 1:ss
    plot(steps(:,:,i),time(:,:,i)); hold on
end
plot(steps2(:,:,1),time2(:,:,1)); hold on
title('Iterations as function of processing time');
xlabel('Number of iterations')
ylabel('Time (seconds)')
legend([sets, sets2],'Location','northwest')



%% DICE mean
figure
for i = 1:ss 
   plot(steps(:,:,i),Dice(:,:,i)); hold on
end
plot(steps2(:,:,1),Dice2(:,:,1)); hold on
title('DICE mean as function of iterations');
xlabel('Number of iterations')
ylabel('DICE mean')
legend([sets, sets2],'Location','northwest')

%% Dice Std
figure 
for i = 1:ss
    plot(steps(:,:,i),DiceStd(:,:,i)); hold on
end
plot(steps2(:,:,1),DiceStd2(:,:,1)); hold on
title('DICE standard deviation as function of iterations');
xlabel('Number of iterations')
ylabel('DICE standard deviation')
legend([sets, sets2],'Location','northeast')

%% DICE vs time
figure
for i = 1:ss 
    plot(time(:,:,i),Dice(:,:,i)); hold on
end
plot(time2(:,:,1),Dice2(:,:,1)); hold on
title('DICE mean as function of processing time');
xlabel('Time (seconds)')
ylabel('DICE mean')
legend([sets, sets2],'Location','southeast')
xlim([1, 10000])
%ylim([0.38, 0.41])

%% Haus
figure
for i = 1:ss 
    plot(steps(:,:,i),Haus(:,:,i)); hold on
end
plot(steps2(:,:,1),Haus2(:,:,1)); hold on
title('Hausdorff distance mean as function of iterations');
xlabel('Number of iterations')
ylabel('Hausdorff distance (pixels) mean')
legend([sets, sets2],'Location','northeast')

%% Haus Std
figure 
for i = 1:ss
    plot(steps(:,:,i),HausStd(:,:,i)); hold on
end
plot(steps2(:,:,1),HausStd2(:,:,1)); hold on
title('Hausdorff distance standard deviation as function of iterations');
xlabel('Number of iterations')
ylabel('Hausdorff distance (pixels) standard deviation')
legend([sets, sets2],'Location','northwest')

%% Haus vs time
figure
for i = 1:ss 
    plot(time(:,:,i),Haus(:,:,i)); hold on
end
plot(time2(:,:,1),Haus2(:,:,1)); hold on
title('Hausdorff distance mean as function of processing time');
xlabel('Time (seconds)')
ylabel('Hausdorff distance (pixels)')
legend([sets, sets2],'Location','northeast')
xlim([1, 10000])

%% FPR vs Iter
figure
for i = 1:ss 
    plot(steps(:,:,i),FPR(:,:,i)); hold on
end
plot(steps2(:,:,1),FPR2(:,:,1)); hold on
title('FPR mean as function of iterations');
xlabel('Iterations')
ylabel('FPR (1-Specificity) mean')
legend([sets, sets2],'Location','northeast')

%% FPR std vs Iter
figure
for i = 1:ss 
    plot(steps(:,:,i),FPRStd(:,:,i)); hold on
end
plot(steps2(:,:,1),FPRStd2(:,:,1)); hold on
title('FPR standard deviation as function of iterations');
xlabel('Iterations')
ylabel('FPR (1-Specificity) standard deviation')
legend([sets, sets2],'Location','southwest')

%% TPR vs Iter
figure
for i = 1:ss 
    plot(steps(:,:,i),TPR(:,:,i)); hold on
end
plot(steps2(:,:,1),TPR2(:,:,1)); hold on
title('TPR mean as function of iterations');
xlabel('Iterations')
ylabel('TPR (Sensitivity) mean')
legend([sets, sets2],'Location','southwest')
%% TPR std vs Iter
figure
for i = 1:ss 
    plot(steps(:,:,i),TPRStd(:,:,i)); hold on
end
plot(steps2(:,:,1),TPRStd2(:,:,1)); hold on
title('TPR standard deviation as function of iterations');
xlabel('Iterations')
ylabel('TPR (Sensitivity) standard deviation')
legend([sets, sets2],'Location','southeast')