clear
clf
close all

feature('accel', 'on')

setss = {'results/results_opti/LocalCVcontourResults/LocalCVcontourResults.mat', ...
         'results/results_opti//LocalMScontourResults/LocalMScontourResults.mat', ...
         'results/results_opti//ShawnGlobalCV2DcontourResults/ShawnGlobalCV2DcontourResults.mat'};
sets = {'LocalCV', 'LocalMS', 'GlobalCV'};

ss = size(setss, 2);

sizeResults =1;
imgsNr = 330;

DiceAll = zeros(2,330,ss);
TPRAll = zeros(2,330, ss);
TNRAll = zeros(2,330,ss);
FPRAll = zeros(2,330,ss);
FNRAll = zeros(2,330,ss);
HausAll = zeros(2,330,ss);
DiceMean = zeros(2,sizeResults,ss);
HausMean = zeros(2,sizeResults,ss);

for j = 1:ss
    load(setss{j});
    for i = 1:330
        for k = 1:2
            DiceAll(k,i,j) = results(k).vals(i).full.diceVal;
            TPRAll(k,i,j) = results(k).vals(i).full.TPR;
            TNRAll(k,i,j) = results(k).vals(i).full.TNR;
            FPRAll(k,i,j) = results(k).vals(i).full.FPR;
            FNRAll(k,i,j) = results(k).vals(i).full.FNR;
            HausAll(k,i,j) = results(k).vals(i).full.hausdorff;
        end
    end
    for i = 1:sizeResults
        for k = 1:2
            DiceMean(k,i,j) = results(k).data.full.diceValMean;
            HausMean(k,i,j) = results(k).data.full.hausdorffValMean;
        end
    end
    
end

%% Dice All
col=[ 0.4660    0.6740    0.1880;
      0         0.4470    0.7410;
      0.8500    0.3250    0.0980;
      0.9290    0.6940    0.1250];
figure 
x = 1:imgsNr;
plot(x, DiceAll(1,:,1), 'color', col(1,:,:)); hold on
temp1(1:imgsNr) = DiceMean(1,:,i);
%plot(x, temp1, 'color', col(1,:,:), 'lineStyle', '--'); hold on
for i = 1:ss
    temp2(1:imgsNr) = DiceMean(2,:,i);
    plot(x, DiceAll(2,:,i), 'color', col(i+1,:,:)); hold on
   % plot(x, temp2, 'color', col(i+1,:,:),'lineStyle', '--'); hold on
    title(['DICE as function of image slice']);
    legend('Initial mask',sets{1},sets{2}, sets{3},'Location','southeast')
    xlabel('Slice')
    ylabel('DICE')
    xlim([1, 330])
end


%% Hausdorff All
col=[ 0.4660    0.6740    0.1880;
      0         0.4470    0.7410;
      0.8500    0.3250    0.0980;
      0.9290    0.6940    0.1250];
figure 
x = 1:imgsNr;
plot(x, HausAll(1,:,1), 'color', col(1,:,:)); hold on
temp1(1:imgsNr) = HausMean(1,:,i);
%plot(x, temp1, 'color', col(1,:,:), 'lineStyle', '--'); hold on
for i = 1:ss
    temp2(1:imgsNr) = HausMean(2,:,i);
    plot(x, HausAll(2,:,i), 'color', col(i+1,:,:)); hold on
 %   plot(x, temp2, 'color', col(i+1,:,:),'lineStyle', '--'); hold on
    title(['Hausdorff distance as function of image slice']);
    legend('Initial mask',sets{1},sets{2}, sets{3},'Location','northeast')
    xlabel('Slice')
    ylabel('Hausdorff distance (pixels)')
    xlim([1, 330])
end


