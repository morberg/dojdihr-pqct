clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang\Contour\preAC'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;

% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (max 330)
data.imgsNr = 330;

% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.ac = 'LocalCV';% Completed
%data.ac = 'LocalMS';
%data.ac = 'RegCV2D';
%data.ac = 'RegCV3D'; Completed
%data.ac = 'ShawnGlobalCV2D'; %Completed

% Select parameter to step
data.parameter = 'iter';
%data.parameter = 'rad';
%data.parameter = 'alpha';

% Equal step
data.eqStep = 1;

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 1;
% Select to save the individual segmented joints
data.saveJointsSeg = 1;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 1; 

% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 1;
data.evalJoint2 = 1;
data.evalJoint3 = 1;
data.evalJoint4 = 1;

data.evalAccuracy = 0;
data.evalAccuracyRatio = 0;
data.evalDice  = 0;
data.evalHausdorff = 0;


%%
data = preprocessing(data);
data = histAnalysis(data);

%%
if data.eqStep == 1
steps = (20:20:20);
elseif data.eqStep == 0
    if strcmp(data.ac, 'LocalCV')
        steps = (1:10:50);
    elseif strcmp(data.ac, 'LocalMS')
        steps = (1:10:50);
    elseif strcmp(data.ac, 'RegCV2D')
        steps = (1:10:50);
    elseif strcmp(data.ac, 'RegCV3D')
        steps = (1:10:50);
    elseif strcmp(data.ac, 'ShawnGlobalCV2D')
        steps = (1:10:50);
    else
        disp('Method not found!');
    end
end

%%
data = Thresholder(data);
data = MO(data);
dataTemp = data.imDataArrayBW;
data = Validation(data);
result.step = 0;
result.time = 1;
result.data = data.resultStats;
result.vals = data.results;
result.parameter = data.parameter;
results = result;
%%
for i = 1:size(steps,2)
    
    data.imDataArrayBW = dataTemp;
    tic
    if strcmp(data.ac, 'LocalCV')
        data = localCV(data, steps(i));
    elseif strcmp(data.ac, 'LocalMS')          
        data = localMS(data, steps(i));
    elseif strcmp(data.ac, 'RegCV2D')
        data = regCV2D(data, steps(i));
    elseif strcmp(data.ac, 'RegCV3D')
        data.imDataArrayBW = activecontour(data.imDataArrayAdj,data.imDataArrayBW,steps(i),'Chan-Vese');
    elseif strcmp(data.ac, 'ShawnGlobalCV2D')
        data = ShawnGlobalCV2D(data, steps(i));
    else
        disp('Method not found!');
    end
    
    time = toc;
    data = Validation(data);

    if 1 == exist('results')
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        result.vals = data.results;
        results = [results, result];
    else
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        result.vals = data.results;
        results = result;
    end
    save([data.ac,'contourResults.mat'],'results');
    disp(['Step: ', num2str(steps(i)), ' time: ', num2str(time), ' s ' ]);
end

disp('Finalized result creation');
data = postprocessing(data);
