% singles

setss = {'results/GlobalThresh.mat', 'results/EntropyThresh.mat',...
    'results/OtsuThresh.mat', 'results/NiblackThresh.mat'};
sets = {'Gaussian', 'Entropy', 'Otsu',...
    'Niblack'};
ss = size(setss, 2);

stepss = zeros(1,ss);
times = zeros(1, ss);
TPRs = zeros(1, ss);
TPRstd = zeros(1, ss);

TNRs = zeros(1,ss);
TNRstd = zeros(1,ss);

FPRs = zeros(1,ss);
FPRstd = zeros(1,ss);

FNRs = zeros(1,ss);
FNRstd = zeros(1,ss);

Dices = zeros(1,ss);
DiceStd = zeros(1,ss);
DiceAll = zeros(1,330,ss);
TPRAll = zeros(1,330, ss);
TNRAll = zeros(1,330,ss);
FPRAll = zeros(1,330,ss);
FNRAll = zeros(1,330,ss);

for j = 1:ss
    load(setss{j});

    stepss(1,j) = results.step;
    times(1,j) = results.time;
    TPRs(1,j) = results.data.full.TPRMean;
    TPRstd(1,j) = sqrt(results.data.full.TPRVar);
    
    TNRs(1,j) = results.data.full.TNRMean;
    TNRstd(1,j) = sqrt(results.data.full.TNRVar);
    
    FPRs(1,j) = results.data.full.FPRMean;
    FPRstd(1,j) = sqrt(results.data.full.FPRVar);
    
    FNRs(1,j) = results.data.full.FNRMean;
    FNRstd(1,j) = sqrt(results.data.full.FNRVar);
    Dices(1,j) = results.data.full.diceValMean;
    DiceStd(1,j) = sqrt(results.data.full.diceValVar);
    for i = 1:330
        DiceAll(1,i,j) = results.vals(i).full.diceVal;
        TPRAll(1,i,j) = results.vals(i).full.TPR;
        TNRAll(1,i,j) = results.vals(i).full.TNR;
        FPRAll(1,i,j) = results.vals(i).full.FPR;
        FNRAll(1,i,j) = results.vals(i).full.FNR;
    end

end


%% ROC 1
for i = 1:ss
    plot(FPRs(:,i), TPRs(:,i), '*'); hold on
end
title('ROC');
xlabel('FPR (1-Specificity)')
ylabel('TPR (Sensitivity)')
legend(sets,'Location','northwest')
xlim([0.0014, 0.0025])
ylim([0.385, 0.402])

%% ROC 2
for i = 1:ss
    plot(FNRs(:,i), TNRs(:,i), '*'); hold on
end
title('TPR as function of FPR');
xlabel('FPR')
ylabel('TPR')
legend(sets,'Location','southeast')
%xlim([0.0015, 0.0025])
%ylim([0.35, 0.42])

%% Dice All
figure 
x = 1:330;
for i = 1:ss
    plot(x, DiceAll(1,:,i)); hold on
end
title('DICE as function of image slice');
xlabel('Slice')
ylabel('DICE')
legend(sets,'Location','northeast')
xlim([1, 330])

%% FPR All
x = 1:330;
figure
for i = 1:ss
    plot(x, FPRAll(1,:,i)); hold on
end
title('FPR as function of image slice');
xlabel('Slice')
ylabel('FPR (1-Specificity)')
legend(sets,'Location','northeast')
xlim([1, 330])

%% TPR All
x = 1:330;
figure
for i = 1:ss
    plot(x, TPRAll(1,:,i)); hold on
end
title('TPR as function of image slice');
xlabel('Slice')
ylabel('TPR (Sensitivity)')
legend(sets,'Location','northeast')
xlim([1, 330])

%% FNR All
x = 1:330;
figure
for i = 1:ss
    plot(x, FNRAll(1,:,i)); hold on
end
title('FNR as function of image slice');
xlabel('Slice')
ylabel('FNR (1-Sensitivity)')
legend(sets,'Location','northeast')
xlim([1, 330])

%% TNR All
x = 1:330;
figure
for i = 1:ss
    plot(x, TNRAll(1,:,i)); hold on
end
title('TNR as function of image slice');
xlabel('Slice')
ylabel('TNR (Specificity)')
legend(sets,'Location','northeast')
xlim([1, 330])

%% Times
x = 1:330;
figure
for i = 1:ss
    plot(x, times(1,:,i)); hold on
end
title('TNR as function of image slice');
xlabel('Slice')
ylabel('TNR (Specificity)')
legend(sets,'Location','northeast')
xlim([1, 330])