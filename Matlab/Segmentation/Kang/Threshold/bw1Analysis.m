% singles
clear


setss = {'results/GlobalBW1New.mat', 'results/EntropyBW1New.mat',...
    'results/OtsuBW1New.mat', 'results/NiblackBW1New.mat'};
sets = {'Global', 'Entropy', 'Otsu','Niblack'};

ss = size(setss, 2);

setSize = 21;

steps = zeros(1, setSize, ss);
DiceAll = zeros(1,setSize,ss);
TPRAll = zeros(1,setSize, ss);
TNRAll = zeros(1,setSize,ss);
FPRAll = zeros(1,setSize,ss);
FNRAll = zeros(1,setSize,ss);

TPAll = zeros(1,setSize, ss);
TNAll = zeros(1,setSize,ss);
FPAll = zeros(1,setSize,ss);
FNAll = zeros(1,setSize,ss);

for j = 1:ss
    load(setss{j});
    for i = 1:setSize
        steps(1,i,j) = results(i).step;
        DiceAll(1,i,j) = results(i).data.full.diceValMean;
        TPRAll(1,i,j) = results(i).data.full.TPRMean;
        TNRAll(1,i,j) = results(i).data.full.TNRMean;
        FPRAll(1,i,j) = results(i).data.full.FPRMean;
        FNRAll(1,i,j) = results(i).data.full.FNRMean;
        
        TPAll(1,i,j) = results(i).data.full.TPMean;
        TNAll(1,i,j) = results(i).data.full.TNMean;
        FPAll(1,i,j) = results(i).data.full.FPMean;
        FNAll(1,i,j) = results(i).data.full.FNMean;
    end
end


%% ROC 1
for i = 1:ss
    plot(FPRAll(1,:,i), TPRAll(1,:,i)); hold on
end
title('ROC');
xlabel('FPR (1-Specificity)')
ylabel('TPR (Sensitivity)')
legend(sets,'Location','southeast')
% xlim([0, 0.015])
% ylim([0.9, 1])

%% Dice All
figure 
for i = 1:ss
    plot(steps(1,:,i), DiceAll(1,:,i)); hold on
end
title('DICE as function of minimum object size');
xlabel('Minimum object size')
ylabel('DICE')
legend(sets,'Location','southeast')
xlim([0, 100])

%% FPR All
figure
for i = 1:ss
    plot(steps(1,:,i), FPRAll(1,:,i)); hold on
end
title('FPR as function of minimum object size');
xlabel('Minimum object size')
ylabel('FPR (1-Specificity)')
legend(sets,'Location','northwest')
xlim([0, 100])

%% TPR All
figure
for i = 1:ss
    plot(steps(1,:,i), TPRAll(1,:,i)); hold on
end
title('TPR as function of minimum object size');
xlabel('Minimum object size')
ylabel('TPR (Sensitivity)')
legend(sets,'Location','southeast')
xlim([0, 100])

%% TNR All
figure
for i = 1:ss
    plot(steps(1,:,i), TNRAll(1,:,i)); hold on
end
title('TNR as function of minimum object size');
xlabel('Minimum object size')
ylabel('TNR (Specificity)')
legend(sets,'Location','southwest')
xlim([0, 100])

%% FNR All
figure
for i = 1:ss
    plot(steps(1,:,i), FNRAll(1,:,i)); hold on
end
title('FNR as function of minimum object size');
xlabel('Minimum object size')
ylabel('FNR (1-Sensitivity)')
legend(sets,'Location','northeast')
xlim([0, 100])