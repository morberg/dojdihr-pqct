function data = Thresholder(data)
%THRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Thresholding'];
        h = waitbar(0,wbMsg);
    end
    data = histAnalysis2(data);
    if data.wb == 1
        waitbar(1/2)
    end
    data = globalThresholder(data, (data.tissue.center+(2.5*data.tissue.width))/65536);
    if data.wb == 1
        waitbar(2/2)
        close(h)
    end
    if data.displayTimes ==1
        fprintf("Thresholding complete. Time: %3.2f s\n", toc(time));
    end
end

