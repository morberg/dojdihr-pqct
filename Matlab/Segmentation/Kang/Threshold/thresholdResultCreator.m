clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method 
%'Global', 'Entropy', 'Otsu', 'Niblack'
data.seg = 'Niblack';

%'Thresh', 'Morph', BW1, 'BW2'
test = 'BW2';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;

data = preprocessing(data);

%%
if strcmp(data.seg, 'Global')
    data = histAnalysis2(data);
    thresh = 2.5;
    data = globalThresholder(data, (data.tissue.center+thresh*data.tissue.width)/65536);
    sterlSize = 4;
    BW1Size = 0;
elseif strcmp(data.seg, 'Entropy')
    thresh = 255;
    data = entropyThresholder(data, thresh);
    sterlSize = 4;
    BW1Size = 0;
elseif strcmp(data.seg, 'Otsu')   
    thresh = 2;
    data = multiThresholder(data, thresh);
    sterlSize = 4;
    BW1Size = 0;
elseif strcmp(data.seg, 'Niblack')
    thresh = 1.5;
    data = niblackThresholder(data, thresh);
    sterlSize = 4;
    BW1Size = 0;
end

dataTemp = data.imDataArrayBW;

if strcmp(test,'Morph')
    steps = 1:1:10;
    dataTemp = data.imDataArrayBW;
elseif strcmp(test,'BW1')
    steps = 0:5:100;
    dataTemp = data.imDataArrayBW;
elseif strcmp(test,'BW2')
    steps = 0:200:4000;
    data.imDataArrayBW = bwareaopen(data.imDataArrayBW, BW1Size);
    data = morph(data, sterlSize); 
    dataTemp = data.imDataArrayBW;
else
    steps = 1;
end


for i = 1:size(steps,2)
    
    data.imDataArrayBW = dataTemp;
    tic
       
    if strcmp(test,'Morph')
        data = morph(data, steps(i));
    elseif strcmp(test, 'BW1')
        data.imDataArrayBW = bwareaopen(data.imDataArrayBW, steps(i));
        data = morph(data, sterlSize);  
    elseif strcmp(test, 'BW2')
        for j = 1:330
            data.imDataArrayBW(:,:,j) = bwareaopen(data.imDataArrayBW(:,:,j), steps(i));
        end
    end
    
    time = toc;
    data = Validation(data);

    if 1 == exist('results')
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        result.vals = data.results;
        results = [results, result];
    else
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        result.vals = data.results;
        results = result;
    end
    disp(['Step: ', num2str(steps(i)), ' time: ', num2str(time)]);
end
%%
save(['results\',data.seg,test,'New.mat'],'results');
%%
%%data = postprocessing(data);