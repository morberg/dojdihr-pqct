% singles
clear


setss = {'results/GlobalBW2New.mat', 'results/EntropyBW2New.mat',...
    'results/OtsuBW2New.mat', 'results/NiblackBW2New.mat'};
sets = {'Gaussian', 'Entropy', 'Otsu','Niblack'};

ss = size(setss, 2);

setSize = 21;

steps = zeros(1, setSize, ss);
times = zeros(1, setSize, ss);

DiceAll = zeros(1,setSize,ss);
DiceStd = zeros(1,setSize,ss);

TPRAll = zeros(1,setSize, ss);
TPRStd = zeros(1,setSize,ss);

TNRAll = zeros(1,setSize,ss);
TNRStd = zeros(1,setSize,ss);

FPRAll = zeros(1,setSize,ss);
FPRStd = zeros(1,setSize,ss);

FNRAll = zeros(1,setSize,ss);
FNRStd = zeros(1,setSize,ss);

TPAll = zeros(1,setSize, ss);
TNAll = zeros(1,setSize,ss);
FPAll = zeros(1,setSize,ss);
FNAll = zeros(1,setSize,ss);

for j = 1:ss
    load(setss{j});
    for i = 1:setSize
        steps(1,i,j) = results(i).step;
        times(1,i,j) = results(i).time;
        
        DiceAll(1,i,j) = results(i).data.full.diceValMean;
        DiceStd(1,i,j) = sqrt(results(i).data.full.diceValVar);
        
        TPRAll(1,i,j) = results(i).data.full.TPRMean;
        TPRStd(1,i,j) = sqrt(results(i).data.full.TPRVar);
        
        TNRAll(1,i,j) = results(i).data.full.TNRMean;
        TNRStd(1,i,j) = sqrt(results(i).data.full.TNRVar);
        
        FPRAll(1,i,j) = results(i).data.full.FPRMean;
        FPRStd(1,i,j) = sqrt(results(i).data.full.FPRVar);
        
        FNRAll(1,i,j) = results(i).data.full.FNRMean;
        FNRStd(1,i,j) = sqrt(results(i).data.full.FNRVar);
        
        TPAll(1,i,j) = results(i).data.full.TPMean;
        TNAll(1,i,j) = results(i).data.full.TNMean;
        FPAll(1,i,j) = results(i).data.full.FPMean;
        FNAll(1,i,j) = results(i).data.full.FNMean;
    end

end

%% Time
figure
for i = 1:ss
    plot(steps(1,:,i), times(1,:,i)); hold on
end
title('Time as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('Time (seconds)')
legend(sets,'Location','northwest')
xlim([1, 10])
%ylim([0.38, 0.41])

%% ROC 1
for i = 1:ss
    plot(FPRAll(1,:,i), TPRAll(1,:,i)); hold on
end
title('ROC');
xlabel('FPR (1-Specificity)')
ylabel('TPR (Sensitivity)')
legend(sets,'Location','northeast')
% ylim([0.94, 0.995])
% xlim([0.003, 0.006])

%% Dice All
figure 
for i = 1:ss
    plot(steps(1,:,i), DiceAll(1,:,i)); hold on
end
title('DICE mean as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('DICE mean')
legend(sets,'Location','southeast')
xlim([0, 4000])
ylim([0.94, 0.985])

%% Dice std
figure 
for i = 1:ss
    plot(steps(1,:,i), DiceStd(1,:,i)); hold on
end
title('DICE standard deviation as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('DICE standard deviation')
legend(sets,'Location','southeast')
xlim([0, 4000])
ylim([0, 0.04])

%% FPR All
figure
for i = 1:ss
    plot(steps(1,:,i), FPRAll(1,:,i)); hold on
end
%title('FPR mean as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('FPR (1-Specificity) mean')
legend(sets,'Location','northeast')
xlim([0, 4000])
ylim([0.004, 0.014])

%% FPR Std
figure
for i = 1:ss
    plot(steps(1,:,i), FPRStd(1,:,i)); hold on
end
%title('FPR standard deviation as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('FPR (1-Specificity) standard deviation')
legend(sets,'Location','northeast')
xlim([0, 4000])
ylim([0.002, 0.017])

%% TPR All
figure
for i = 1:ss
    plot(steps(1,:,i), TPRAll(1,:,i)); hold on
end
title('TPR mean as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('TPR (Sensitivity) mean')
legend(sets,'Location','southeast')
xlim([0, 4000])
ylim([0.96, 1])

%% TPR std
figure
for i = 1:ss
    plot(steps(1,:,i), TPRStd(1,:,i)); hold on
end
title('TPR standard deviation as function of minimum object area');
xlabel('Minimum object area (pixels)')
ylabel('TPR (Sensitivity) standard deviation')
legend(sets,'Location','northeast')
xlim([0, 4000])
ylim([0, 0.08])

%% TNR std
figure
for i = 1:ss
    plot(steps(1,:,i), TNRAll(1,:,i)); hold on
end
title('TNR as function of minimum object size');
xlabel('Minimum object area (pixels)')
ylabel('TNR (Specificity)')
legend(sets,'Location','southeast')
xlim([0, 4000])
ylim([0.985, 0.997])

%% FNR All
figure
for i = 1:ss
    plot(steps(1,:,i), FNRAll(1,:,i)); hold on
end
title('FNR as function of minimum object size');
xlabel('Minimum object area (pixels)')
ylabel('FNR (1-Sensitivity)')
legend(sets,'Location','northwest')
xlim([0, 4000])
ylim([0.005, 0.035])