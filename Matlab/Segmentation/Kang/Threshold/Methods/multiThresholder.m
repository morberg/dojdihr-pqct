function data = multiThresholder(data, threshs)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
  
    for i = 1:330
        t = multithresh(data.imDataArrayAdj(:,:,i),threshs);
        data.imDataArrayBW(:,:,i)  = imbinarize(data.imDataArrayAdj(:,:,i), double(t(2))/65536);
    end
    
end
