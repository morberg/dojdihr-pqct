function data = global2DThresholder(data, thresh)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here

    for i = 1:330
        hist = histcounts(data.imDataArrayAdj(:,:,i), 65536);
        x = 1:65536;
        x = x(hist > 0);
        hist = hist(hist > 0);

        f = fit(x(:),hist(:),'gauss3');

        center = [f.b1, f.b2, f.b3];
        width  = [f.c1, f.c2, f.c3];
        height = [f.a1, f.a2, f.a3];

        %[~, index1] = min(center);
        [~, index2] = min(height);
        %index3 = 6-index1-index2;

%         data.noise.center   = center(index1);
%         data.noise.width    = width(index1);
%         data.noise.height   = height(index1);
%         data.tissue.center  = center(index3);
%         data.tissue.width   = width(index3);
%         data.tissue.height  = height(index3);
        data.bone.center    = center(index2);
        data.bone.width     = width(index2);
        %data.bone.height    = height(index2);
        
        thresh2D = (data.bone.center+(thresh*data.bone.width))/65536;

        globalBW = imbinarize(data.imDataArrayAdj(:,:,i), thresh2D);

        %globalBW1 = bwareaopen(globalBW, 10);

        data.imDataArrayBW(:,:,i) = logical(globalBW);
    end
end
