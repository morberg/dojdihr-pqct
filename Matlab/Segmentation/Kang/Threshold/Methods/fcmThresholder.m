function data = fcmThresholder(data, nRegions)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
  
    for i = 1:330

        img = data.imDataArrayAdj(:,:,i);
        H = single(img(:));
        options = [2, 100, 1e-5,0];
        [~, U, ~] = fcm(H,nRegions,options);

        maxU = max(U);
        for j=1:nRegions
            tmpindx = find(U(j,:) == maxU);
            H(tmpindx) = j;
        end
        [r,c] = size(img);
        bw = reshape(H,r,c);
        
        bwAll = (bw == 1);
        bwAll(:,:,2) = (bw == 2);
        bwAll(:,:,3) = (bw == 3);

        bw1Sum = sum(sum(bwAll(:,:,1)));
        bw2Sum = sum(sum(bwAll(:,:,2)));
        bw3Sum = sum(sum(bwAll(:,:,3)));
        
        [~,I] = min([bw1Sum, bw2Sum, bw3Sum]);
        
        data.imDataArrayBW(:,:,i) = bwAll(:,:,I);
        
        disp(['Fuccy C-Mean Step: ', num2str(i)]);
        
    end
    
end

