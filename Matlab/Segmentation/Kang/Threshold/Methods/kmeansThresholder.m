function data = kmeansThresholder(data, nRegions)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
  
    for i = 1:330

        img = data.imDataArrayAdj(:,:,i);
        H = single(img(:));

        [ClassIndex, ~] = kmeans(H,nRegions,'distance','sqEuclidean','Replicates', nRegions+1);

        [N,M,~] = size(img);
        
        bw = reshape(ClassIndex, N,M);
        
        bwAll = (bw == 1);
        bwAll(:,:,2) = (bw == 2);
        bwAll(:,:,3) = (bw == 3);

        bw1Sum = sum(sum(bwAll(:,:,1)));
        bw2Sum = sum(sum(bwAll(:,:,2)));
        bw3Sum = sum(sum(bwAll(:,:,3)));
        
        [~,I] = min([bw1Sum, bw2Sum, bw3Sum]);
        
        data.imDataArrayBW(:,:,i) = bwAll(:,:,I);
        
        disp(['K-Mean Step: ', num2str(i)]);
        
    end
    
end

