function data = adaptThresholder(data, thresh)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
    T = adaptthresh(data.imDataArrayAdj,thresh, 'Statistic', 'mean');
    [N,M] = size(data.imDataArrayAdj(:,:,1));
    mask = zeros(N,M,330);
    
    for i = 1:330
        [mask(:,:,i), ~] = SegmentMasker(data.imDataArrayAdj(:,:,i));
    end
    
    adaptBW = imbinarize(data.imDataArrayAdj, T);
    
    %adaptBW1 = zeros(N,M,330); 
    
    adaptBW1 = bitand(mask, adaptBW);

    adaptBW2 = bwareaopen(adaptBW1, 10);

    data.imDataArrayBW = logical(adaptBW2);

end
