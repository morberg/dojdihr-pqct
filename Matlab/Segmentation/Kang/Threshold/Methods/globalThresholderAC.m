function data = globalThresholderAC(data, thresh)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
    globalBW = imbinarize(data.imDataArrayAdj, thresh);

    globalBW = bwareaopen(globalBW, 10);

    data.imDataArrayBW = logical(globalBW);

end

