function data = entropyThresholder(data, thresh)
%GLOBALTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
  
    for i = 1:330
        [t,~,~] = mipentropy_th(im2double(data.imDataArrayAdj(:,:,i)),thresh);
        data.imDataArrayBW(:,:,i)  = imbinarize(data.imDataArrayAdj(:,:,i), t);
    end
    
end

