function data = niblackThresholder(data, thresh)
%NIBLACKTHRESHOLDER Summary of this function goes here
%   Detailed explanation goes here
    [N,M] = size(data.imDataArrayAdj(:,:,1));
    mask = zeros(N,M,330);
    
    for i = 1:330
        [mask(:,:,i), ~] = SegmentMasker(data.imDataArrayAdj(:,:,i));
    end
    
    data.imDataArrayBW = zeros(N,M,330);
    
    for i = 1:330
        data.imDataArrayBW(:,:,i) = niblack(data.imDataArrayAdj(:,:,i),[400 400], thresh, 0);
    end
    
    data.imDataArrayBW = logical(data.imDataArrayBW .* mask);

end

