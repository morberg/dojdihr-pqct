clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method 
%'Global', 'Entropy', 'Otsu', 'Niblack'
data.seg = 'Otsu';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 0;
% Select to have waitbars
data.wb = 0;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;

%%
folders = dir(pathdata.datapath);

for i = 3:size(folders, 1)
    data.setName = folders(i).name;
    
    data = preprocessing(data);

    data = histAnalysis2(data);

    result.set = folders(i).name;
    
    result.centerNoise = data.noise.center;
    result.widthNoise = data.noise.width;
    result.heightNoise = data.noise.height;
    
    result.centerTissue = data.tissue.center;
    result.widthTissue = data.tissue.width;
    result.heightTissue = data.tissue.height;
    
    result.thresh = data.tissue.center+2.5*data.tissue.width;

    if 1 == exist('results')
        results = [results, result];
    else
        results = result;
    end
    disp(['Step: ', num2str(i), ' ',folders(i).name])
end

save('C:\Users\Andersen\Documents\github\radetect_src\Matlab\Segmentation\Kang\Threshold\Gaussian\results2.mat', 'results');
