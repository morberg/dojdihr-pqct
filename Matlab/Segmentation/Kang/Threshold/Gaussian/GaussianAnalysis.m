clear
close all 

load('results2.mat')

[M,N] = size(results)

centerTissue = zeros(1, N);
widthTissue = zeros(1,N);
heightTissue = zeros(1,N);

for i = 1:N

    centerTissue(1,i) = results(i).centerTissue;
    widthTissue(1,i) = results(i).widthTissue;
    heightTissue(1,i) = results(i).heightTissue;
    
end

figure 
h = histogram(centerTissue, 50);
title('Mean distribution')
ylabel('Counts')
xlabel('Bins')

figure
h2 = histogram(widthTissue/2, 50);
title('Standard deviation distribution')
ylabel('Counts')
xlabel('Bins')

figure
h3 = histogram(heightTissue, 50);
title('Height distribution')
ylabel('Counts')
xlabel('Bins')


