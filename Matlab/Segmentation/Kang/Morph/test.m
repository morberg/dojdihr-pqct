clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'Global';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;


data = preprocessing(data);

data = Thresholder(data);

%%
    
    BW1Size = 0;
    strelSize = 4;
    BW2Size = 2000;
    

    BW1 = data.imDataArrayBW;
    BW1(:,:,1:110) = imdilate(data.imDataArrayBW(:,:,1:110), strel('sphere', strelSize));
    BW1(:,:,111:220) = imdilate(data.imDataArrayBW(:,:,111:220), strel('sphere', strelSize));
    BW1(:,:,221:330) = imdilate(data.imDataArrayBW(:,:,221:330), strel('sphere', strelSize));
    
    figure
    imshow(BW1(:,:,1))
    
    for i = 1:data.imgsNr
        BW1(:,:,i) = imfill(BW1(:,:,i), 'holes');
    end
    
    figure
    imshow(BW1(:,:,1))

    data.imDataArrayBW(:,:,1:110) = imerode(BW1(:,:,1:110), strel('sphere', strelSize));
    data.imDataArrayBW(:,:,111:220) = imerode(BW1(:,:,111:220), strel('sphere', strelSize));
    data.imDataArrayBW(:,:,221:330) = imerode(BW1(:,:,221:330), strel('sphere', strelSize));
    
    figure
    imshow(data.imDataArrayBW(:,:,1))
    
    for j = 1:data.imgsNr
        data.imDataArrayBW(:,:,j) = bwareaopen(data.imDataArrayBW(:,:,j), BW2Size);   
    end
    
    figure
    imshow(data.imDataArrayBW(:,:,1))
    