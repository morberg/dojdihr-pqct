function data = MO(data)
%MO Summary of this function goes here
%   Detailed explanation goes here

    time = tic;
    if data.wb == 1
        wbMsg = [data.step, 'Morphologic operations'];
        h = waitbar(0,wbMsg);
    end
    
    BW1Size = 0;
    strelSize = 5;
    BW2Size = 2000;
    
    data.imDataArrayBW = bwareaopen(data.imDataArrayBW, BW1Size);

    BW1 = data.imDataArrayBW;
    BW1(:,:,1:110) = imdilate(data.imDataArrayBW(:,:,1:110), strel('sphere', strelSize));
    BW1(:,:,111:220) = imdilate(data.imDataArrayBW(:,:,111:220), strel('sphere', strelSize));
    BW1(:,:,221:330) = imdilate(data.imDataArrayBW(:,:,221:330), strel('sphere', strelSize));
    
    if data.wb == 1
        waitbar(1/3)
    end
    
    for i = 1:data.imgsNr
        BW1(:,:,i) = imfill(BW1(:,:,i), 'holes');
    end
    
    if data.wb == 1
        waitbar(2/3)
    end

    data.imDataArrayBW(:,:,1:110) = imerode(BW1(:,:,1:110), strel('sphere', strelSize));
    data.imDataArrayBW(:,:,111:220) = imerode(BW1(:,:,111:220), strel('sphere', strelSize));
    data.imDataArrayBW(:,:,221:330) = imerode(BW1(:,:,221:330), strel('sphere', strelSize));
    
    for j = 1:data.imgsNr
        data.imDataArrayBW(:,:,j) = bwareaopen(data.imDataArrayBW(:,:,j), BW2Size);   
    end
    
    if data.wb == 1
        waitbar(3/3)
        close(h)
    end
    if data.displayTimes ==1
        fprintf("Morphologic operations complete. Time: %3.2f s\n", toc(time));
    end
    
    
end

