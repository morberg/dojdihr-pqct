function data = morph(data, size)
%MORPH Summary of this function goes here
%   Detailed explanation goes here

    BW1 = data.imDataArrayBW;
    BW1(:,:,1:110) = imdilate(data.imDataArrayBW(:,:,1:110), strel('sphere', size));
    BW1(:,:,111:220) = imdilate(data.imDataArrayBW(:,:,111:220), strel('sphere', size));
    BW1(:,:,221:330) = imdilate(data.imDataArrayBW(:,:,221:330), strel('sphere', size));

    for i = 1:data.imgsNr
        BW1(:,:,i) = imfill(BW1(:,:,i), 'holes');
    end

    data.imDataArrayBW(:,:,1:110) = imerode(BW1(:,:,1:110), strel('sphere', size));
    data.imDataArrayBW(:,:,111:220) = imerode(BW1(:,:,111:220), strel('sphere', size));
    data.imDataArrayBW(:,:,221:330) = imerode(BW1(:,:,221:330), strel('sphere', size));

end

