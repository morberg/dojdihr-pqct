% singles
clear

setss = {'Global2DAOAfterFinal.mat'};
sets = {'Global'};

ss = size(setss, 2);

setSize = 1;

steps = zeros(1, setSize, ss);
DiceAll = zeros(1,setSize,ss);
TPRAll = zeros(1,setSize, ss);
TNRAll = zeros(1,setSize,ss);
FPRAll = zeros(1,setSize,ss);
FNRAll = zeros(1,setSize,ss);

TPAll = zeros(1,setSize, ss);
TNAll = zeros(1,setSize,ss);
FPAll = zeros(1,setSize,ss);
FNAll = zeros(1,setSize,ss);

for j = 1:ss
    load(setss{j});
    for i = 1:setSize
        steps(1,i,j) = results(i).step;
        DiceAll(1,i,j) = results(i).data.full.diceValMean;
        TPRAll(1,i,j) = results(i).data.full.TPRMean;
        TNRAll(1,i,j) = results(i).data.full.TNRMean;
        FPRAll(1,i,j) = results(i).data.full.FPRMean;
        FNRAll(1,i,j) = results(i).data.full.FNRMean;
        
        TPAll(1,i,j) = results(i).data.full.TPMean;
        TNAll(1,i,j) = results(i).data.full.TNMean;
        FPAll(1,i,j) = results(i).data.full.FPMean;
        FNAll(1,i,j) = results(i).data.full.FNMean;
    end

end

%% DICE as function of FPR
for i = 1:ss
    plot(FPRAll(1,:,i), DiceAll(1,:,i)); hold on
end
title('DICE as function of FPR');
xlabel('FPR')
ylabel('DICE')
legend(sets,'Location','southeast')


%% ROC 1
for i = 1:ss
    plot(FPRAll(1,:,i), TPRAll(1,:,i)); hold on
end
title('TPR as function of FPR');
xlabel('FPR')
ylabel('TPR')
legend(sets,'Location','southeast')
% xlim([])
% ylim([0.57, 0.53])

%% Dice All
figure 

for i = 1:ss
    plot(steps(1,:,i), DiceAll(1,:,i), '*'); hold on
end
title('DICE as function of image slice');
xlabel('Slice')
ylabel('DICE')
legend(sets,'Location','southeast')
% xlim([0, 100])
% ylim([0.535, 0.564])

%% Dice Otsu
figure 
for i = 2:2
    plot(steps(1,:,i), DiceAll(1,:,i)); hold on
end
title('DICE as function of image slice');
xlabel('Slice')
ylabel('DICE')
legend(sets,'Location','northeast')

%% FPR All
x = 1:330;
figure
for i = 1:ss
    plot(steps(1,:,i), FPRAll(1,:,i)); hold on
end
title('FPR as function of image slice');
xlabel('Slice')
ylabel('FPR')
legend(sets,'Location','northeast')
% xlim([0, 100])

%% TPR All

figure
for i = 1:ss
    plot(steps(1,:,i), TPRAll(1,:,i)); hold on
end
title('TPR as function of image slice');
xlabel('Slice')
ylabel('TPR')
legend(sets,'Location','northeast')
% xlim([0, 100])
% ylim([0.38, 0.41])
