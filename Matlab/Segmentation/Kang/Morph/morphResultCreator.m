clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% The segmentation method
data.seg = 'Global';

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;

%%
data = preprocessing(data);

%%

steps = 1:1:10;

for i = 1:size(steps,2)
    
    if strcmp(data.seg, 'Global')
        data = histAnalysis(data);
        data = globalThresholder(data, (data.bone.center+(0.9*data.bone.width))/65536);
    elseif strcmp(data.seg, 'Entropy')
        data = entropyThresholder(data, 255);
    elseif strcmp(data.seg, 'Otsu')   
        data = multiThresholder(data, 2);
    elseif strcmp(data.seg, 'Niblack')
        data = niblackThresholder(data, 1.5);
    end
    
    %data.imDataArrayBW = bwareaopen(data.imDataArrayBW, 100);
    
    tic
    data = morph(data, steps(i));
    time = toc;
    data = Validation(data);

    if 1 == exist('results')
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        result.vals = data.results;
        results = [results, result];
    else
        result.step = steps(i);
        result.time = time;
        result.data = data.resultStats;
        result.vals = data.results;
        results = result;
    end
    disp(['Step: ', num2str(steps(i)), ' time: ', num2str(time)]);
 
end

save([data.seg '0morphResults.mat'],'results');