close all
clear

addpath('SegmentMasker')

Reg = dicomread('G:\results\1-1\BW_Mask\C0002055_00134.dcm');
nonReg = dicomread('C:\Github\radetect_src\Matlab\results\1-1\BW_Mask\C0002055_00134.dcm');
figure, imshowpair(Reg,nonReg)
% imDataUint = im2uint16(im);
% 
% minIm = min(min(imDataUint));
% maxIm = max(max(imDataUint));
% 
% low_in = double(minIm)/65536;
% high_in = double(maxIm)/65536;
%     
% imAdj = imadjust(imDataUint, [low_in, high_in]);
% 
% 
% [imageBW, imageSeg] = SegmentMasker(imAdj);
% 
% figure 
% imshow(imageBW)
% 
% ia=niblack(imAdj,[40 40], 0.5, 0);
% 
% figure, imshow(ia)
% 
% BW = ia .* imageBW;
% 
% figure, imshow(BW)
% 
% BW1 = bwareaopen(BW, 100);
% 
% figure, imshow(BW1)
% 
% BW2 = imclose(BW1, strel('disk', 3));
% 
% figure, imshow(BW2)
% 
% BW3 = imfill(BW2, 'holes');
% 
% figure, imshow(BW3)
% 
% BW4 = bwareaopen(BW3, 400);
% 
% figure, imshow(BW4)