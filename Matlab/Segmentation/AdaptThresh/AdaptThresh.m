function [imageBW, imageSeg] = AdaptThresh(image)
%ADAPTTHRESH Summary of this function goes here
%   Detailed explanation goes here

[mask, maskSeg] = SegmentMasker(image);

mask = imerode(mask, strel('disk', 10));

T = adaptthresh(image, 0.5);

imageBW = imbinarize(maskSeg, T);

%imageBW = bwareaopen(BW3, 50); 
figure, imshowpair(imageBW, imageBW), title('bwareaopen');

BW = edge(maskSeg,'Canny',[0.005, 0.1], 3);

figure, imshowpair(BW, BW), title('bwareaopen');

BW2 = BW .* imerode(mask, strel('disk', 2));

figure, imshowpair(BW2, BW2), title('bwareaopen');
% 
%imageBW = imclose(BW3, strel('disk', 15));
% %figure, imshowpair(BW4, BW3), title('imclose');
% 
% BW5 = imclose(BW4, strel('disk', 2));
% %figure, imshowpair(BW4, BW5), title('imclose');
% 
% BW5 = imfill(BW4, 'holes');
% %figure, imshowpair(BW5, BW4), title('imfill');
% 
% BW6 = bwareaopen(BW5, 300);
% %figure, imshowpair(BW6, BW5), title('bwareaopen');
% 
% BW7 = imopen(BW6, strel('disk', 7));
% %figure, imshowpair(BW6, BW7), title('bwareaopen');
% 
% imageBW = bwareaopen(BW7, 100);
% %figure, imshowpair(BW7, imageBW), title('bwareaopen');

imageSeg = image;
imageSeg(~imageBW) = 0; 

end

