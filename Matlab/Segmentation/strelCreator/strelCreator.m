SE = strel('sphere', 15)

figure
isosurface(SE.Neighborhood)
title('Sphere (31x31x31)')

SE = strel('cuboid', 15)

figure
isosurface(SE.Neighborhood)
title('Sphere (31x31x31)')