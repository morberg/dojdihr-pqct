function [imageBW, imageSeg] = SegmentMasker(image)
%SEGMENTMASKER Summary of this function goes here
%   Detailed explanation goes here
        
        BW = imbinarize(image, 0.4);
        
        BW1 = imclearborder(BW);
        %figure, imshowpair(BW, BW1), title('imcloseborder')
        
        BW2 = bwareaopen(BW1, 10);
        %figure, imshowpair(BW1, BW2), title('areaopen')
        
        BW3 = imdilate(BW2, strel('disk', 5)); 
        %figure, imshowpair(BW2, BW3), title('dilate')
        
        BW4 = imclose(BW3, strel('disk', 10));
        %figure, imshowpair(BW3, BW4), title('close')
        
        BW5 = bwareaopen(BW4, 300);
        %figure, imshowpair(BW4, BW5), title('areaopen')
        
        BW6 = imfill(BW5, 'holes');
        %figure, imshowpair(BW5, BW6), title('fill')
         
        BW7 = imdilate(BW6, strel('disk', 25)); 
        %figure, imshowpair(BW6, BW7), title('dilate')
        
        imageBW = imfill(BW7, 'holes');
        %figure, imshowpair(BW7, imageBW), title('fill')
   
        imageSeg = image;
        imageSeg(~imageBW) = 0;
        
end

