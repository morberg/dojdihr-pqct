% Author: Frederik Andersen
% Created: 14-02-2018
% Based on: Automatic segmentation of cortical and trabecular compartments
% based on a dual threshold technique for in vivo micro-CT bone analysis
% Description: Method for segmenting trabecular and cortical compartments using two
% thresholds

function [imageBW, imageSeg] = otsuSegmentaion(image)
    
    threshes = double(multithresh(image,2));

    BW1 = imbinarize(double(image), threshes(1,2));

    BW2 = bwareaopen(BW1,10);

    se = strel('disk', 10);
    BW3 = imclose(BW2, se);

    imageBW = imfill(BW3, 'holes');

    imageSeg = uint16(image);
    imageSeg(~imageBW) = 0;

end