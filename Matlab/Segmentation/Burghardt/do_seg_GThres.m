    if do_seg_GThres == 1
        %P = 40; % percentage thresholding
        %BW = im > (max(max(im))/100)*P;
        
        %thresh = double(multithresh(im, 2))/65536;
        %BW = imbinarize(im,thresh(1,2));
        
        [BW, threshold] = edge(im,'Canny');
        f = 2.6;
        BW = edge(im,'Canny',threshold * f);
            
        % Morphological Operations
        % Parameters
        r = 15;
        d = 0;
        se1 = strel('disk', r, d);
        r = 1;
        se2 = strel('disk', r, d);
        se3 = strel('disk', outline, d);
        
        figure, imshow(BW, []), axis on, title('BW');
        BW = bwareaopen(BW, 24);
        BW = imclearborder(BW);
        figure, imshow(BW, []), axis on, title('BW area open');
        
        
        BW = imclose(BW, se1);
        BW = imfill(BW, 'holes');
        BW = imopen(BW, se2);
        BW = imdilate(BW, se3);
        
        imMask = imData;
        imMask(~BW) = 0;

        if plot_imMask == 1
            figure, imshow(imMask, []), axis on, title('Masked image (FILTERED)');
            figure, imshowpair(imData,imMask), title('Original image vs. mask (FILTERED)');
        end    
    end