% Author: Morten Morberg Madsen
% Created: 14-02-2018
% Based on: 
% Reproducibility of direct quantitative measures of cortical 
% bone microarchitecture of the distal radius and tibia by HR-pQCT
% Description: 
% Method for segmentation of cortical bone microarchitecture, however, only
% the initial segmentation should be tested, to verify Laplace-Hamming
% filtration for periosteal surface segmentation only.

clear
close all
clc
tic

%% Handle plots
plot_imData             = 0;
plot_imLap              = 0;
plot_imDataEdgeEnch     = 0;
plot_imHam              = 0;
plot_imDataLapHam       = 0;
plot_imDataFilt         = 0;
plot_imMask             = 1;


%% Handle computation
do_histAdjust           = 1;

do_filtering_wiener     = 1;
do_filtering_median     = 0;
do_filtering_gaussian   = 0;

do_seg_GThres           = 1;

%% Load test data
datapath        = 'G:\DANACT Anonymiseret\15-2\';
imagename       = 'C0003357_00003.dcm'; 
imData          = dicomread(fullfile(datapath, imagename));
dicomInfo       = dicominfo(fullfile(datapath, imagename));

%% PreProssing - Histogram adjustment
if do_histAdjust == 1
    imData = im2uint16(imData);
    [counts, binlocation] = imhist(imData, 65536);

    binlocation_real = binlocation(counts > 0);

    imData = imadjust(imData, [binlocation_real(1,1)/65536,...
    binlocation_real(size(binlocation_real,1),1)/65536], []);
end

if plot_imData == 1
        figure, imshow(imData), axis on, title('Original image');  
end

%% Edge enhancement - Laplacian filtering
lapFilt = fspecial('laplacian', 0.2); % Creating filter
imLap = imfilter(imData, -lapFilt); % Adding filter to image

if plot_imLap == 1
        figure;
        imshow(imLap, []);
        axis on;
        title('Laplacian filtered');  
end

imDataEdgeEnch = imData + imLap; % Adding filtered image

if plot_imDataEdgeEnch == 1
        figure, imshow(imDataEdgeEnch, []), axis on,
        title('Edge enhancement - Laplacian filtering');
end

%% Noise filtering - Hamming filtering % TODO fix...
% [rows, cols] = size(imDataLap);
% hamWinR = window(@hamming,rows);
% hamWinC = window(@hamming,cols);
% [maskR,maskC] = meshgrid(hamWinR,hamWinC);
% figure;surf(maskR)
% figure;surf(maskC)
% 
% hamWindow = maskR.*maskC;
% 
% old = num2cell(size(hamWindow));
% new = uint16(hamWindow*65536); Fixing double -> uint16 convertion
% 
% figure;surf(hamWindow)
% figure;surf(new)
% 
% imDataLapHamFft = abs(fft2(imDataLap.*new.'));
% imDataLapHam = ifft2(imDataLapHamFft);
% 
% if plot_imDataLapHam == 1
%         figure;
%         imshow(imDataLapHam,[]);
%         axis on;
%         title('Hamming filtered');  
% end

%% Noise filtering - Adaptive Wiener filtering
if do_filtering_wiener == 1
    % Kernel size:
    K = 3;
    L = 3;

    imDataFilt = wiener2(imDataEdgeEnch, [K,L]);

    if plot_imDataFilt == 1
            figure, imshow(imDataFilt, []), axis on,
            title('Noise filtering - Adaptive Wiener filtering');
    end
end

%% Noise filtering - Median filtering
if do_filtering_median == 1
    % Kernel size:
    K = 3;
    L = 3;
    
    imDataFilt = medfilt2(imDataEdgeEnch, [K, L]);

    if plot_imDataFilt == 1
            figure,imshow(imDataFilt, []), axis on,
            title('Noise filtering - Median filtering');
    end
end

%% Noise filtering - Gaussian filtering
if do_filtering_gaussian == 1
    imDataFilt = imgaussfilt(imDataEdgeEnch, 0.5);

    if plot_imDataFilt == 1
            figure, imshow(imDataFilt, []), axis on,
            title('Noise filtering - Gaussian filtering');
    end
end

%% Segmentation - Global Thresholding
if do_seg_GThres == 1
    P = 50; % percentage thresholding
    BW = imData > (max(max(imData))/100)*P;
    BW_filt = imDataFilt > (max(max(imDataFilt))/100)*P;
    
    imMask_filt = imData;
    imMask = imData;
    
    imMask_filt(~BW_filt) = 0;
    imMask(~BW) = 0;
    
    
    if plot_imMask == 1
        figure, imshow(imMask, []), axis on, title('Masked image');
        figure, imshowpair(imData,imMask), title('Original image vs. mask');
    
        figure, imshow(imMask_filt, []), axis on, title('Masked image (FILTERED)');
        figure, imshowpair(imData,imMask_filt), title('Original image vs. mask (FILTERED)');
        
        figure, imshowpair(imMask,imMask_filt), title('Unfiltered vs filtered mask');
    end    
end