% Author: Morten Morberg Madsen
% Created: 14-02-2018
% Based on: 
% Reproducibility of direct quantitative measures of cortical 
% bone microarchitecture of the distal radius and tibia by HR-pQCT
% Description: 
% Method for segmentation of cortical bone microarchitecture, however, only
% the initial segmentation should be tested, to verify Laplace-Hamming
% filtration for periosteal surface segmentation only.


%pos = get (gcf, 'position');
%set(0, 'DefaultFigurePosition', pos)

clear
close all
clc
tic

%% Handle plots
plot_imData             = 0;
plot_imLap              = 0;
plot_imDataEdgeEnch     = 0;
plot_imHam              = 0;
plot_imDataLapHam       = 0;
plot_imDataFilt         = 0;
plot_imMask             = 0;
plot_bbox               = 0;
plot_cropElems          = 0;


%% Handle computation
do_pre_histAdjust       = 1;
do_enhanc_lap           = 1;

do_filtering_wiener     = 1;
do_filtering_median     = 0;
do_filtering_gaussian   = 0;

do_seg_GThres           = 1;
do_post_label           = 1;
do_post_crop            = 1;
do_post_pad             = 1;

%% Parameters
outline                 = 1;
padSize                 = 75;

%% Load test data
dataPath        = 'G:\DANACT_Anonymiseret\1-1\'; %TODO
patternName     = '*.DCM'; 
datasetList     = dir(fullfile(dataPath,patternName));

filePrefix      = strsplit((getfield(datasetList,'name')) , '_');
filePrefix      = filePrefix{1};

outputPath      = 'C:\Github\radetect_src\Matlab\Segmentation\Burghardt\results\'; %TODO

% Achive histogram count for first img
imData = dicomread(fullfile(dataPath,datasetList(1).name));
imData = im2uint16(imData);
[counts, binlocation] = imhist(imData, 65536);
binlocation_real = binlocation(counts > 0);


for k=3
    [imData,map,alpha,overlays] = dicomread(fullfile(dataPath,datasetList(k).name));
    imInfo = dicominfo(fullfile(dataPath,datasetList(k).name),'UseDictionaryVR', true);

    %% PreProssing - Histogram adjustment
    if do_pre_histAdjust == 1
    
        imData = im2uint16(imData);
        
       
        imDataOrg = imadjust(imData, [binlocation_real(1,1)/65536,...
        binlocation_real(size(binlocation_real,1),1)/65536], []);
    
        [counts, binlocation] = imhist(imDataOrg, 65536);
                 
        thresh = double(multithresh(imDataOrg))/65536;

        imData = imadjust(imDataOrg, [thresh,...
        binlocation_real(size(binlocation_real,1),1)/65536],[]);
        %figure, imshow(imDataOrg,[]);
    end

    if plot_imData == 1
            figure, imshow(imData), axis on, title('Original image');  
    end

    %% Edge enhancement - Laplacian filtering
    if do_enhanc_lap == 1
        lapFilt = fspecial('laplacian', 0.2); % Creating filter
        im = imfilter(imData, -lapFilt); % Adding filter to image

        if plot_imLap == 1
                figure;
                imshow(im, []);
                axis on;
                title('Laplacian filtered');  
        end

        im = imData + im; % Adding filtered image

        if plot_imDataEdgeEnch == 1
                figure, imshow(im, []), axis on,
                title('Edge enhancement - Laplacian filtering');
        end
    else 
        im = imData;
    end
    %% Noise filtering - Adaptive Wiener filtering
    if do_filtering_wiener == 1
        % Kernel size
        K = 3;
        L = 3;

        im = wiener2(im, [K,L]);

        if plot_imDataFilt == 1
                figure, imshow(im, []), axis on,
                title('Noise filtering - Adaptive Wiener filtering');
        end
    end

    %% Noise filtering - Median filtering
    if do_filtering_median == 1
        % Kernel size:
        K = 3;
        L = 3;

        im = medfilt2(im, [K, L]);

        if plot_imDataFilt == 1
                figure,imshow(im, []), axis on,
                title('Noise filtering - Median filtering');
        end
    end
    %% Noise filtering - Gaussian filtering
    if do_filtering_gaussian == 1
        im = imgaussfilt(im, 0.5);

        if plot_imDataFilt == 1
                figure, imshow(im, []), axis on,
                title('Noise filtering - Gaussian filtering');
        end
    end
    
    
    
    %% Segmentation - Global Thresholding
    if do_seg_GThres == 1
        % Morphological Operations 
        % Parameters
        se1 = strel('disk', 10);
        se2 = strel('disk', 20);
        se3 = strel('disk', 19);%20
        
        se4 = strel('disk', 2);
        
        se5 = strel('disk', 15); % Present multiple
        se6 = strel('disk', 5);
        
        %% Thresholding
        P = 40; % percentage thresholding
        BW = im > (max(max(im))/100)*P;
        BW1=BW;
        %figure, imshowpair(imDataOrg,BW), title('#1.1 Raw thresholding');
        
        BW = imclearborder(BW);
        %figure, imshowpair(BW1,BW), title('#1.2 Imclearborder');
        
        BW = bwareaopen(BW, 100);
        %figure, imshowpair(BW1,BW), title('#1.3 bwareaopen 400');
        
        BW = imdilate(BW, se6); 
        %figure, imshowpair(BW1,BW), title('imdilate se6');
        
        BW = imclose(BW, se1);
        %figure, imshowpair(BW1,BW), title('#1.4 imclose se1');
        
        BW = imfill(BW, 'holes');
        %figure, imshowpair(BW1,BW), title('#1.5 imfill holes');
         
        BW = imdilate(BW, se2);
        %figure, imshowpair(BW1,BW), title('#1.6 imdilate se2');
        
        imMask = imData;
        imMask(~BW) = 0;
        %figure, imshowpair(imDataOrg,imMask), title('#1.7 Thresholding mask')
        
        BW = imerode(BW,se3);
        %figure, imshowpair(imDataOrg,BW), title('imerode se3');
        
        %% Edge-based segmentation
        [BW2, threshold] = edge(imData,'Canny');
        f = 1;
        
        BW2 = edge(imMask,'Canny',threshold * f);
        %figure, imshowpair(BW2,BW2), title('Raw Segmentation');
        BW2 = BW2.*BW;
        
        BW = BW2;
        %figure, imshowpair(BW2,BW), title('Cropped BW');

        
        BW2 = bwareaopen(BW2, 50);
        %figure, imshowpair(BW,BW2), title('bwareaopen 20');

        BW2 = imdilate(BW2, se4); 
        %figure, imshowpair(BW,BW2), title('imdilate se4');
        
        BW2 = bwareaopen(BW2, 200);
        %figure, imshowpair(BW,BW2), title('bwareaopen 50');
        
        BW2 = imclose(BW2,se5);
        %figure, imshowpair(BW,BW2), title('imclose se5');

        %BW = bwmorph(BW,'spur',inf);
        BW2 = bwmorph(BW2,'bridge',Inf); % Bridge/Open first
        %figure, imshowpair(BW,BW2), title('bwmorph bridge');
        
        BW2 = imfill(BW2,'holes');
        %figure, imshowpair(BW,BW2), title('imfill holes');
        
        BW2 = bwareaopen(BW2, 100);
        %figure, imshowpair(BW,BW2), title('bwareaopen 100');
        
        BW2 = imerode(BW2, se4);
        %figure, imshowpair(BW,BW2), title('imerode se4');
        
        BW2 = bwmorph(BW2,'spur',inf);
        %figure, imshowpair(BW,BW2), title('bwmorph spur');
        
        imMask = imDataOrg;
        imMask(~BW2) = 0;
        figure, imshow(imMask, []), axis on, title('imMask');
        figure, imshowpair(imDataOrg,imMask), title('imDataOrg vs imMask'); 
  
        if plot_imMask == 1
            figure, imshow(imMask, []), axis on, title('Masked image (FILTERED)');
            figure, imshowpair(imData,imMask), title('Original image vs. mask (FILTERED)');
        end    
    end
    
    %% Postprocessing - Labeling
    if do_post_label == 1
        [labelBW, numElems] = bwlabel(BW2); 
        bbox = regionprops(labelBW, 'BoundingBox');
        
        if plot_bbox == 1
            figure, imshow(imMask), axis on, hold on
            for j=1:numElems
                bbIdx = bbox(j).BoundingBox;
                rectangle('Position', [bbIdx(1), bbIdx(2), bbIdx(3), bbIdx(4)],... 
                'EdgeColor','r', 'LineWidth', 3)
            end
            title('Postprocessing - Bounding Box'); 
        end
    end
    
    %% Postprocessing - Cropping
    if do_post_crop == 1 
        elemCell = cell(numElems,1);
        for i=1:numElems
            bbI = ceil(bbox(i).BoundingBox);
            idx_x = [bbI(1)-outline bbI(1)+bbI(3)+outline];
            idx_y = [bbI(2)-outline bbI(2)+bbI(4)+outline];

            %im = imMask; % For masked image and label_bw for bw 
            im = imMask;% == i; % For BW image
            elemCell{i} = im(idx_y(1):idx_y(2), idx_x(1):idx_x(2));
        end

        % Padding of crops
        if do_post_pad == 1
            for i=1:numel(elemCell)
                   elemCell{i} = padarray(elemCell{i},[padSize padSize],0,'both');
            end
        end
        
        if plot_cropElems == 1
            figure
            for i=1:numel(elemCell)
                subplot(1,numel(elemCell),i), imshow(elemCell{i})
            end
        end 
    end
    
    %% Output - Save output
    % BW
    %outputFile = strcat(outputPath,'BW_', filePrefix,'_',num2str(sprintf('%05d',k-1)), '.DCM');
    %dicomwrite(uint16(double(intmax('uint16'))*mat2gray(BW)), outputFile, imInfo) 
    % imMask
    outputFile = strcat(outputPath,'imMask_', filePrefix,'_',num2str(sprintf('%05d',k-1)), '.DCM');
    dicomwrite(uint16(double(intmax('uint16'))*mat2gray(imMask)), outputFile, imInfo)
    % elements
    %for i = 1:numel(elemCell)
        %outputFile = strcat(outputPath, filePrefix,'_',num2str(sprintf('%05d',k-1)),'_',(sprintf('%d',i)), '.DCM');
        %dicomwrite(uint16(double(intmax('uint16'))*mat2gray(elemCell{i})), outputFile, imInfo) 
    %end
    
    fprintf('Identified %d elements \n', numElems);
    fprintf('Segemented image No.: %d of %d \n', k, numel(datasetList));
end
toc
