function [imageBW, imageSeg ] = perioSeg(image)
% Author: Morten Morberg Madsen
% Created: 14-02-2018
% Based on: 
% Reproducibility of direct quantitative measures of cortical 
% bone microarchitecture of the distal radius and tibia by HR-pQCT
% Description: 
% Method for segmentation of cortical bone microarchitecture, however, only
% the initial segmentation should be tested, to verify Laplace-Hamming
% filtration for periosteal surface segmentation only.


%% Handle computation

do_enhanc_lap           = 1;

do_filtering_wiener     = 1;

do_seg_GThres           = 1;

thresh = double(multithresh(image))/65536;

imData = imadjust(image,[thresh, double(max(max(image)))/65536]);


    %% Noise filtering - Adaptive Wiener filtering
    if do_filtering_wiener == 1
        % Kernel size
        K = 5;
        L = 5;

        imEdge = wiener2(imData, [K,L]);    
        
    end
    %% Edge enhancement - Laplacian filtering
    if do_enhanc_lap == 1
        lapFilt = fspecial('laplacian', 0.2); % Creating filter
        im = imfilter(imEdge, -lapFilt); % Adding filter to image

        im = imEdge + im; % Adding filtered image
       
    else 
        im = imEdge;
    end
   
%     %% Noise filtering - Median filtering
%     if do_filtering_median == 1
%         % Kernel size:
%         K = 3;
%         L = 3;
% 
%         im = medfilt2(im, [K, L]);
% 
%         if plot_imDataFilt == 1
%                 figure,imshow(im, []), axis on,
%                 title('Noise filtering - Median filtering');
%         end
%     end
%     %% Noise filtering - Gaussian filtering
%     if do_filtering_gaussian == 1
%         im = imgaussfilt(im, 0.5);
% 
%         if plot_imDataFilt == 1
%                 figure, imshow(im, []), axis on,
%                 title('Noise filtering - Gaussian filtering');
%         end
%     end
    
    %% Segmentation - Global Thresholding
    if do_seg_GThres == 1
        % Morphological Operations 
        % Parameters
        se1 = strel('disk', 10);
        se2 = strel('disk', 20);
        se3 = strel('disk', 19);%20
        
        se4 = strel('disk', 2);
        
        se5 = strel('disk', 20); % Present multiple
        se6 = strel('disk', 5);
        
        %% Thresholding
        P = 20; % percentage thresholding
        BW = im > (max(max(im))/100)*P;
        BW1=BW;
        %figure, imshowpair(image,BW), title('#1.1 Raw thresholding');
        
        BW = imclearborder(BW);
        %figure, imshowpair(BW1,BW), title('#1.2 Imclearborder');
        
        BW = bwareaopen(BW, 100);
        %figure, imshowpair(BW1,BW), title('#1.3 bwareaopen 400');
        
        BW = imdilate(BW, se6); 
        %figure, imshowpair(BW1,BW), title('imdilate se6');
        
        BW = imclose(BW, se1);
        %figure, imshowpair(BW1,BW), title('#1.4 imclose se1');
        
        BW = imfill(BW, 'holes');
        %figure, imshowpair(BW1,BW), title('#1.5 imfill holes');
         
        BW = imdilate(BW, se2);
        %figure, imshowpair(BW1,BW), title('#1.6 imdilate se2');
        
        imMask = imData;
        imMask(~BW) = 0;
        %figure, imshowpair(imDataOrg,imMask), title('#1.7 Thresholding mask')
        
        BW = imerode(BW,se3);
        %figure, imshowpair(image,BW), title('imerode se3');
        
        %% Edge-based segmentation
        [BW2, threshold] = edge(imData,'Canny');
        f = 1;
        
        BW2 = edge(imMask,'Canny',threshold * f);
        figure, imshowpair(BW2,BW2), title('Raw Segmentation');
        BW2 = BW2.*BW;
        figure, imshowpair(BW2,BW2), title('Raw Segmentation');
        
        L = watershed(BW2);
        figure, imshowpair(L,L), title('Raw Segmentation');
        
        
        
%         BW2 = imclose(BW2, strel('disk', 1));
%         figure, imshowpair(BW2,BW2), title('Raw Segmentation');
%         
%         BW2 = bwareaopen(BW2, 50);
%         figure, imshowpair(BW2,BW2), title('Raw Segmentation');
%         
%         BW2 = imclose(BW2, strel('disk', 10));
%         figure, imshowpair(BW2,BW2), title('Raw Segmentation');
        
        
        
        
        imageBW = BW2;
        imageSeg = BW2;
%         
%        BW = BW2;
%         figure, imshowpair(BW2,BW), title('Cropped BW');
% 
%         BW2 = bwareaopen(BW2, 70);
%         %figure, imshowpair(BW,BW2), title('bwareaopen 20');
%         
%         BW2 = imclose(BW2,se5);
%         
%         %figure, imshowpair(BW,BW2), title('bwareaopen 20');
% 
%         BW2 = imdilate(BW2, se4); 
%         %figure, imshowpair(BW,BW2), title('imdilate se4');
%         
%         BW2 = bwareaopen(BW2, 200);
%         %figure, imshowpair(BW,BW2), title('bwareaopen 50');
%         
%         BW2 = imclose(BW2,se5);
%         %figure, imshowpair(BW,BW2), title('imclose se5');
% 
%         %BW = bwmorph(BW,'spur',inf);
%         BW2 = bwmorph(BW2,'bridge',Inf); % Bridge/Open first
%         %figure, imshowpair(BW,BW2), title('bwmorph bridge');
%         
%         BW2 = imfill(BW2,'holes');
%         %figure, imshowpair(BW,BW2), title('imfill holes');
%         
%         BW2 = bwareaopen(BW2, 100);
%         %figure, imshowpair(BW,BW2), title('bwareaopen 100');
%         
%         BW2 = imerode(BW2, se4);
%         %figure, imshowpair(BW,BW2), title('imerode se4');
%         
%         imageBW = bwmorph(BW2,'spur',inf);
%         %figure, imshowpair(BW,BW2), title('bwmorph spur');
%         
%         imageSeg = image;
%         imageSeg(~imageBW) = 0;   
    end
end