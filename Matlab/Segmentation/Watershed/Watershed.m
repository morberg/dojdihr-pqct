% Author: Morten Morberg Madsen
% Created: 22-02-2018
% Based on: https://se.mathworks.com/help/images/examples/marker-controlled-watershed-segmentation.html


function [imageBW, imageSeg] = Watershed(image)
    
    figure, imshow(image,[]), title('Input image (image)');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Global thresholding for mask creation                              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % --------------------------------------------------------------------
    % Step 1: Global Threshold and denoise
    % --------------------------------------------------------------------
    P = 40; % percentage thresholding
    iGT = image > (max(max(image))/100)*P;
    %figure, imshowpair(image,iGT), title('Image Global thresholded (iGT)');
    
    iIC = imclearborder(iGT);
    iAO = bwareaopen(iIC, 20);
    %figure, imshowpair(iGT,iAO), title('Image bordercleared + areopned (iAO)');

    % --------------------------------------------------------------------
    % Step 2: Close mask
    % --------------------------------------------------------------------
    iD = imdilate(iAO, strel('disk',20));
    %figure, imshowpair(iGT,iD), title('Image dialated (iD)');
    
    iF = imfill(iD, 'holes');
    %figure, imshowpair(iGT,iF), title('Image filled *holes* (iF)');
    
    
    iMask = image;
    iMask(~iF) = 0;
    %figure, imshow(iMask,[]), title('Image masked (iMask)');
    BW = imerode(iF, strel('disk',2));
    %figure, imshowpair(iF,BW), title('Image filled *holes* (iF)');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Marker-Controlled Watershed Segmentation                           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % --------------------------------------------------------------------
    % Step 1: Use the Gradient Magnitude as the Segmentation Function
    % --------------------------------------------------------------------
   
    hy = fspecial('sobel');
    hx  = hy';
    
    Iy = imfilter(double(iMask), hy, 'replicate');
    Ix = imfilter(double(iMask), hx, 'replicate');
    
    gradMag = sqrt(Ix.^2 + Iy.^2);
    
    %figure, imshow(gradMag,[]), title('Gradient magnitude (gradMag)');
    
    % --------------------------------------------------------------------
    % Step 2: Mark the Foreground Objects
    % --------------------------------------------------------------------
    
    se = strel('disk',5);
    
    Io = imopen(iMask, se);
    figure, imshow(Io,[]), title('Image opened (Io)');
    
    Ie = imerode(iMask, se);
    %figure, imshow(Ie,[]), title('Image eroded (Ie)');
    
    Iobr = imreconstruct(Ie, iMask);
    %figure, imshow(Iobr,[]), title('Image eroded (Ie)');
    
    Ioc = imclose(Io, se);
    %figure, imshow(Ioc,[]), title('Image opened-closed (Ioc)');
    
    Iobrd = imdilate(Iobr, se);
    Iobrcbr = imreconstruct(imcomplement(Iobrd), imcomplement(Iobr));
    Iobrcbr = imcomplement(Iobrcbr);
    
    %figure, imshow(Iobrcbr,[]), title('Opening-closing by reconstruction (Iobrcbr)');
    
    fgm = imregionalmax(Iobrcbr);
    
    %figure, imshow(fgm,[]), title('Regional maxima of opening-closing by reconstruction (fgm)');
    
    I2 = iMask;
    I2(fgm) = 255;
    %figure, imshow(I2,[]), title('Regional maxima superimposed on original image (I2)');
    
    se2 = strel(ones(2,2));
    fgm2 = imclose(fgm, se2);
    fgm3 = imerode(fgm2, se2);
       
    fgm4 = bwareaopen(fgm3, 20);
    I3 = iMask;
    I3(fgm4) = 255;
    figure, imshow(I3,[]), title('Modified regional maxima superimposed on original image (fgm4)');
    
    % --------------------------------------------------------------------
    % Step 3: Compute Background Markers
    % --------------------------------------------------------------------
    
    bw = imbinarize(Iobrcbr);
    %figure, imshow(bw,[]), title('Thresholded opening-closing by reconstruction (bw)');
    
    D = bwdist(bw);
    DL = watershed(D);
    bgm = DL == 0;
    %figure, imshow(bgm,[]), title('Watershed ridge lines (bgm)');
    
    % --------------------------------------------------------------------
    % Step 4: Compute the Watershed Transform of the Segmentation Function
    % --------------------------------------------------------------------
<<<<<<< HEAD:Matlab/Segmentation/WatershedSeg/WatershedSeg.m
    
    gradMagE = gradMag.*BW;
    %figure, imshow(gradMagE,[]), title('(gradMagE)');
    gradMag2 = imimposemin(gradMagE, bgm | fgm4);
    
    
    %gradMag2 = imerode(gradMag2, strel('disk',1));
=======
    gradMag2 = imimposemin(gradMag, bgm | fgm4);
>>>>>>> parent of 8ca9e8a... Updated Watershed and Started TestScript as combination with periosteal:Matlab/Segmentation/Watershed/Watershed.m
    %figure, imshow(gradMag2,[]), title('(gradmag2)');
    
    L = watershed(gradMag2);
    %figure, imshow(L,[]), title('Watershed (L)');
    
    imageBW = image;
    imageSeg = image;
end