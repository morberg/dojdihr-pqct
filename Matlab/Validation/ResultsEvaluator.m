function data = ResultsEvaluator(data)
%RESULTSEVALUATOR Summary of this function goes here
%   Detailed explanation goes here
    data.resultStats.size = data.imgsNr;

    if data.evalAccuracy == 1
        TP = zeros(1, data.resultStats.size);
        TN = zeros(1, data.resultStats.size);
        FP = zeros(1, data.resultStats.size);
        FN = zeros(1, data.resultStats.size);
    end

    if data.evalAccuracyRatio == 1
        TPR = zeros(1, data.resultStats.size);
        TNR = zeros(1, data.resultStats.size);
        FPR = zeros(1, data.resultStats.size);
        FNR = zeros(1, data.resultStats.size);
    end

    if data.evalDice == 1
        diceVal         = zeros(1, data.resultStats.size);
    end

    if data.evalHausdorff == 1
        hausdorffVal    = zeros(1, data.resultStats.size);
    end

    names = fieldnames(data.results);
    resultCell = struct2cell(data.results);

    for i = 2:size(names)

        elem = cell2struct(resultCell(i,:,:), names(i), 1);

        for j = 1:data.imgsNr

            if data.evalAccuracy == 1
                TP(1,j) = elem(j).(char(names(i))).TP;
                TN(1,j) = elem(j).(char(names(i))).TN;
                FP(1,j) = elem(j).(char(names(i))).FP;
                FN(1,j) = elem(j).(char(names(i))).FN;
            end

            if data.evalAccuracyRatio == 1
                TPR(1,j) = elem(j).(char(names(i))).TPR;
                TNR(1,j) = elem(j).(char(names(i))).TNR;
                FPR(1,j) = elem(j).(char(names(i))).FPR;
                FNR(1,j) = elem(j).(char(names(i))).FNR;
            end

            if data.evalDice == 1
                diceVal(1,j)        = elem(j).(char(names(i))).diceVal;
            end

            if data.evalHausdorff == 1
                hausdorffVal(1,j)   = elem(j).(char(names(i))).hausdorff;
            end
        end

        if data.evalAccuracy == 1

            tempStruct.TPMean = mean(TP);
            tempStruct.TPVar = var(TP);
            [tempStruct.TPMax,tempStruct.TPMaxNr] = max(TP);
            [tempStruct.TPMin,tempStruct.TPMinNr] = min(TP);

            tempStruct.TNMean = mean(TN);
            tempStruct.TNVar = var(TN);
            [tempStruct.TNMax,tempStruct.TNMaxNr] = max(TN);
            [tempStruct.TNMin,tempStruct.TNMinNr] = min(TN);

            tempStruct.FPMean = mean(FP);
            tempStruct.FPVar = var(FP);
            [tempStruct.FPMax,tempStruct.FPMaxNr] = max(FP);
            [tempStruct.FPMin,tempStruct.FPMinNr] = min(FP);

            tempStruct.FNMean = mean(FN);
            tempStruct.FNVar = var(FN);
            [tempStruct.FNMax,tempStruct.FNMaxNr] = max(FN);
            [tempStruct.FNMin,tempStruct.FNMinNr] = min(FN);

        end

        if data.evalAccuracyRatio == 1

            tempStruct.TPRMean = mean(TPR);
            tempStruct.TPRVar = var(TPR);
            [tempStruct.TPRMax,tempStruct.TPRMaxNr] = max(TPR);
            [tempStruct.TPRMin,tempStruct.TPRMinNr] = min(TPR);

            tempStruct.TNRMean = mean(TNR);
            tempStruct.TNRVar = var(TNR);
            [tempStruct.TNRMax,tempStruct.TNRMaxNr] = max(TNR);
            [tempStruct.TNRMin,tempStruct.TNRMinNr] = min(TNR);

            tempStruct.FPRMean = mean(FPR);
            tempStruct.FPRVar = var(FPR);
            [tempStruct.FPRMax,tempStruct.FPRMaxNr] = max(FPR);
            [tempStruct.FPRMin,tempStruct.FPRMinNr] = min(FPR);

            tempStruct.FNRMean = mean(FNR);
            tempStruct.FNRVar = var(FNR);
            [tempStruct.FNRMax,tempStruct.FNRMaxNr] = max(FNR);
            [tempStruct.FNRMin,tempStruct.FNRMinNr] = min(FNR);

        end

        if data.evalDice == 1
            tempStruct.diceValMean = mean(diceVal);
            tempStruct.diceValVar = var(diceVal);
            [tempStruct.diceValMax,tempStruct.diceValMaxNr] = max(diceVal);
            [tempStruct.diceValMin,tempStruct.diceValMinNr] = min(diceVal);
        end

        if data.evalHausdorff == 1
            tempStruct.hausdorffValMean = mean(hausdorffVal);
            tempStruct.hausdorffValVar = var(hausdorffVal);
            [tempStruct.hausdorffValMax,tempStruct.hausdorffValMaxNr] = max(hausdorffVal);
            [tempStruct.hausdorffValMin,tempStruct.hausdorffValMinNr] = min(hausdorffVal);
        end

        data.resultStats.tempStruct = tempStruct;

        f = fieldnames(data.resultStats);
        v = struct2cell(data.resultStats);
        f{strmatch('tempStruct',f,'exact')} = names{i};
        data.resultStats = cell2struct(v,f);

    end
end

