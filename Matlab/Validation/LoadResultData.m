function data = LoadResultData(data)
%SEGPROCESSOR Summary of this function goes here
%   Detailed explanation goes here

    dataPath            = [data.segmentPath,'results\1-1\BW_Mask'];
    joint1BWPath        = [data.segmentPath,'results\1-1\Joints\BW_Mask\1\'];
    joint2BWPath        = [data.segmentPath,'results\1-1\Joints\BW_Mask\2\'];
    joint3BWPath        = [data.segmentPath,'results\1-1\Joints\BW_Mask\3\'];
    joint4BWPath        = [data.segmentPath,'results\1-1\Joints\BW_Mask\4\'];
    datasetList         = dir(fullfile(dataPath,'*.DCM'));
    setSize         = numel(datasetList);
    if (data.imgNr+data.imgsNr) > setSize
        fprintf("ERROR: image numbers out of range\n");
        return
    end

    % Get the fileprefix for later save
    data.filePrefix      = strsplit((getfield(datasetList,'name')) , '_');
    data.filePrefix      = data.filePrefix{1};

    % Get the data size and info 
    imData = dicomread(fullfile(dataPath,datasetList(data.imgNr+1).name));
    sizeIm = size(imData);
    data.imDataArrayBW = logical(false(sizeIm(1),sizeIm(2),(data.imgsNr)));
    data.imDataArrayBW(:,:,1) = imData;

    for k=2:(data.imgsNr)
         
       if data.evalFull == 1
            imData = dicomread(fullfile(dataPath,datasetList(k+data.imgNr).name));
            data.imDataArrayBW(:,:,k) = logical(imData);
        end

        if data.evalJoint1 == 1
            imData = dicomread(fullfile(joint1BWPath,datasetList(k+data.imgNr).name));
            data.joint1BW(:,:,k) = logical(imData);
        end

        if data.evalJoint2 == 1
            imData = dicomread(fullfile(joint2BWPath,datasetList(k+data.imgNr).name));
            data.joint2BW(:,:,k) = logical(imData);
        end

        if data.evalJoint3 == 1
            imData = dicomread(fullfile(joint3BWPath,datasetList(k+data.imgNr).name));
            data.joint3BW(:,:,k) = logical(imData);
        end

        if data.evalJoint4 == 1
            imData = dicomread(fullfile(joint4BWPath,datasetList(k+data.imgNr).name));
            data.joint4BW(:,:,k) = logical(imData);
        end
    end

end


