function data = Validation(data)
%VALIDATION Summary of this function goes here
%   Detailed explanation goes here
data.step = 'Evaluating';

if 1 == isfield(data,'results')
    data = rmfield(data, 'results');
end

if 1 == isfield(data,'resultStats')
    data = rmfield(data, 'resultStats');
end

if data.evalData == 0
    data = LoadResultData(data);
end

if (data.evalJoint1 || data.evalJoint2 || data.evalJoint3 || data.evalJoint4)
    data = cropJoints(data);
end


GTdata = LoadGTData(data);

data = Evaluator(data, GTdata);

% [~, index] = sort([data.results.nr]);
% data.results = data.results(index);

data = ResultsEvaluator(data);

end

