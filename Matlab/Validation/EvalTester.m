close all
clear 

GTPath   = 'C:\Users\Andersen\Documents\github\radetect_src\Labeling\1-1_label\1-1\';
segData.segmentPath = 'C:\Users\Andersen\Documents\github\radetect_src\Matlab\Segmentation\Niblack\';

data.evalData = 1;

full = 1;
joint1 = 0;
joint2 = 0;
joint3 = 0;
joint4 = 0;
wb = 1;
sizeEval = 14;

% Processor
GTdata.doFull = full;
GTdata.doJoint1 = joint1;
GTdata.doJoint2 = joint2;
GTdata.doJoint3 = joint3;
GTdata.doJoint4 = joint4;

segData.doFull = full;
segData.doJoint1 = joint1;
segData.doJoint2 = joint2;
segData.doJoint3 = joint3;
segData.doJoint4 = joint4;

showDiffPic = 0;

if wb == 1
    wbMsg = 'Evaluating...';
    h = waitbar(0,wbMsg);
end 

GTDatasetList     = dir(fullfile(GTPath,'*.png'));
GTSetSize         = numel(GTDatasetList);

for i = 1:sizeEval
    
    if wb == 1
       waitbar(i/sizeEval);
    end

    GTdata.GTImageNr      = strsplit(GTDatasetList(i).name , {'_','.'});
    GTdata.GTImageNr      = str2double(GTdata.GTImageNr{2});
    segData.segImageNr     = GTdata.GTImageNr;

    GTdata.GTPath = [GTPath, GTDatasetList(i).name];

    GTdata = GTProcessor(GTdata);

    segData = SegProcessor(segData);

    if 0 == exist('results')
        results = Evaluator(segData, GTdata);
    else
        results = [results, Evaluator(segData, GTdata)];
    end

end

[~, index] = sort([results.nr]);
data.results = results(index);

data = ResultsEvaluator(data);

if wb == 1
    close(h)
end

%% Image show
    image = 9;

    diff = uint8(results(9).full.underSeg*1+results(9).full.overSeg*2);

    RGB = 255-label2rgb(diff, 'winter');
    
    figure
    imshow(RGB)
    title('Difference map. Pink is over segmentation. Yellow is Under segmentation.')


