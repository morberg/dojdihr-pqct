function [data] = Evaluator(data, GTdata)
%EVALUATOR Summary of this function goes here
%   Detailed explanation goes here

    [m,n] = size(data.imDataArrayBW(:,:,1));    
    
    if data.wb == 1
        wbMsg = [data.step];
        h = waitbar(0,wbMsg);
    end 
    
    for i = 1:data.imgsNr
    % Full BW
        results.nr     = i;
        
        if data.wb == 1
            waitbar(i/data.imgsNr);
        end
    
        if data.evalFull == 1

            if data.evalAccuracy == 1
                full.TP = sum(sum(bitand(uint8(GTdata.imDataArrayBW(:,:,i)),uint8(data.imDataArrayBW(:,:,i)))));
                full.TN = m*n - sum(sum(uint8(bitor(GTdata.imDataArrayBW(:,:,i),data.imDataArrayBW(:,:,i)))));
                full.FP = sum(sum(uint8(data.imDataArrayBW(:,:,i) - GTdata.imDataArrayBW(:,:,i))));
                full.FN = sum(sum(uint8(GTdata.imDataArrayBW(:,:,i) - data.imDataArrayBW(:,:,i))));
            end

            if data.evalAccuracyRatio == 1
                full.TPR = full.TP / (full.TP + full.FN);
                full.TNR = full.TN / (full.TN + full.FP);
                full.FPR = full.FP / (full.FP + full.TN);
                full.FNR = full.FN / (full.FN + full.TP); 
            end

            if data.evalDice  == 1
                full.diceVal = dice(data.imDataArrayBW(:,:,i), GTdata.imDataArrayBW(:,:,i));
            end

            if data.evalHausdorff == 1
                full.hausdorff = HausdorffDist(data.imDataArrayBW(:,:,i), GTdata.imDataArrayBW(:,:,i));
            end

            results.full = full;

        end

        % Joint 1 
        if data.evalJoint1 == 1

            if data.evalAccuracy == 1
                joint1.TP = sum(sum(bitand(uint8(GTdata.joint1BW(:,:,i)),uint8(data.joint1BW(:,:,i)))));
                joint1.TN = m*n - sum(sum(uint8(bitor(GTdata.joint1BW(:,:,i),data.joint1BW(:,:,i)))));
                joint1.FP = sum(sum(uint8(data.joint1BW(:,:,i) - GTdata.joint1BW(:,:,i))));
                joint1.FN = sum(sum(uint8(GTdata.joint1BW(:,:,i) - data.joint1BW(:,:,i))));
            end

            if data.evalAccuracyRatio == 1
                joint1.TPR = joint1.TP / (joint1.TP + joint1.FN);
                joint1.TNR = joint1.TN / (joint1.TN + joint1.FP);
                joint1.FPR = joint1.FP / (joint1.FP + joint1.TN);
                joint1.FNR = joint1.FN / (joint1.FN + joint1.TP); 
            end

            if data.evalDice  == 1
                joint1.diceVal = dice(data.joint1BW(:,:,i), GTdata.joint1BW(:,:,i));
            end

            if data.evalHausdorff == 1
                joint1.hausdorff = HausdorffDist(data.joint1BW(:,:,i), GTdata.joint1BW(:,:,i));
            end

            results.joint1 = joint1;

        end

        % Joint 2 
        if data.evalJoint2 == 1

            if data.evalAccuracy == 1
                joint2.TP = sum(sum(bitand(uint8(GTdata.joint2BW(:,:,i)),uint8(data.joint2BW(:,:,i)))));
                joint2.TN = m*n - sum(sum(uint8(bitor(GTdata.joint2BW(:,:,i),data.joint2BW(:,:,i)))));
                joint2.FP = sum(sum(uint8(data.joint2BW(:,:,i) - GTdata.joint2BW(:,:,i))));
                joint2.FN = sum(sum(uint8(GTdata.joint2BW(:,:,i) - data.joint2BW(:,:,i))));
            end

            if data.evalAccuracyRatio == 1
                joint2.TPR = joint2.TP / (joint2.TP + joint2.FN);
                joint2.TNR = joint2.TN / (joint2.TN + joint2.FP);
                joint2.FPR = joint2.FP / (joint2.FP + joint2.TN);
                joint2.FNR = joint2.FN / (joint2.FN + joint2.TP); 
            end

            if data.evalDice  == 1
                joint2.diceVal = dice(data.joint2BW(:,:,i), GTdata.joint2BW(:,:,i));
            end

            if data.evalHausdorff == 1
                joint2.hausdorff = HausdorffDist(data.joint2BW(:,:,i), GTdata.joint2BW(:,:,i));
            end

            results.joint2 = joint2;

        end

        % Joint 3 
        if data.evalJoint3 == 1

            if data.evalAccuracy == 1
                joint3.TP = sum(sum(bitand(uint8(GTdata.joint3BW(:,:,i)),uint8(data.joint3BW(:,:,i)))));
                joint3.TN = m*n - sum(sum(uint8(bitor(GTdata.joint3BW(:,:,i),data.joint3BW(:,:,i)))));
                joint3.FP = sum(sum(uint8(data.joint3BW(:,:,i) - GTdata.joint3BW(:,:,i))));
                joint3.FN = sum(sum(uint8(GTdata.joint3BW(:,:,i) - data.joint3BW(:,:,i))));
            end

            if data.evalAccuracyRatio == 1
                joint3.TPR = joint3.TP / (joint3.TP + joint3.FN);
                joint3.TNR = joint3.TN / (joint3.TN + joint3.FP);
                joint3.FPR = joint3.FP / (joint3.FP + joint3.TN);
                joint3.FNR = joint3.FN / (joint3.FN + joint3.TP); 
            end

            if data.evalDice  == 1
                joint3.diceVal = dice(data.joint3BW(:,:,i), GTdata.joint3BW(:,:,i));
            end

            if data.evalHausdorff == 1
                joint3.hausdorff = HausdorffDist(data.joint3BW(:,:,i), GTdata.joint3BW(:,:,i));
            end

            results.joint3 = joint3;

        end

        % Joint 4
        if data.evalJoint4 == 1

            if data.evalAccuracy == 1
                joint4.TP = sum(sum(bitand(uint8(GTdata.joint4BW(:,:,i)),uint8(data.joint4BW(:,:,i)))));
                joint4.TN = m*n - sum(sum(uint8(bitor(GTdata.joint4BW(:,:,i),data.joint4BW(:,:,i)))));
                joint4.FP = sum(sum(uint8(data.joint4BW(:,:,i) - GTdata.joint4BW(:,:,i))));
                joint4.FN = sum(sum(uint8(GTdata.joint4BW(:,:,i) - data.joint4BW(:,:,i))));
            end

            if data.evalAccuracyRatio == 1
                joint4.TPR = joint4.TP / (joint4.TP + joint4.FN);
                joint4.TNR = joint4.TN / (joint4.TN + joint4.FP);
                joint4.FPR = joint4.FP / (joint4.FP + joint4.TN);
                joint4.FNR = joint4.FN / (joint4.FN + joint4.TP); 
            end

            if data.evalDice  == 1
                joint4.diceVal = dice(data.joint4BW(:,:,i), GTdata.joint4BW(:,:,i));
            end

            if data.evalHausdorff == 1
                joint4.hausdorff = HausdorffDist(data.joint4BW(:,:,i), GTdata.joint4BW(:,:,i));
            end

            results.joint4 = joint4;

        end
        
        if 0 == isfield(data,'results')
            data.results = results;
        else
            data.results = [data.results, results];
        end
    
    end
    
    if data.wb == 1
        close(h)
    end

end

