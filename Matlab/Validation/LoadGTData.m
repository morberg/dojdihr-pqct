function GTdata = LoadGTData(data)
%GTPROCESSER Summary of this function goes here
%   Detailed explanation goes here

    datasetList         = dir(fullfile(data.GTPath,'*.png'));
    setSize             = numel(datasetList);
    if (data.imgNr+data.imgsNr) > setSize
        fprintf("ERROR: image numbers out of range\n");
        return
    end

    % Get the fileprefix for later save
    filePrefix      = strsplit((getfield(datasetList,'name')) , '_');
    filePrefix      = filePrefix{1};

    % Get the data size and info 
    imData = imread(fullfile(data.GTPath,[filePrefix,'_',num2str(data.imgNr+1),'.png']));
    sizeIm = size(imData);
    GTdata.imDataArrayBW = logical(false(sizeIm(1),sizeIm(2),(data.imgsNr)));
    GTdata.imDataArrayBW(:,:,1) = imfill(imData, 'holes');
    GTdata.imDataArrayBW(:,:,1) = bwareaopen(GTdata.imDataArrayBW(:,:,1), 100);

    for k=2:(data.imgsNr)
       if data.evalFull == 1
            imData = imread(fullfile(data.GTPath,[filePrefix,'_',num2str(data.imgNr+k),'.png']));
            GTdata.imDataArrayBW(:,:,k) = imfill(logical(imData), 'holes'); 
            GTdata.imDataArrayBW(:,:,k) = bwareaopen(GTdata.imDataArrayBW(:,:,k), 100);
       end
    end
    
    if (data.evalJoint1 || data.evalJoint1 || data.evalJoint3 || data.evalJoint4)  
        GTdata.wb = 0;
        GTdata.imDataArrayAdj = data.imDataArrayAdj;
        GTdata.displayTimes = 1;
        GTdata.wb = 1;
        GTdata.step = 'Cropping';
        GTdata = cropJoints(GTdata);
    end
    
end



