profile on
clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\'];
data.GTPath   = [pathdata.gitpath, 'radetect_src\Labeling\1-1_label\1-1\'];

segPath  = 'radetect_src\Matlab\Segmentation';
regPath  = 'radetect_src\Matlab\Registration';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';
detPath  = 'radetect_src\Matlab\Detection';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, regPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));
addpath(genpath([gitPath, detPath ]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 1;
% Select to save the individual segmented joints
data.saveJointsSeg = 1;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 1; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 0;

data.evalFull = 0;
data.evalJoint1 = 0;
data.evalJoint2 = 0;
data.evalJoint3 = 0;
data.evalJoint4 = 0;

data.evalAccuracy = 0;
data.evalAccuracyRatio = 0;
data.evalDice  = 0;
data.evalHausdorff = 0;

data = preprocessing(data);

data = segmentation(data);

data = registration(data);

imageSet = logical(data.imDataArrayBW);

imageSet = ringRemoval(imageSet);

imageSet = logical(jointSplitterv2(imageSet));

imageSet = logical(elementRemoval(imageSet));

erosions = DetectionMethod(imageSet);

cc = bwconncomp(erosions, 26);

for i = 1:cc.NumObjects

   erosion = false(size(erosions));
   erosion(cc.PixelIdxList{i}) = 1;

   [ero.vMethod,ero.centroid,ero.wMethod,ero.dMethod,~] = quantification(erosion,imageSet);
   
   if 1 == exist('results')
       results = [results,ero]; 
   else
       results = ero;
   end     
end

profsave