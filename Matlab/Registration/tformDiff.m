function [rotDiff,xDiff,yDiff] = tformDiff(tform1,tform2,M,N)
%TFORMDIFF Summary of this function goes here
%   Detailed explanation goes here
    T1 = tform1.T;
    T2 = tform2.T;

    rot1 = mean([acosd(T1(1,1)),asind(T1(1,2)),-asind(T1(2,1)),acosd(T1(2,2))]);
    rot2 = mean([acosd(T2(1,1)),-asind(T2(1,2)),asind(T2(2,1)),acosd(T2(2,2))]);

    S1 = [1,0,0;0,1,0;-floor(N/2),-floor(M/2),1];
    S2 = [1,0,0;0,1,0;floor(N/2),floor(M/2),1];

    R1 = [cosd(rot1) sind(rot1) 0; -sind(rot1) cosd(rot1) 0; 0 0 1];
    S31 = S1*R1*S2;

    xReal = (T1(3,1)- S31(3,1))/S31(3,3);
    yReal = (T1(3,2)- S31(3,2))/S31(3,3);

    R2 = [cosd(rot2) -sind(rot2) 0; sind(rot2) cosd(rot2) 0; 0 0 1];
    S32 = S1*R2*S2;

    trans = T2/S32;

    xFound = trans(3,1);
    yFound = trans(3,2);

    rotDiff = abs(rot1-rot2);
    xDiff = abs(xReal+xFound);
    yDiff = abs(yReal+yFound);
end

