clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

set = '220';

if strcmp(set, '110')
    im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_110.png']);
    im1 = bwareaopen(im1, 100);
    im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_111.png']);
    im2 = bwareaopen(im2, 100);
elseif strcmp(set, '220')
    im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_220.png']);
    im1 = bwareaopen(im1, 100);
    im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_221.png']);
    im2 = bwareaopen(im2, 100);
end

diceOrg = dice(im1, im2);
hausdorffOrg = HausdorffDist(im1, im2);

fixed  = double(im1);
moving = double(im2);

[M,N] = size(im1);

testSize     = 75;
diceVal      = zeros(1,testSize);
hausdorffVal = zeros(1,testSize);
time         = zeros(1,testSize);
x            = zeros(1,testSize);
y            = zeros(1,testSize);
rot          = zeros(1,testSize);

params.GradientMagnitudeTolerance = 0.00001;
params.MinimumStepLength = 0.00001;
params.MaximumStepLength = 0.03;
params.MaximumIterations = 140;
params.RelaxationFactor = 0.3;

for i = 1:testSize

    tic
    [tform2,imReg] = register(moving, fixed,1,params);
    time(1,i) = toc;
    
    diceVal(1,i) = dice(logical(imReg), logical(fixed));
    
    hausdorffVal(1,i) = HausdorffDist(logical(imReg), logical(fixed));
    
    [x(1,i),y(1,i), rot(1,i)] = getTform(tform2,M,N);
    
    %[rotDiff(1,i),xDiff(1,i),yDiff(1,i)] = tformDiff(tform1,tform2, M,N);
    
    disp(['Iteration: ', num2str(i)])

end

    if exist([pathdata.gitpath, 'radetect_src/Matlab/Registration/resultsFull',set,'.mat'], 'file') == 2
    
        load([pathdata.gitpath, 'radetect_src/Matlab/Registration/resultsFull',set,'.mat']);
        
        results.diceOrg = diceOrg;
        results.hausdorffOrg = hausdorffOrg;
        
        results.diceMean = (results.diceMean*results.iter+mean(diceVal)*testSize)/(results.iter+testSize);
        results.diceStd  = (results.diceStd*results.iter+std(diceVal)*testSize)/(results.iter+testSize);
        results.diceMin  = min([results.diceMin,min(diceVal)]);
        results.diceMax  = max([results.diceMax,max(diceVal)]);
        
        results.hausdorffMean = (results.hausdorffMean*results.iter+mean(hausdorffVal)*testSize)/(results.iter+testSize);
        results.hausdorffStd  = (results.hausdorffStd*results.iter+std(hausdorffVal)*testSize)/(results.iter+testSize);
        results.hausdorffMin  = min([results.hausdorffMin,min(hausdorffVal)]);
        results.hausdorffMax  = max([results.hausdorffMax,max(hausdorffVal)]);
        
        results.xMean = (results.xMean*results.iter+mean(x)*testSize)/(results.iter+testSize);
        results.xStd  = (results.xStd*results.iter+std(x)*testSize)/(results.iter+testSize);
        results.xMin  = min([results.xMin,min(x)]);
        results.xMax  = max([results.xMax,max(x)]);
        
        results.yMean = (results.yMean*results.iter+mean(y)*testSize)/(results.iter+testSize);
        results.yStd  = (results.yStd*results.iter+std(y)*testSize)/(results.iter+testSize);
        results.yMin  = min([results.yMin,min(y)]);
        results.yMax  = max([results.yMax,max(y)]);
        
        results.rotMean = (results.rotMean*results.iter+mean(rot)*testSize)/(results.iter+testSize);
        results.rotStd  = (results.rotStd*results.iter+std(rot)*testSize)/(results.iter+testSize);
        results.rotMin  = min([results.rotMin,min(rot)]);
        results.rotMax  = max([results.rotMax,max(rot)]);

        results.timeMean = (results.timeMean*results.iter+mean(time)*testSize)/(results.iter+testSize);
        results.timeStd  = (results.timeStd*results.iter+std(time)*testSize)/(results.iter+testSize);
        results.timeMin  = min([results.timeMin,min(time)]);
        results.timeMax  = max([results.timeMax,max(time)]);

        results.iter = results.iter+testSize;
    else
        results.diceOrg = diceOrg;
        results.hausdorffOrg = hausdorffOrg;
        
        results.diceMean = mean(diceVal);
        results.diceStd  = std(diceVal);
        results.diceMin  = min(diceVal);
        results.diceMax  = max(diceVal);
        
        results.hausdorffMean = mean(hausdorffVal);
        results.hausdorffStd  = std(hausdorffVal);
        results.hausdorffMin  = min(hausdorffVal);
        results.hausdorffMax  = max(hausdorffVal);
        
        results.xMean = mean(x);
        results.xStd  = std(x);
        results.xMin  = min(x);
        results.xMax  = max(x);
        
        results.yMean = mean(y);
        results.yStd  = std(y);
        results.yMin  = min(y);
        results.yMax  = max(y);
        
        results.rotMean = mean(rot);
        results.rotStd  = std(rot);
        results.rotMin  = min(rot);
        results.rotMax  = max(rot);

        results.timeMean = mean(time);
        results.timeStd  = std(time);
        results.timeMin  = min(time);
        results.timeMax  = max(time);

        results.iter = testSize;
    end
    
    save([pathdata.gitpath, 'radetect_src/Matlab/Registration/resultsFull',set,'.mat'], 'results');
    
