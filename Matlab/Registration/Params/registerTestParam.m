clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

TestName = '12';
% GraMag  Mini   Max  Iter  Relax
Param = 'Relax';

im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_1.png']);
im1 = bwareaopen(im1, 100);
im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_2.png']);
im2 = bwareaopen(im2, 100);

diceOrg = dice(im1, im2);

fixed = double(im1);

if strcmp(TestName, '11')
    moving = double(im1);
elseif strcmp(TestName, '12')
    moving = double(im2);
end

if strcmp(Param, 'GraMag')
    steps = 0.00002:0.00002:0.0002;
elseif strcmp(Param, 'Mini')
    steps = 0.000002:0.000002:0.00002;
elseif strcmp(Param, 'Max')
    steps = 0.01:0.01:0.1;
elseif strcmp(Param, 'Iter')
    steps = 20:20:200;
elseif strcmp(Param, 'Relax')
    steps = 0.1:0.09:0.91;
end

[M,N] = size(im1);

testSize    = 100;
rotDiff     = zeros(1,testSize);
xDiff       = zeros(1,testSize);
yDiff       = zeros(1,testSize);
diceVal     = zeros(1,testSize);
time        = zeros(1,testSize);

for j = 1:size(steps, 2)
    
    if strcmp(Param, 'GraMag')
        params.GradientMagnitudeTolerance = steps(1,j);
        params.MinimumStepLength = 0.00001;
        params.MaximumStepLength = 0.0325;
        params.MaximumIterations = 100;
        params.RelaxationFactor = 0.5;
    elseif strcmp(Param, 'Mini')
        params.GradientMagnitudeTolerance = 0.0001;
        params.MinimumStepLength = steps(1,j);
        params.MaximumStepLength = 0.0325;
        params.MaximumIterations = 100;
        params.RelaxationFactor = 0.5;
    elseif strcmp(Param, 'Max')
        params.GradientMagnitudeTolerance = 0.0001;
        params.MinimumStepLength = 0.00001;
        params.MaximumStepLength = steps(1,j);
        params.MaximumIterations = 100;
        params.RelaxationFactor = 0.5;
    elseif strcmp(Param, 'Iter')
        params.GradientMagnitudeTolerance = 0.0001;
        params.MinimumStepLength = 0.00001;
        params.MaximumStepLength = 0.0325;
        params.MaximumIterations = steps(1,j);
        params.RelaxationFactor = 0.5;
    elseif strcmp(Param, 'Relax')
        params.GradientMagnitudeTolerance = 0.0001;
        params.MinimumStepLength = 0.00001;
        params.MaximumStepLength = 0.0325;
        params.MaximumIterations = 100;
        params.RelaxationFactor = steps(1,j);
    end

    
    for i = 1:testSize

        rot = randi([-5,20]);
        x = randi([-60,40]);
        y = randi([-45,40]);

        [im, tform1] = translate(moving, x,y,rot);

        tic
        [tform2,imReg] = register(im, fixed,1,params);
        time(1,i) = toc;

        [rotDiff(1,i),xDiff(1,i),yDiff(1,i)] = tformDiff(tform1,tform2, M,N);

        diceVal(1,i) = dice(logical(imReg), logical(fixed));

        disp(['Iteration: ', num2str(i)])

    end
    
    result.rotMean = mean(rotDiff);
    result.rotStd  = std(rotDiff);
    result.rotMin  = min(rotDiff);
    result.rotMax  = max(rotDiff);

    result.xMean = mean(xDiff);
    result.xStd  = std(xDiff);
    result.xMin  = min(xDiff);
    result.xMax  = max(xDiff);

    result.yMean = mean(yDiff);
    result.yStd  = std(yDiff);
    result.yMin  = min(yDiff);
    result.yMax  = max(yDiff);

    result.diceMean = mean(diceVal);
    result.diceStd  = std(diceVal);
    result.diceMin  = min(diceVal);
    result.diceMax  = max(diceVal);

    result.timeMean = mean(time);
    result.timeStd  = std(time);
    result.timeMin  = min(time);
    result.timeMax  = max(time);

    result.iter = testSize;
    
    result.step = steps(j);
    
    if 1 == exist('results')
        results = [results, result];
    else
        results = result;
    end
    
    disp(['Step: ', num2str(j)])
end

save([pathdata.gitpath, 'radetect_src/Matlab/Registration/', TestName, Param,'RegisterTest.mat'], 'results');
