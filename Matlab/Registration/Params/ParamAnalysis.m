clear
close all

load('11GraMagRegisterTest.mat')

ss = 10;

rotMean = zeros(1,ss);
rotStd = zeros(1, ss);
rotMin = zeros(1, ss);
rotMax = zeros(1, ss);
xMean = zeros(1,ss);
xStd = zeros(1,ss);
xMin = zeros(1,ss);
xMax = zeros(1,ss);
yMean = zeros(1,ss);
yStd = zeros(1,ss);
yMin = zeros(1,ss);
yMax = zeros(1,ss);
timeMean = zeros(1,ss);
timeStd = zeros(1,ss);
timeMin = zeros(1,ss);
timeMax = zeros(1,ss);
step = zeros(1,ss);

for j = 1:ss

    rotMean(1,j) = results(j).rotMean;
    rotStd(1,j) = results(j).rotStd;
    rotMin(1,j) = results(j).rotMin;
    rotMax(1,j) = results(j).rotMax;
    xMean(1,j) = results(j).xMean;
    xStd(1,j) = results(j).xStd;
    xMin(1,j) = results(j).xMin;
    xMax(1,j) = results(j).xMax;
    yMean(1,j) = results(j).yMean;
    yStd(1,j) = results(j).yStd;
    yMin(1,j) = results(j).yMin;
    yMax(1,j) = results(j).yMax;
    timeMean(1,j) = results(j).timeMean;
    timeStd(1,j) = results(j).timeStd;
    timeMin(1,j) = results(j).timeMin;
    timeMax(1,j) = results(j).timeMax;
    step(1,j) = results(j).step;

end

%%
figure
errorbar(step, rotMean, rotStd); hold on
errorbar(step, xMean, xStd); hold on
errorbar(step, yMean, yStd)
legend('Rotation', 'X translation', 'Y translation', 'Location', 'northeast')
xlabel('Gradient Magnitude')
ylabel('Registration Error (Pixels)')
title('Registration error with Gradient Magnitude')
xlim([0,0.00021])
ylim([-1,2.5])

%%
figure
errorbar(step, timeMean, timeStd)
legend('Time', 'Location', 'southwest')
xlabel('Gradient Magnitude')
ylabel('Registration time (seconds)')
title('Registration error with Gradient Magnitude')
xlim([0,0.00021])
