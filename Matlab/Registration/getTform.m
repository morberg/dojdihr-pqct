function [xFound,yFound, rot] = getTform(tform,M,N)
%GETTFORM Summary of this function goes here
%   Detailed explanation goes here
    T2 = tform.T;

    rot = mean([acosd(T2(1,1)),-asind(T2(1,2)),asind(T2(2,1)),acosd(T2(2,2))]);

    S1 = [1,0,0;0,1,0;-floor(N/2),-floor(M/2),1];
    S2 = [1,0,0;0,1,0;floor(N/2),floor(M/2),1];

    R2 = [cosd(rot) -sind(rot) 0; sind(rot) cosd(rot) 0; 0 0 1];
    S3 = S1*R2*S2;

    trans = T2/S3;

    xFound = trans(3,1);
    yFound = trans(3,2);


end

