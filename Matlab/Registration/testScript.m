clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_1.png']);
im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_2.png']);

[M,N] = size(im1);

moving = double(im2);
fixed = double(im1);
rot= -5:20;
y = -60:50;
x=-45:50;

r = randi([-5,20]);

[moving, tform1] = translate(moving, rot,y,x);

figure
imshow(moving);
    
[tform2, im1] = register(moving, fixed, 1);

%[rotDiff,xDiff,yDiff] = tformDiff(tform1,tform2, M,N);

figure
imshowpair(im1, fixed, 'diff');
title('im1')