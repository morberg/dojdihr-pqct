clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

TestName = '12';

im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_1.png']);
im1 = bwareaopen(im1, 100);
im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_2.png']);
im2 = bwareaopen(im2, 100);

diceOrg = dice(im1, im2);

fixed = double(im1);

if strcmp(TestName, '11')
    moving = double(im1);
elseif strcmp(TestName, '12')
    moving = double(im2);
end

[M,N] = size(im1);

params.GradientMagnitudeTolerance = 0.00001;
params.MinimumStepLength = 0.00001;
params.MaximumStepLength = 0.03;
params.MaximumIterations = 140;
params.RelaxationFactor = 0.3;

testSize    = 300;
rotDiff     = zeros(1,testSize);
xDiff       = zeros(1,testSize);
yDiff       = zeros(1,testSize);
diceVal     = zeros(1,testSize);
time        = zeros(1,testSize);

for i = 1:testSize
    
    rot = randi([-5,20]);
    x = randi([-60,40]);
    y = randi([-45,40]);

    [im, tform1] = translate(moving, x,y,rot);
    
    tic
    [tform2,imReg] = register(im, fixed,1,params);
    time(1,i) = toc;
    
    [rotDiff(1,i),xDiff(1,i),yDiff(1,i)] = tformDiff(tform1,tform2, M,N);
    
    diceVal(1,i) = dice(logical(imReg), logical(fixed));
    
    disp(['Iteration: ', num2str(i)])

end

    if exist([pathdata.gitpath, 'radetect_src/Matlab/Registration/', TestName, 'registerTest.mat'], 'file') == 2
    
        load([pathdata.gitpath, 'radetect_src/Matlab/Registration/', TestName, 'registerTest.mat']);

        results.rotMean = (results.rotMean*results.iter+mean(rotDiff)*testSize)/(results.iter+testSize);
        results.rotStd  = (results.rotStd*results.iter+std(rotDiff)*testSize)/(results.iter+testSize);
        results.rotMin  = min([results.rotMin,min(rotDiff)]);
        results.rotMax  = max([results.rotMax,max(rotDiff)]);

        results.xMean = (results.xMean*results.iter+mean(xDiff)*testSize)/(results.iter+testSize);
        results.xStd  = (results.xStd*results.iter+std(xDiff)*testSize)/(results.iter+testSize);
        results.xMin  = min([results.xMin,min(xDiff)]);
        results.xMax  = max([results.xMax,max(xDiff)]);

        results.yMean = (results.yMean*results.iter+mean(yDiff)*testSize)/(results.iter+testSize);
        results.yStd  = (results.yStd*results.iter+std(yDiff)*testSize)/(results.iter+testSize);
        results.yMin  = min([results.yMin,min(yDiff)]);
        results.yMax  = max([results.yMax,max(yDiff)]);
        
        results.diceMean = (results.diceMean*results.iter+mean(diceVal)*testSize)/(results.iter+testSize);
        results.diceStd  = (results.diceStd*results.iter+std(diceVal)*testSize)/(results.iter+testSize);
        results.diceMin  = min([results.diceMin,min(diceVal)]);
        results.diceMax  = max([results.diceMax,max(diceVal)]);

        results.timeMean = (results.timeMean*results.iter+mean(time)*testSize)/(results.iter+testSize);
        results.timeStd  = (results.timeStd*results.iter+std(time)*testSize)/(results.iter+testSize);
        results.timeMin  = min([results.timeMin,min(time)]);
        results.timeMax  = max([results.timeMax,max(time)]);

        results.iter = results.iter+testSize;
    else
        results.rotMean = mean(rotDiff);
        results.rotStd  = std(rotDiff);
        results.rotMin  = min(rotDiff);
        results.rotMax  = max(rotDiff);

        results.xMean = mean(xDiff);
        results.xStd  = std(xDiff);
        results.xMin  = min(xDiff);
        results.xMax  = max(xDiff);

        results.yMean = mean(yDiff);
        results.yStd  = std(yDiff);
        results.yMin  = min(yDiff);
        results.yMax  = max(yDiff);
        
        results.diceMean = mean(diceVal);
        results.diceStd  = std(diceVal);
        results.diceMin  = min(diceVal);
        results.diceMax  = max(diceVal);

        results.timeMean = mean(time);
        results.timeStd  = std(time);
        results.timeMin  = min(time);
        results.timeMax  = max(time);

        results.iter = testSize;
    end
    
    save([pathdata.gitpath, 'radetect_src/Matlab/Registration/', TestName, 'registerTest.mat'], 'results');
    
