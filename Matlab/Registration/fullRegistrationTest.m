clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 1;
data.evalJoint2 = 1;
data.evalJoint3 = 1;
data.evalJoint4 = 1;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;
% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

params.GradientMagnitudeTolerance = 0.00001;
params.MinimumStepLength = 0.00001;
params.MaximumStepLength = 0.03;
params.MaximumIterations = 140;
params.RelaxationFactor = 0.3;

data = preprocessing(data);

data = LoadGTData(data);

dataOri = data;

cb_ref = imref2d(size(data.imDataArrayBW(:,:,1)));

[tform1,~] = register(double(data.joint1BW(:,:,111)), double(data.joint1BW(:,:,110)),1,params);
[tform2,~] = register(double(data.joint2BW(:,:,111)), double(data.joint2BW(:,:,110)),1,params);
[tform3,~] = register(double(data.joint3BW(:,:,111)), double(data.joint3BW(:,:,110)),1,params);
[tform4,~] = register(double(data.joint4BW(:,:,111)), double(data.joint4BW(:,:,110)),1,params);

for i = 111:220
    data.joint1BW(:,:,i) = imwarp(data.joint1BW(:,:,i), tform1,'OutputView', cb_ref);
    data.joint2BW(:,:,i) = imwarp(data.joint2BW(:,:,i), tform2,'OutputView', cb_ref);
    data.joint3BW(:,:,i) = imwarp(data.joint3BW(:,:,i), tform3,'OutputView', cb_ref);
    data.joint4BW(:,:,i) = imwarp(data.joint4BW(:,:,i), tform4,'OutputView', cb_ref);
    disp(['Image: ' num2str(i)]);
end

[tform12,~] = register(double(data.joint1BW(:,:,221)), double(data.joint1BW(:,:,220)),1,params);
[tform22,~] = register(double(data.joint2BW(:,:,221)), double(data.joint2BW(:,:,220)),1,params);
[tform32,~] = register(double(data.joint3BW(:,:,221)), double(data.joint3BW(:,:,220)),1,params);
[tform42,~] = register(double(data.joint4BW(:,:,221)), double(data.joint4BW(:,:,220)),1,params);

for i = 221:330
    data.joint1BW(:,:,i) = imwarp(data.joint1BW(:,:,i), tform12,'OutputView', cb_ref);
    data.joint2BW(:,:,i) = imwarp(data.joint2BW(:,:,i), tform22,'OutputView', cb_ref);
    data.joint3BW(:,:,i) = imwarp(data.joint3BW(:,:,i), tform32,'OutputView', cb_ref);
    data.joint4BW(:,:,i) = imwarp(data.joint4BW(:,:,i), tform42,'OutputView', cb_ref);
    disp(['Image: ' num2str(i)]);
end

data.imDataArrayBW = data.joint1BW + data.joint2BW + data.joint3BW +data.joint4BW;

%%
[M,N] = size(data.joint1BW(:,:,1));
[xFound,yFound, rot] = getTform(tform4,M,N);
[~,tform222] = translate(data.joint4BW(:,:,111), xFound,yFound,rot);
[xFound2,yFound2,rot2] = getTform(tform222,M,N);
reTrans2 = imwarp(data.joint4BW(:,:,111), invert(tform222),'OutputView', cb_ref);
figure
imshowpair(dataOri.joint4BW(:,:,111), reTrans2)
%%
reTrans2 = imwarp(data.joint4BW(:,:,111), invert(tform4),'OutputView', cb_ref);
[xFound2,yFound2, rot2] = getTform(invert(tform4),M,N)
figure
imshowpair(dataOri.joint4BW(:,:,111), reTrans2)
%%
figure
imshowpair(data.joint4BW(:,:,111), dataOri.joint4BW(:,:,111))

%%

volumeViewer(data.imDataArrayBW)

   
