clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

gitPath  = pathdata.gitpath;
data.resultPath  = [pathdata.gitpath, 'radetect_src\Matlab\Segmentation\Kang'];
data.GTPath   = [pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/'];

segPath  = 'radetect_src\Matlab\Segmentation';
prePath  = 'radetect_src\Matlab\Preprocessing';
postPath = 'radetect_src\Matlab\Postprocessing';
valPath  = 'radetect_src\Matlab\Validation';

addpath(genpath([gitPath, segPath ]));
addpath(genpath([gitPath, prePath ]));
addpath(genpath([gitPath, postPath]));
addpath(genpath([gitPath, valPath]));

% The drive name of the harddrive
data.datapath = pathdata.datapath;
% The name of the set to be validated
data.setName = '1-1';

% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

% Select to save the BW segmentation mask
data.saveBW = 1;
% Select to save the segmented image
data.saveSeg = 0;
% Select to save the individual segmented joints
data.saveJointsSeg = 0;
% select to save the individual BW segmentation masks for the joints
data.saveJointsBW = 0; 

% Select to display the timing of the invidual steps
data.displayTimes = 1;
% Select to have waitbars
data.wb = 1;

data.evalData = 1;

data.evalFull = 1;
data.evalJoint1 = 1;
data.evalJoint2 = 1;
data.evalJoint3 = 1;
data.evalJoint4 = 1;
data.sizeEval = 0;

data.evalAccuracy = 1;
data.evalAccuracyRatio = 1;
data.evalDice  = 1;
data.evalHausdorff = 0;
% Number of images to process (maks 330)
data.imgsNr = 330;
% Image to start from (Range is 0 to 329)
data.imgNr = 0;

data = preprocessing(data);

data = LoadGTData(data);


cb_ref = imref2d(size(data.imDataArrayBW(:,:,1)));

[tform1,~] = register(double(data.joint1BW(:,:,111)), double(data.joint1BW(:,:,110)),1);
[tform2,~] = register(double(data.joint2BW(:,:,111)), double(data.joint2BW(:,:,110)),1);
[tform3,~] = register(double(data.joint3BW(:,:,111)), double(data.joint3BW(:,:,110)),1);
[tform4,~] = register(double(data.joint4BW(:,:,111)), double(data.joint4BW(:,:,110)),1);

for i = 111:220
    data.joint1BW(:,:,i) = imwarp(data.joint1BW(:,:,i), tform1,'OutputView', cb_ref);
    data.joint2BW(:,:,i) = imwarp(data.joint2BW(:,:,i), tform2,'OutputView', cb_ref);
    data.joint3BW(:,:,i) = imwarp(data.joint3BW(:,:,i), tform3,'OutputView', cb_ref);
    data.joint4BW(:,:,i) = imwarp(data.joint4BW(:,:,i), tform4,'OutputView', cb_ref);
    disp(['Image: ' num2str(i)]);
end

[tform1,~] = register(double(data.joint1BW(:,:,221)), double(data.joint1BW(:,:,220)),1);
[tform2,~] = register(double(data.joint2BW(:,:,221)), double(data.joint2BW(:,:,220)),1);
[tform3,~] = register(double(data.joint3BW(:,:,221)), double(data.joint3BW(:,:,220)),1);
[tform4,~] = register(double(data.joint4BW(:,:,221)), double(data.joint4BW(:,:,220)),1);

for i = 221:330
    data.joint1BW(:,:,i) = imwarp(data.joint1BW(:,:,i), tform1,'OutputView', cb_ref);
    data.joint2BW(:,:,i) = imwarp(data.joint2BW(:,:,i), tform2,'OutputView', cb_ref);
    data.joint3BW(:,:,i) = imwarp(data.joint3BW(:,:,i), tform3,'OutputView', cb_ref);
    data.joint4BW(:,:,i) = imwarp(data.joint4BW(:,:,i), tform4,'OutputView', cb_ref);
    disp(['Image: ' num2str(i)]);
end


[xFound,yFound, rotFound] = getTform(tform2,M,N);