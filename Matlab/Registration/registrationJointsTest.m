clear
clf
close all

feature('accel', 'on')
load('C:\pathdata.mat')

set = '220';

if strcmp(set, '110')
    im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_110.png']);
    im1 = bwareaopen(im1, 100);
    im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_111.png']);
    im2 = bwareaopen(im2, 100);
elseif strcmp(set, '220')
    im1 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_220.png']);
    im1 = bwareaopen(im1, 100);
    im2 = imread([pathdata.gitpath, 'radetect_src/Labeling/GT/1-1/Label_221.png']);
    im2 = bwareaopen(im2, 100);
end

diceOrg = dice(im1, im2);
hausdorffOrg = HausdorffDist(im1, im2);

fixed  = double(im1);
moving = double(im2);

im1CC = bwconncomp(im1);
im2CC = bwconncomp(im2);

joint11 = false(size(im1));
joint11(im1CC.PixelIdxList{1})=1;
joint12 = false(size(im1));
joint12(im1CC.PixelIdxList{2})=1;
joint13 = false(size(im1));
joint13(im1CC.PixelIdxList{3})=1;
joint14 = false(size(im1));
joint14(im1CC.PixelIdxList{4})=1;

joint21 = false(size(im2));
joint21(im2CC.PixelIdxList{1})=1;
joint22 = false(size(im2));
joint22(im2CC.PixelIdxList{2})=1;
joint23 = false(size(im2));
joint23(im2CC.PixelIdxList{3})=1;
joint24 = false(size(im2));
joint24(im2CC.PixelIdxList{4})=1;

diceOrg1 = dice(joint11, joint21);
hausdorffOrg1 = HausdorffDist(joint11, joint21);
diceOrg2 = dice(joint12, joint22);
hausdorffOrg2 = HausdorffDist(joint12, joint22);
diceOrg3 = dice(joint13, joint23);
hausdorffOrg3 = HausdorffDist(joint13, joint23);
diceOrg4 = dice(joint14, joint24);
hausdorffOrg4 = HausdorffDist(joint14, joint24);

[M,N] = size(im1);

testSize     = 200;
diceVal      = zeros(1,testSize);
hausdorffVal = zeros(1,testSize);
time         = zeros(1,testSize);
x1           = zeros(1,testSize);
x2           = zeros(1,testSize);
x3           = zeros(1,testSize);
x4           = zeros(1,testSize);
y1            = zeros(1,testSize);
y2            = zeros(1,testSize);
y3            = zeros(1,testSize);
y4            = zeros(1,testSize);
rot1          = zeros(1,testSize);
rot2          = zeros(1,testSize);
rot3          = zeros(1,testSize);
rot4          = zeros(1,testSize);

params.GradientMagnitudeTolerance = 0.00001;
params.MinimumStepLength = 0.00001;
params.MaximumStepLength = 0.03;
params.MaximumIterations = 140;
params.RelaxationFactor = 0.3;

for i = 1:testSize

    tic
    [tform1,imReg1] = register(double(joint21), double(joint11),1,params);
    [tform2,imReg2] = register(double(joint22), double(joint12),1,params);
    [tform3,imReg3] = register(double(joint23), double(joint13),1,params);
    [tform4,imReg4] = register(double(joint24), double(joint14),1,params);
    time(1,i) = toc;
    
    imReg = imReg1 + imReg2 + imReg3 + imReg4;
    
    diceVal(1,i) = dice(logical(imReg), logical(fixed));
    
    hausdorffVal(1,i) = HausdorffDist(logical(imReg), logical(fixed));
    
    [x1(1,i),y1(1,i), rot1(1,i)] = getTform(tform1,M,N);
    
    [x2(1,i),y2(1,i), rot2(1,i)] = getTform(tform2,M,N);
    
    [x3(1,i),y3(1,i), rot3(1,i)] = getTform(tform3,M,N);
    
    [x4(1,i),y4(1,i), rot4(1,i)] = getTform(tform4,M,N);
    
    disp(['Iteration: ', num2str(i)])

end

    if exist([pathdata.gitpath, 'radetect_src/Matlab/Registration/resultsJoints',set,'.mat'], 'file') == 2
    
        load([pathdata.gitpath, 'radetect_src/Matlab/Registration/resultsJoints',set,'.mat']);
        
        results.diceOrg = diceOrg;
        results.hausdorffOrg = hausdorffOrg;
        
        results.diceMean = (results.diceMean*results.iter+mean(diceVal)*testSize)/(results.iter+testSize);
        results.diceStd  = (results.diceStd*results.iter+std(diceVal)*testSize)/(results.iter+testSize);
        results.diceMin  = min([results.diceMin,min(diceVal)]);
        results.diceMax  = max([results.diceMax,max(diceVal)]);
        
        results.hausdorffMean = (results.hausdorffMean*results.iter+mean(hausdorffVal)*testSize)/(results.iter+testSize);
        results.hausdorffStd  = (results.hausdorffStd*results.iter+std(hausdorffVal)*testSize)/(results.iter+testSize);
        results.hausdorffMin  = min([results.hausdorffMin,min(hausdorffVal)]);
        results.hausdorffMax  = max([results.hausdorffMax,max(hausdorffVal)]);
        
        results.xMean1 = (results.xMean1*results.iter+mean(x1)*testSize)/(results.iter+testSize);
        results.xStd1  = (results.xStd1*results.iter+std(x1)*testSize)/(results.iter+testSize);
        results.xMin1  = min([results.xMin1,min(x1)]);
        results.xMax1  = max([results.xMax1,max(x1)]);
        
        results.xMean2 = (results.xMean2*results.iter+mean(x2)*testSize)/(results.iter+testSize);
        results.xStd2  = (results.xStd2*results.iter+std(x2)*testSize)/(results.iter+testSize);
        results.xMin2  = min([results.xMin2,min(x2)]);
        results.xMax2  = max([results.xMax2,max(x2)]);
        
        results.xMean3 = (results.xMean3*results.iter+mean(x3)*testSize)/(results.iter+testSize);
        results.xStd3  = (results.xStd3*results.iter+std(x3)*testSize)/(results.iter+testSize);
        results.xMin3  = min([results.xMin3,min(x3)]);
        results.xMax3  = max([results.xMax3,max(x3)]);
        
        results.xMean4 = (results.xMean4*results.iter+mean(x4)*testSize)/(results.iter+testSize);
        results.xStd4  = (results.xStd4*results.iter+std(x4)*testSize)/(results.iter+testSize);
        results.xMin4  = min([results.xMin4,min(x4)]);
        results.xMax4  = max([results.xMax4,max(x4)]);
        
        results.yMean1 = (results.yMean1*results.iter+mean(y1)*testSize)/(results.iter+testSize);
        results.yStd1  = (results.yStd1*results.iter+std(y1)*testSize)/(results.iter+testSize);
        results.yMin1  = min([results.yMin1,min(y1)]);
        results.yMax1  = max([results.yMax1,max(y1)]);
        
        results.yMean2 = (results.yMean2*results.iter+mean(y2)*testSize)/(results.iter+testSize);
        results.yStd2  = (results.yStd2*results.iter+std(y2)*testSize)/(results.iter+testSize);
        results.yMin2  = min([results.yMin2,min(y2)]);
        results.yMax2  = max([results.yMax2,max(y2)]);
        
        results.yMean3 = (results.yMean3*results.iter+mean(y3)*testSize)/(results.iter+testSize);
        results.yStd3  = (results.yStd3*results.iter+std(y3)*testSize)/(results.iter+testSize);
        results.yMin3 = min([results.yMin3,min(y3)]);
        results.yMax3  = max([results.yMax3,max(y3)]);
        
        results.yMean4 = (results.yMean4*results.iter+mean(y4)*testSize)/(results.iter+testSize);
        results.yStd4  = (results.yStd4*results.iter+std(y4)*testSize)/(results.iter+testSize);
        results.yMin4  = min([results.yMin4,min(y4)]);
        results.yMax4  = max([results.yMax4,max(y4)]);
        
        results.rotMean1 = (results.rotMean1*results.iter+mean(rot1)*testSize)/(results.iter+testSize);
        results.rotStd1  = (results.rotStd1*results.iter+std(rot1)*testSize)/(results.iter+testSize);
        results.rotMin1  = min([results.rotMin1,min(rot1)]);
        results.rotMax1  = max([results.rotMax1,max(rot1)]);
        
        results.rotMean2 = (results.rotMean2*results.iter+mean(rot2)*testSize)/(results.iter+testSize);
        results.rotStd2  = (results.rotStd2*results.iter+std(rot2)*testSize)/(results.iter+testSize);
        results.rotMin2  = min([results.rotMin2,min(rot2)]);
        results.rotMax2  = max([results.rotMax2,max(rot2)]);
        
        results.rotMean3 = (results.rotMean3*results.iter+mean(rot3)*testSize)/(results.iter+testSize);
        results.rotStd3  = (results.rotStd3*results.iter+std(rot3)*testSize)/(results.iter+testSize);
        results.rotMin3  = min([results.rotMin3,min(rot3)]);
        results.rotMax3  = max([results.rotMax3,max(rot3)]);
        
        results.rotMean4 = (results.rotMean4*results.iter+mean(rot4)*testSize)/(results.iter+testSize);
        results.rotStd4  = (results.rotStd4*results.iter+std(rot4)*testSize)/(results.iter+testSize);
        results.rotMin4  = min([results.rotMin4,min(rot4)]);
        results.rotMax4 = max([results.rotMax4,max(rot4)]);

        results.timeMean = (results.timeMean*results.iter+mean(time)*testSize)/(results.iter+testSize);
        results.timeStd  = (results.timeStd*results.iter+std(time)*testSize)/(results.iter+testSize);
        results.timeMin  = min([results.timeMin,min(time)]);
        results.timeMax  = max([results.timeMax,max(time)]);

        results.iter = results.iter+testSize;
    else
        results.diceOrg = diceOrg;
        results.hausdorffOrg = hausdorffOrg;
        
        results.diceMean = mean(diceVal);
        results.diceStd  = std(diceVal);
        results.diceMin  = min(diceVal);
        results.diceMax  = max(diceVal);
        
        results.hausdorffMean = mean(hausdorffVal);
        results.hausdorffStd  = std(hausdorffVal);
        results.hausdorffMin  = min(hausdorffVal);
        results.hausdorffMax  = max(hausdorffVal);
        
        results.xMean1 = mean(x1);
        results.xStd1  = std(x1);
        results.xMin1  = min(x1);
        results.xMax1  = max(x1);
        
        results.xMean2 = mean(x2);
        results.xStd2  = std(x2);
        results.xMin2  = min(x2);
        results.xMax2  = max(x2);
        
        results.xMean3 = mean(x3);
        results.xStd3  = std(x3);
        results.xMin3  = min(x3);
        results.xMax3  = max(x3);
        
        results.xMean4 = mean(x4);
        results.xStd4  = std(x4);
        results.xMin4  = min(x4);
        results.xMax4  = max(x4);
        
        results.yMean1 = mean(y1);
        results.yStd1  = std(y1);
        results.yMin1  = min(y1);
        results.yMax1  = max(y1);
        
        results.yMean2 = mean(y2);
        results.yStd2  = std(y2);
        results.yMin2  = min(y2);
        results.yMax2  = max(y2);
        
        results.yMean3 = mean(y3);
        results.yStd3  = std(y3);
        results.yMin3  = min(y3);
        results.yMax3  = max(y3);
        
        results.yMean4 = mean(y4);
        results.yStd4  = std(y4);
        results.yMin4  = min(y4);
        results.yMax4  = max(y4);
        
        results.rotMean1 = mean(rot1);
        results.rotStd1  = std(rot1);
        results.rotMin1  = min(rot1);
        results.rotMax1  = max(rot1);
        
        results.rotMean2 = mean(rot2);
        results.rotStd2  = std(rot2);
        results.rotMin2  = min(rot2);
        results.rotMax2  = max(rot2);
        
        results.rotMean3 = mean(rot3);
        results.rotStd3  = std(rot3);
        results.rotMin3  = min(rot3);
        results.rotMax3  = max(rot3);
        
        results.rotMean4 = mean(rot4);
        results.rotStd4  = std(rot4);
        results.rotMin4  = min(rot4);
        results.rotMax4  = max(rot4);

        results.timeMean = mean(time);
        results.timeStd  = std(time);
        results.timeMin  = min(time);
        results.timeMax  = max(time);

        results.iter = testSize;
    end
    
    save([pathdata.gitpath, 'radetect_src/Matlab/Registration/resultsJoints',set,'.mat'], 'results');
