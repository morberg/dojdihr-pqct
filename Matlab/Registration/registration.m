function data = registration(data)
%REGISTRATION Summary of this function goes here
%   Detailed explanation goes here
    time = tic;
    
    data.step = 'Registration: ';
    
    if data.wb == 1
        wbMsg = [data.step];
        h = waitbar(0,wbMsg);
    end

    if 1 ~= isfield(data,'joint1BW')
        data = cropJoints(data);
    end
    
    if data.wb == 1
        waitbar(1/10)
    end
    
    cb_ref = imref2d(size(data.imDataArrayBW(:,:,1)));
    params.GradientMagnitudeTolerance = 0.0001;
    params.MinimumStepLength = 0.00001;
    params.MaximumStepLength = 0.0325;
    params.MaximumIterations = 100;
    params.RelaxationFactor = 0.5;
    
    if data.imgNr+data.imgsNr >= 111 && data.imgNr <= 110
        if data.imgNr+data.imgsNr > 220
            limit = 220;
        else
            limit = data.imgNr+data.imgsNr;
        end
        if data.wb == 1
            waitbar(2/10)
        end
        [tform1,~] = register(double(data.joint1BW(:,:,111)), double(data.joint1BW(:,:,110)),1,params);
        
        if data.wb == 1
            waitbar(3/10)
        end
        [tform2,~] = register(double(data.joint2BW(:,:,111)), double(data.joint2BW(:,:,110)),1,params);
        
        if data.wb == 1
            waitbar(4/10)
        end
        [tform3,~] = register(double(data.joint3BW(:,:,111)), double(data.joint3BW(:,:,110)),1,params);
        
        if data.wb == 1
            waitbar(5/10)
        end
        [tform4,~] = register(double(data.joint4BW(:,:,111)), double(data.joint4BW(:,:,110)),1,params);

        for i = 111:limit
            data.joint1BW(:,:,i) = imwarp(data.joint1BW(:,:,i), tform1,'OutputView', cb_ref);
            data.joint1(:,:,i) = imwarp(data.joint1(:,:,i), tform1,'OutputView', cb_ref);
            
            data.joint2BW(:,:,i) = imwarp(data.joint2BW(:,:,i), tform2,'OutputView', cb_ref);
            data.joint2(:,:,i) = imwarp(data.joint2(:,:,i), tform2,'OutputView', cb_ref);
            
            data.joint3BW(:,:,i) = imwarp(data.joint3BW(:,:,i), tform3,'OutputView', cb_ref);
            data.joint3(:,:,i) = imwarp(data.joint3(:,:,i), tform3,'OutputView', cb_ref);
            
            data.joint4BW(:,:,i) = imwarp(data.joint4BW(:,:,i), tform4,'OutputView', cb_ref);
            data.joint4(:,:,i) = imwarp(data.joint4(:,:,i), tform4,'OutputView', cb_ref);
        end
    end

    if data.imgNr+data.imgsNr >= 221
        if data.imgNr+data.imgsNr == 330
            limit = 330;
        else
            limit = data.imgNr+data.imgsNr;
        end
        
        if data.wb == 1
            waitbar(6/10)
        end
        [tform1,~] = register(double(data.joint1BW(:,:,221)), double(data.joint1BW(:,:,220)),1,params);
        
        if data.wb == 1
            waitbar(7/10)
        end
        [tform2,~] = register(double(data.joint2BW(:,:,221)), double(data.joint2BW(:,:,220)),1,params);
        
        if data.wb == 1
            waitbar(8/10)
        end
        [tform3,~] = register(double(data.joint3BW(:,:,221)), double(data.joint3BW(:,:,220)),1,params);
        
        if data.wb == 1
            waitbar(9/10)
        end
        [tform4,~] = register(double(data.joint4BW(:,:,221)), double(data.joint4BW(:,:,220)),1,params);

        for i = 221:limit
            data.joint1BW(:,:,i) = imwarp(data.joint1BW(:,:,i), tform1,'OutputView', cb_ref);
            data.joint1(:,:,i) = imwarp(data.joint1(:,:,i), tform1,'OutputView', cb_ref);
            
            data.joint2BW(:,:,i) = imwarp(data.joint2BW(:,:,i), tform2,'OutputView', cb_ref);
            data.joint2(:,:,i) = imwarp(data.joint2(:,:,i), tform2,'OutputView', cb_ref);
            
            data.joint3BW(:,:,i) = imwarp(data.joint3BW(:,:,i), tform3,'OutputView', cb_ref);
            data.joint3(:,:,i) = imwarp(data.joint3(:,:,i), tform3,'OutputView', cb_ref);
            
            data.joint4BW(:,:,i) = imwarp(data.joint4BW(:,:,i), tform4,'OutputView', cb_ref);
            data.joint4(:,:,i) = imwarp(data.joint4(:,:,i), tform4,'OutputView', cb_ref);
        end
    end
    
    if data.wb == 1
        waitbar(10/10)
    end
    
    data.imDataArrayBW = data.joint1BW + data.joint2BW + data.joint3BW +data.joint4BW;
    data.imDataArraySeg = data.joint1 + data.joint2 + data.joint3 +data.joint4;
    
    if data.wb == 1
        close(h)
    end
    
    if data.displayTimes ==1
        fprintf("Registration Complete. Time: %3.2f s\n", toc(time));
    end
end

