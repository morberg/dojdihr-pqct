function [im2,tform] = translate(im, x,y,rot)
%TRANSLATE Summary of this function goes here
%   Detailed explanation goes here
    [M,N] = size(im);

    R = [cosd(rot) sind(rot) 0; -sind(rot) cosd(rot) 0; 0 0 1];

    T= [1 0 0; 0 1 0; -N/2 -M/2 1];
    T2= [1 0 0; 0 1 0; N/2+x M/2+y 1];

    tform = affine2d(T*R*T2);

    cb_ref = imref2d(size(im));

    [im2,~] = imwarp(im,tform,'OutputView', cb_ref);

end

